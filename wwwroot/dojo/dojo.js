/*
	Copyright (c) 2004-2006, The Dojo Foundation
	All Rights Reserved.

	Licensed under the Academic Free License version 2.1 or above OR the
	modified BSD license. For more information on Dojo licensing, see:

		http://dojotoolkit.org/community/licensing.shtml
*/

/*
	This is a compiled version of Dojo, built for deployment and not for
	development. To get an editable version, please visit:

		http://dojotoolkit.org

	for documentation and information on getting the source.
*/

if(typeof dojo=="undefined"){
var dj_global=this;
var dj_currentContext=this;
function dj_undef(_1,_2){
return (typeof (_2||dj_currentContext)[_1]=="undefined");
}
if(dj_undef("djConfig",this)){
var djConfig={};
}
if(dj_undef("dojo",this)){
var dojo={};
}
dojo.global=function(){
return dj_currentContext;
};
dojo.locale=djConfig.locale;
dojo.version={major:0,minor:0,patch:0,flag:"dev",revision:Number("$Rev: 7460 $".match(/[0-9]+/)[0]),toString:function(){
with(dojo.version){
return major+"."+minor+"."+patch+flag+" ("+revision+")";
}
}};
dojo.getObject=function(_3,_4,_5,_6){
var _7,_8;
if(typeof _3!="string"){
return undefined;
}
_7=_5;
if(!_7){
_7=dojo.global();
}
var _9=_3.split("."),i=0,_b,_c,_d;
do{
_b=_7;
_d=_9[i];
_c=_7[_9[i]];
if((_4)&&(!_c)){
_c=_7[_9[i]]={};
}
_7=_c;
i++;
}while(i<_9.length&&_7);
_8=_7;
_7=_b;
return (_6)?{obj:_7,prop:_d}:_8;
};
dojo.exists=function(_e,_f){
if(typeof _f=="string"){
dojo.deprecated("dojo.exists(obj, name)","use dojo.exists(name, obj, /*optional*/create)","0.6");
var tmp=_e;
_e=_f;
_f=tmp;
}
return (!!dojo.getObject(_e,false,_f));
};
dojo.evalProp=function(_11,_12,_13){
dojo.deprecated("dojo.evalProp","just use hash syntax. Sheesh.","0.6");
return _12[_11]||(_13?(_12[_11]={}):undefined);
};
dojo.parseObjPath=function(_14,_15,_16){
dojo.deprecated("dojo.parseObjPath","use dojo.getObject(path, create, context, true)","0.6");
return dojo.getObject(_14,_16,_15,true);
};
dojo.evalObjPath=function(_17,_18){
dojo.deprecated("dojo.evalObjPath","use dojo.getObject(path, create)","0.6");
return dojo.getObject(_17,_18);
};
dojo.errorToString=function(_19){
return (_19["message"]||_19["description"]||_19);
};
dojo.raise=function(_1a,_1b){
if(_1b){
_1a=_1a+": "+dojo.errorToString(_1b);
}else{
_1a=dojo.errorToString(_1a);
}
try{
if(djConfig.isDebug){
dojo.hostenv.println("FATAL exception raised: "+_1a);
}
}
catch(e){
}
throw _1b||Error(_1a);
};
dojo.debug=function(){
};
dojo.debugShallow=function(obj){
};
dojo.profile={start:function(){
},end:function(){
},stop:function(){
},dump:function(){
}};
function dj_eval(_1d){
return dj_global.eval?dj_global.eval(_1d):eval(_1d);
}
dojo.unimplemented=function(_1e,_1f){
var _20="'"+_1e+"' not implemented";
if(_1f!=null){
_20+=" "+_1f;
}
dojo.raise(_20);
};
dojo.deprecated=function(_21,_22,_23){
var _24="DEPRECATED: "+_21;
if(_22){
_24+=" "+_22;
}
if(_23){
_24+=" -- will be removed in version: "+_23;
}
dojo.debug(_24);
};
dojo.render=(function(){
function vscaffold(_25,_26){
var tmp={capable:false,support:{builtin:false,plugin:false},prefixes:_25};
for(var i=0;i<_26.length;i++){
tmp[_26[i]]=false;
}
return tmp;
}
return {name:"",ver:dojo.version,os:{win:false,linux:false,osx:false},html:vscaffold(["html"],["ie","opera","khtml","safari","moz"]),svg:vscaffold(["svg"],["corel","adobe","batik"]),vml:vscaffold(["vml"],["ie"]),swf:vscaffold(["Swf","Flash","Mm"],["mm"]),swt:vscaffold(["Swt"],["ibm"])};
})();
dojo.hostenv=(function(){
var _29={isDebug:false,allowQueryConfig:false,baseScriptUri:"",baseRelativePath:"",libraryScriptUri:"",iePreventClobber:false,ieClobberMinimal:true,preventBackButtonFix:true,delayMozLoadingFix:false,searchIds:[],parseWidgets:true};
if(typeof djConfig=="undefined"){
djConfig=_29;
}else{
for(var _2a in _29){
if(typeof djConfig[_2a]=="undefined"){
djConfig[_2a]=_29[_2a];
}
}
}
return {name_:"(unset)",version_:"(unset)",getName:function(){
return this.name_;
},getVersion:function(){
return this.version_;
},getText:function(uri){
dojo.unimplemented("getText","uri="+uri);
}};
})();
dojo.hostenv.getBaseScriptUri=function(){
if(djConfig.baseScriptUri.length){
return djConfig.baseScriptUri;
}
var uri=new String(djConfig.libraryScriptUri||djConfig.baseRelativePath);
if(!uri){
dojo.raise("Nothing returned by getLibraryScriptUri(): "+uri);
}
djConfig.baseScriptUri=djConfig.baseRelativePath;
return djConfig.baseScriptUri;
};
(function(){
var _2d={pkgFileName:"__package__",loading_modules_:{},loaded_modules_:{},addedToLoadingCount:[],removedFromLoadingCount:[],inFlightCount:0,modulePrefixes_:{dojo:{name:"dojo",value:"src"}},registerModulePath:function(_2e,_2f){
this.modulePrefixes_[_2e]={name:_2e,value:_2f};
},moduleHasPrefix:function(_30){
var mp=this.modulePrefixes_;
return Boolean(mp[_30]&&mp[_30].value);
},getModulePrefix:function(_32){
if(this.moduleHasPrefix(_32)){
return this.modulePrefixes_[_32].value;
}
return _32;
},getTextStack:[],loadUriStack:[],loadedUris:[],post_load_:false,modulesLoadedListeners:[],unloadListeners:[],loadNotifying:false,insideBindHandler:false};
for(var _33 in _2d){
dojo.hostenv[_33]=_2d[_33];
}
})();
dojo.hostenv.loadPath=function(_34,_35,cb){
var uri;
if(_34.charAt(0)=="/"||_34.match(/^\w+:/)){
uri=_34;
}else{
uri=this.getBaseScriptUri()+_34;
}
if(djConfig.cacheBust&&dojo.render.html.capable){
uri+="?"+String(djConfig.cacheBust).replace(/\W+/g,"");
}
try{
return !_35?this.loadUri(uri,cb):this.loadUriAndCheck(uri,_35,cb);
}
catch(e){
dojo.debug(e);
return false;
}
};
dojo.hostenv.loadUri=function(uri,cb){
if(this.loadedUris[uri]){
return true;
}
var _3a=this.getText(uri,null,true);
if(!_3a){
return false;
}
this.loadedUris[uri]=true;
if(cb){
_3a="("+_3a+")";
}
var _3b=dj_eval(_3a);
if(cb){
cb(_3b);
}
return true;
};
dojo.hostenv.loadUriAndCheck=function(uri,_3d,cb){
var ok=true;
try{
ok=this.loadUri(uri,cb);
}
catch(e){
dojo.debug("failed loading ",uri," with error: ",e);
}
return Boolean(ok&&this.findModule(_3d,false));
};
dojo.loaded=function(){
};
dojo.unloaded=function(){
};
dojo.hostenv.loaded=function(){
this.loadNotifying=true;
this.post_load_=true;
var mll=this.modulesLoadedListeners;
for(var x=0;x<mll.length;x++){
mll[x]();
}
this.modulesLoadedListeners=[];
this.loadNotifying=false;
dojo.loaded();
};
dojo.hostenv.unloaded=function(){
var mll=this.unloadListeners;
while(mll.length){
(mll.pop())();
}
dojo.unloaded();
};
dojo.addOnLoad=function(obj,_44){
var dh=dojo.hostenv;
if(arguments.length==1){
dh.modulesLoadedListeners.push(obj);
}else{
if(arguments.length>1){
dh.modulesLoadedListeners.push(function(){
obj[_44]();
});
}
}
if(dh.post_load_&&dh.inFlightCount==0&&!dh.loadNotifying&&!dh.insideBindHandler){
dh.callLoaded();
}
};
dojo.addOnUnload=function(obj,_47){
var dh=dojo.hostenv;
if(arguments.length==1){
dh.unloadListeners.push(obj);
}else{
if(arguments.length>1){
dh.unloadListeners.push(function(){
obj[_47]();
});
}
}
};
dojo.hostenv.modulesLoaded=function(){
if(this.post_load_){
return;
}
if(this.loadUriStack.length==0&&this.getTextStack.length==0){
if(this.inFlightCount>0){
dojo.debug("files still in flight!");
return;
}
dojo.hostenv.callLoaded();
}
};
dojo.hostenv.callLoaded=function(){
if(typeof setTimeout=="object"||(djConfig["useXDomain"]&&dojo.render.html.opera)){
setTimeout("dojo.hostenv.loaded();",0);
}else{
dojo.hostenv.loaded();
}
};
dojo.hostenv.getModuleSymbols=function(_49){
var _4a=_49.split(".");
for(var i=_4a.length;i>0;i--){
var _4c=_4a.slice(0,i).join(".");
if((i==1)&&!this.moduleHasPrefix(_4c)){
_4a[0]="../"+_4a[0];
}else{
var _4d=this.getModulePrefix(_4c);
if(_4d!=_4c){
_4a.splice(0,i,_4d);
break;
}
}
}
return _4a;
};
dojo.hostenv._global_omit_module_check=false;
dojo.hostenv.loadModule=function(_4e,_4f,_50){
if(!_4e){
return;
}
_50=this._global_omit_module_check||_50;
var _51=this.findModule(_4e,false);
if(_51){
return _51;
}
if(dj_undef(_4e,this.loading_modules_)){
this.addedToLoadingCount.push(_4e);
}
this.loading_modules_[_4e]=1;
var _52=_4e.replace(/\./g,"/")+".js";
var _53=_4e.split(".");
var _54=this.getModuleSymbols(_4e);
var _55=((_54[0].charAt(0)!="/")&&!_54[0].match(/^\w+:/));
var _56=_54[_54.length-1];
var ok;
if(_56=="*"){
_4e=_53.slice(0,-1).join(".");
while(_54.length){
_54.pop();
_54.push(this.pkgFileName);
_52=_54.join("/")+".js";
if(_55&&_52.charAt(0)=="/"){
_52=_52.slice(1);
}
ok=this.loadPath(_52,!_50?_4e:null);
if(ok){
break;
}
_54.pop();
}
}else{
_52=_54.join("/")+".js";
_4e=_53.join(".");
var _58=!_50?_4e:null;
ok=this.loadPath(_52,_58);
if(!ok&&!_4f){
_54.pop();
while(_54.length){
_52=_54.join("/")+".js";
ok=this.loadPath(_52,_58);
if(ok){
break;
}
_54.pop();
_52=_54.join("/")+"/"+this.pkgFileName+".js";
if(_55&&_52.charAt(0)=="/"){
_52=_52.slice(1);
}
ok=this.loadPath(_52,_58);
if(ok){
break;
}
}
}
if(!ok&&!_50){
dojo.raise("Could not load '"+_4e+"'; last tried '"+_52+"'");
}
}
if(!_50&&!this["isXDomain"]){
_51=this.findModule(_4e,false);
if(!_51){
dojo.raise("symbol '"+_4e+"' is not defined after loading '"+_52+"'");
}
}
return _51;
};
dojo.hostenv.startPackage=function(_59){
var _5a=String(_59);
var _5b=_5a;
var _5c=_59.split(/\./);
if(_5c[_5c.length-1]=="*"){
_5c.pop();
_5b=_5c.join(".");
}
var _5d=dojo.getObject(_5b,true);
this.loaded_modules_[_5a]=_5d;
this.loaded_modules_[_5b]=_5d;
return _5d;
};
dojo.hostenv.findModule=function(_5e,_5f){
var lmn=String(_5e);
if(this.loaded_modules_[lmn]){
return this.loaded_modules_[lmn];
}
if(_5f){
dojo.raise("no loaded module named '"+_5e+"'");
}
return null;
};
dojo.kwCompoundRequire=function(_61){
var _62=_61["common"]||[];
var _63=_62.concat(_61[dojo.hostenv.name_]||_61["default"]||[]);
for(var x=0;x<_63.length;x++){
var _65=_63[x];
if(_65.constructor==Array){
dojo.hostenv.loadModule.apply(dojo.hostenv,_65);
}else{
dojo.hostenv.loadModule(_65);
}
}
};
dojo.require=function(_66){
dojo.hostenv.loadModule.apply(dojo.hostenv,arguments);
};
dojo.requireIf=function(_67,_68){
var _69=arguments[0];
if((_69===true)||(_69=="common")||(_69&&dojo.render[_69].capable)){
var _6a=[];
for(var i=1;i<arguments.length;i++){
_6a.push(arguments[i]);
}
dojo.require.apply(dojo,_6a);
}
};
dojo.requireAfterIf=dojo.requireIf;
dojo.provide=function(_6c){
return dojo.hostenv.startPackage.apply(dojo.hostenv,arguments);
};
dojo.registerModulePath=function(_6d,_6e){
return dojo.hostenv.registerModulePath(_6d,_6e);
};
if(djConfig["modulePaths"]){
for(var param in djConfig["modulePaths"]){
dojo.registerModulePath(param,djConfig["modulePaths"][param]);
}
}
dojo.requireLocalization=function(_6f,_70,_71,_72){
dojo.require("dojo.i18n.loader");
dojo.i18n._requireLocalization.apply(dojo.hostenv,arguments);
};
}
if(typeof window!="undefined"){
(function(){
if(djConfig.allowQueryConfig){
var _73=document.location.toString();
var _74=_73.split("?",2);
if(_74.length>1){
var _75=_74[1];
var _76=_75.split("&");
for(var x in _76){
var sp=_76[x].split("=");
if((sp[0].length>9)&&(sp[0].substr(0,9)=="djConfig.")){
var opt=sp[0].substr(9);
try{
djConfig[opt]=eval(sp[1]);
}
catch(e){
djConfig[opt]=sp[1];
}
}
}
}
}
if(((djConfig["baseScriptUri"]=="")||(djConfig["baseRelativePath"]==""))&&(document&&document.getElementsByTagName)){
var _7a=document.getElementsByTagName("script");
var _7b=/(__package__|dojo|bootstrap1)\.js([\?\.]|$)/i;
for(var i=0;i<_7a.length;i++){
var src=_7a[i].getAttribute("src");
if(!src){
continue;
}
var m=src.match(_7b);
if(m){
var _7f=src.substring(0,m.index);
if(src.indexOf("bootstrap1")>-1){
_7f+="../";
}
if(!this["djConfig"]){
djConfig={};
}
if(djConfig["baseScriptUri"]==""){
djConfig["baseScriptUri"]=_7f;
}
if(djConfig["baseRelativePath"]==""){
djConfig["baseRelativePath"]=_7f;
}
break;
}
}
}
var dr=dojo.render;
var drh=dojo.render.html;
var drs=dojo.render.svg;
var dua=(drh.UA=navigator.userAgent);
var dav=(drh.AV=navigator.appVersion);
var t=true;
var f=false;
drh.capable=t;
drh.support.builtin=t;
dr.ver=parseFloat(drh.AV);
dr.os.mac=dav.indexOf("Macintosh")>=0;
dr.os.win=dav.indexOf("Windows")>=0;
dr.os.linux=dav.indexOf("X11")>=0;
drh.opera=dua.indexOf("Opera")>=0;
drh.khtml=(dav.indexOf("Konqueror")>=0)||(dav.indexOf("Safari")>=0);
drh.safari=dav.indexOf("Safari")>=0;
var _87=dua.indexOf("Gecko");
drh.mozilla=drh.moz=(_87>=0)&&(!drh.khtml);
if(drh.mozilla){
drh.geckoVersion=dua.substring(_87+6,_87+14);
}
drh.ie=(document.all)&&(!drh.opera);
drh.ie50=drh.ie&&dav.indexOf("MSIE 5.0")>=0;
drh.ie55=drh.ie&&dav.indexOf("MSIE 5.5")>=0;
drh.ie60=drh.ie&&dav.indexOf("MSIE 6.0")>=0;
drh.ie70=drh.ie&&dav.indexOf("MSIE 7.0")>=0;
var cm=document["compatMode"];
drh.quirks=(cm=="BackCompat")||(cm=="QuirksMode")||drh.ie55||drh.ie50;
dojo.locale=dojo.locale||(drh.ie?navigator.userLanguage:navigator.language).toLowerCase();
dr.vml.capable=drh.ie;
drs.capable=f;
drs.support.plugin=f;
drs.support.builtin=f;
var _89=window["document"];
var tdi=_89["implementation"];
if((tdi)&&(tdi["hasFeature"])&&(tdi.hasFeature("org.w3c.dom.svg","1.0"))){
drs.capable=t;
drs.support.builtin=t;
drs.support.plugin=f;
}
if(drh.safari){
var tmp=dua.split("AppleWebKit/")[1];
var ver=parseFloat(tmp.split(" ")[0]);
if(ver>=420){
drs.capable=t;
drs.support.builtin=t;
drs.support.plugin=f;
}
}else{
}
})();
dojo.hostenv.startPackage("dojo.hostenv");
dojo.render.name=dojo.hostenv.name_="browser";
dojo.hostenv.searchIds=[];
dojo.hostenv._XMLHTTP_PROGIDS=["Msxml2.XMLHTTP","Microsoft.XMLHTTP","Msxml2.XMLHTTP.4.0"];
dojo.hostenv.getXmlhttpObject=function(){
var _8d=null;
var _8e=null;
try{
_8d=new XMLHttpRequest();
}
catch(e){
}
if(!_8d){
for(var i=0;i<3;++i){
var _90=dojo.hostenv._XMLHTTP_PROGIDS[i];
try{
_8d=new ActiveXObject(_90);
}
catch(e){
_8e=e;
}
if(_8d){
dojo.hostenv._XMLHTTP_PROGIDS=[_90];
break;
}
}
}
if(!_8d){
return dojo.raise("XMLHTTP not available",_8e);
}
return _8d;
};
dojo.hostenv._blockAsync=false;
dojo.hostenv.getText=function(uri,_92,_93){
if(!_92){
this._blockAsync=true;
}
var _94=this.getXmlhttpObject();
function isDocumentOk(_95){
var _96=_95["status"];
return Boolean((!_96)||((200<=_96)&&(300>_96))||(_96==304));
}
if(_92){
var _97=this,_98=null,gbl=dojo.global();
var xhr=dojo.getObject("dojo.io.XMLHTTPTransport");
_94.onreadystatechange=function(){
if(_98){
gbl.clearTimeout(_98);
_98=null;
}
if(_97._blockAsync||(xhr&&xhr._blockAsync)){
_98=gbl.setTimeout(function(){
_94.onreadystatechange.apply(this);
},10);
}else{
if(4==_94.readyState){
if(isDocumentOk(_94)){
_92(_94.responseText);
}
}
}
};
}
_94.open("GET",uri,_92?true:false);
try{
_94.send(null);
if(_92){
return null;
}
if(!isDocumentOk(_94)){
var err=Error("Unable to load "+uri+" status:"+_94.status);
err.status=_94.status;
err.responseText=_94.responseText;
throw err;
}
}
catch(e){
this._blockAsync=false;
if((_93)&&(!_92)){
return null;
}else{
throw e;
}
}
this._blockAsync=false;
return _94.responseText;
};
dojo.hostenv.defaultDebugContainerId="dojoDebug";
dojo.hostenv._println_buffer=[];
dojo.hostenv._println_safe=false;
dojo.hostenv.println=function(_9c){
if(!dojo.hostenv._println_safe){
dojo.hostenv._println_buffer.push(_9c);
}else{
try{
var _9d=document.getElementById(djConfig.debugContainerId?djConfig.debugContainerId:dojo.hostenv.defaultDebugContainerId);
if(!_9d){
_9d=dojo.body();
}
var div=document.createElement("div");
div.appendChild(document.createTextNode(_9c));
_9d.appendChild(div);
}
catch(e){
try{
document.write("<div>"+_9c+"</div>");
}
catch(e2){
window.status=_9c;
}
}
}
};
dojo.addOnLoad(function(){
dojo.hostenv._println_safe=true;
while(dojo.hostenv._println_buffer.length>0){
dojo.hostenv.println(dojo.hostenv._println_buffer.shift());
}
});
function dj_addNodeEvtHdlr(_9f,_a0,fp){
var _a2=_9f["on"+_a0]||function(){
};
_9f["on"+_a0]=function(){
fp.apply(_9f,arguments);
_a2.apply(_9f,arguments);
};
return true;
}
dojo.hostenv._djInitFired=false;
function dj_load_init(e){
dojo.hostenv._djInitFired=true;
var _a4=(e&&e.type)?e.type.toLowerCase():"load";
if(arguments.callee.initialized||(_a4!="domcontentloaded"&&_a4!="load")){
return;
}
arguments.callee.initialized=true;
if(typeof (_timer)!="undefined"){
clearInterval(_timer);
delete _timer;
}
var _a5=function(){
if(dojo.render.html.ie){
dojo.hostenv.makeWidgets();
}
};
if(dojo.hostenv.inFlightCount==0){
_a5();
dojo.hostenv.modulesLoaded();
}else{
dojo.hostenv.modulesLoadedListeners.unshift(_a5);
}
}
if(document.addEventListener){
if(dojo.render.html.opera||(dojo.render.html.moz&&(djConfig["enableMozDomContentLoaded"]===true))){
document.addEventListener("DOMContentLoaded",dj_load_init,null);
}
window.addEventListener("load",dj_load_init,null);
}
if(dojo.render.html.ie&&dojo.render.os.win){
document.write("<scr"+"ipt defer src=\"//:\" "+"onreadystatechange=\"if(this.readyState=='complete'){dj_load_init();}\">"+"</scr"+"ipt>");
}
if(/(WebKit|khtml)/i.test(navigator.userAgent)){
var _timer=setInterval(function(){
if(/loaded|complete/.test(document.readyState)){
dj_load_init();
}
},10);
}
if(dojo.render.html.ie){
dj_addNodeEvtHdlr(window,"beforeunload",function(){
dojo.hostenv._unloading=true;
window.setTimeout(function(){
dojo.hostenv._unloading=false;
},0);
});
}
dj_addNodeEvtHdlr(window,"unload",function(){
if((!dojo.render.html.ie)||(dojo.render.html.ie&&dojo.hostenv._unloading)){
dojo.hostenv.unloaded();
}
});
dojo.hostenv.makeWidgets=function(){
var _a6=[];
if(djConfig.searchIds&&djConfig.searchIds.length>0){
_a6=_a6.concat(djConfig.searchIds);
}
if(dojo.hostenv.searchIds&&dojo.hostenv.searchIds.length>0){
_a6=_a6.concat(dojo.hostenv.searchIds);
}
if((djConfig.parseWidgets)||(_a6.length>0)){
if(dojo.getObject("dojo.widget.Parse")){
var _a7=new dojo.xml.Parse();
if(_a6.length>0){
for(var x=0;x<_a6.length;x++){
var _a9=document.getElementById(_a6[x]);
if(!_a9){
continue;
}
var _aa=_a7.parseElement(_a9,null,true);
dojo.widget.getParser().createComponents(_aa);
}
}else{
if(djConfig.parseWidgets){
var _aa=_a7.parseElement(dojo.body(),null,true);
dojo.widget.getParser().createComponents(_aa);
}
}
}
}
};
dojo.addOnLoad(function(){
if(!dojo.render.html.ie){
dojo.hostenv.makeWidgets();
}
});
try{
if(dojo.render.html.ie){
document.namespaces.add("v","urn:schemas-microsoft-com:vml");
document.createStyleSheet().addRule("v\\:*","behavior:url(#default#VML)");
}
}
catch(e){
}
dojo.hostenv.writeIncludes=function(){
};
if(!dj_undef("document",this)){
dj_currentDocument=this.document;
}
dojo.doc=function(){
return dj_currentDocument;
};
dojo.body=function(){
return dojo.doc().body||dojo.doc().getElementsByTagName("body")[0];
};
dojo.byId=function(id,doc){
if((id)&&((typeof id=="string")||(id instanceof String))){
if(!doc){
doc=dj_currentDocument;
}
var ele=doc.getElementById(id);
if(ele&&(ele.id!=id)&&doc.all){
ele=null;
eles=doc.all[id];
if(eles){
if(eles.length){
for(var i=0;i<eles.length;i++){
if(eles[i].id==id){
ele=eles[i];
break;
}
}
}else{
ele=eles;
}
}
}
return ele;
}
return id;
};
dojo.setContext=function(_af,_b0){
dj_currentContext=_af;
dj_currentDocument=_b0;
};
dojo._fireCallback=function(_b1,_b2,_b3){
if((_b2)&&((typeof _b1=="string")||(_b1 instanceof String))){
_b1=_b2[_b1];
}
return (_b2?_b1.apply(_b2,_b3||[]):_b1());
};
dojo.withGlobal=function(_b4,_b5,_b6,_b7){
var _b8;
var _b9=dj_currentContext;
var _ba=dj_currentDocument;
try{
dojo.setContext(_b4,_b4.document);
_b8=dojo._fireCallback(_b5,_b6,_b7);
}
finally{
dojo.setContext(_b9,_ba);
}
return _b8;
};
dojo.withDoc=function(_bb,_bc,_bd,_be){
var _bf;
var _c0=dj_currentDocument;
try{
dj_currentDocument=_bb;
_bf=dojo._fireCallback(_bc,_bd,_be);
}
finally{
dj_currentDocument=_c0;
}
return _bf;
};
}
dojo.requireIf((djConfig["isDebug"]||djConfig["debugAtAllCosts"]),"dojo.debug");
dojo.requireIf(djConfig["debugAtAllCosts"]&&!window.widget&&!djConfig["useXDomain"],"dojo.browser_debug");
dojo.requireIf(djConfig["debugAtAllCosts"]&&!window.widget&&djConfig["useXDomain"],"dojo.browser_debug_xd");
if(!this["dojo"]){
alert("\"dojo/__package__.js\" is now located at \"dojo/dojo.js\". Please update your includes accordingly");
}
dojo.provide("dojo.lang.common");
dojo.lang.inherits=function(_c1,_c2){
if(!dojo.lang.isFunction(_c2)){
dojo.raise("dojo.inherits: superclass argument ["+_c2+"] must be a function (subclass: ["+_c1+"']");
}
_c1.prototype=new _c2();
_c1.prototype.constructor=_c1;
_c1.superclass=_c2.prototype;
_c1["super"]=_c2.prototype;
};
dojo.lang._mixin=function(obj,_c4){
var _c5={};
for(var x in _c4){
if((typeof _c5[x]=="undefined")||(_c5[x]!=_c4[x])){
obj[x]=_c4[x];
}
}
if(dojo.render.html.ie&&(typeof (_c4["toString"])=="function")&&(_c4["toString"]!=obj["toString"])&&(_c4["toString"]!=_c5["toString"])){
obj.toString=_c4.toString;
}
return obj;
};
dojo.lang.mixin=function(obj,_c8){
for(var i=1,l=arguments.length;i<l;i++){
dojo.lang._mixin(obj,arguments[i]);
}
return obj;
};
dojo.lang.extend=function(_cb,_cc){
for(var i=1,l=arguments.length;i<l;i++){
dojo.lang._mixin(_cb.prototype,arguments[i]);
}
return _cb;
};
dojo.lang._delegate=function(obj,_d0){
function TMP(){
}
TMP.prototype=obj;
var tmp=new TMP();
if(_d0){
dojo.lang.mixin(tmp,_d0);
}
return tmp;
};
dojo.inherits=dojo.lang.inherits;
dojo.mixin=dojo.lang.mixin;
dojo.extend=dojo.lang.extend;
dojo.lang.find=function(_d2,_d3,_d4,_d5){
var _d6=dojo.lang.isString(_d2);
if(_d6){
_d2=_d2.split("");
}
if(_d5){
var _d7=-1;
var i=_d2.length-1;
var end=-1;
}else{
var _d7=1;
var i=0;
var end=_d2.length;
}
if(_d4){
while(i!=end){
if(_d2[i]===_d3){
return i;
}
i+=_d7;
}
}else{
while(i!=end){
if(_d2[i]==_d3){
return i;
}
i+=_d7;
}
}
return -1;
};
dojo.lang.indexOf=dojo.lang.find;
dojo.lang.findLast=function(_da,_db,_dc){
return dojo.lang.find(_da,_db,_dc,true);
};
dojo.lang.lastIndexOf=dojo.lang.findLast;
dojo.lang.inArray=function(_dd,_de){
return dojo.lang.find(_dd,_de)>-1;
};
dojo.lang.isObject=function(it){
if(typeof it=="undefined"){
return false;
}
return (typeof it=="object"||it===null||dojo.lang.isArray(it)||dojo.lang.isFunction(it));
};
dojo.lang.isArray=function(it){
return (it&&it instanceof Array||typeof it=="array");
};
dojo.lang.isArrayLike=function(it){
if((!it)||(dojo.lang.isUndefined(it))){
return false;
}
if(dojo.lang.isString(it)){
return false;
}
if(dojo.lang.isFunction(it)){
return false;
}
if(dojo.lang.isArray(it)){
return true;
}
if((it.tagName)&&(it.tagName.toLowerCase()=="form")){
return false;
}
if(dojo.lang.isNumber(it.length)&&isFinite(it.length)){
return true;
}
return false;
};
dojo.lang.isFunction=function(it){
return (it instanceof Function||typeof it=="function");
};
(function(){
if((dojo.render.html.capable)&&(dojo.render.html["safari"])){
dojo.lang.isFunction=function(it){
if((typeof (it)=="function")&&(it=="[object NodeList]")){
return false;
}
return (it instanceof Function||typeof it=="function");
};
}
})();
dojo.lang.isString=function(it){
return (typeof it=="string"||it instanceof String);
};
dojo.lang.isAlien=function(it){
if(!it){
return false;
}
return !dojo.lang.isFunction(it)&&/\{\s*\[native code\]\s*\}/.test(String(it));
};
dojo.lang.isBoolean=function(it){
return (it instanceof Boolean||typeof it=="boolean");
};
dojo.lang.isNumber=function(it){
return (it instanceof Number||typeof it=="number");
};
dojo.lang.isUndefined=function(it){
return ((typeof (it)=="undefined")&&(it==undefined));
};
dojo.provide("dojo.dom");
dojo.dom.ELEMENT_NODE=1;
dojo.dom.ATTRIBUTE_NODE=2;
dojo.dom.TEXT_NODE=3;
dojo.dom.CDATA_SECTION_NODE=4;
dojo.dom.ENTITY_REFERENCE_NODE=5;
dojo.dom.ENTITY_NODE=6;
dojo.dom.PROCESSING_INSTRUCTION_NODE=7;
dojo.dom.COMMENT_NODE=8;
dojo.dom.DOCUMENT_NODE=9;
dojo.dom.DOCUMENT_TYPE_NODE=10;
dojo.dom.DOCUMENT_FRAGMENT_NODE=11;
dojo.dom.NOTATION_NODE=12;
dojo.dom.dojoml="http://www.dojotoolkit.org/2004/dojoml";
dojo.dom.xmlns={svg:"http://www.w3.org/2000/svg",smil:"http://www.w3.org/2001/SMIL20/",mml:"http://www.w3.org/1998/Math/MathML",cml:"http://www.xml-cml.org",xlink:"http://www.w3.org/1999/xlink",xhtml:"http://www.w3.org/1999/xhtml",xul:"http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul",xbl:"http://www.mozilla.org/xbl",fo:"http://www.w3.org/1999/XSL/Format",xsl:"http://www.w3.org/1999/XSL/Transform",xslt:"http://www.w3.org/1999/XSL/Transform",xi:"http://www.w3.org/2001/XInclude",xforms:"http://www.w3.org/2002/01/xforms",saxon:"http://icl.com/saxon",xalan:"http://xml.apache.org/xslt",xsd:"http://www.w3.org/2001/XMLSchema",dt:"http://www.w3.org/2001/XMLSchema-datatypes",xsi:"http://www.w3.org/2001/XMLSchema-instance",rdf:"http://www.w3.org/1999/02/22-rdf-syntax-ns#",rdfs:"http://www.w3.org/2000/01/rdf-schema#",dc:"http://purl.org/dc/elements/1.1/",dcq:"http://purl.org/dc/qualifiers/1.0","soap-env":"http://schemas.xmlsoap.org/soap/envelope/",wsdl:"http://schemas.xmlsoap.org/wsdl/",AdobeExtensions:"http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"};
dojo.dom.isNode=function(wh){
if(typeof Element=="function"){
try{
return wh instanceof Element;
}
catch(e){
}
}else{
return wh&&!isNaN(wh.nodeType);
}
};
dojo.dom.getUniqueId=function(){
var _ea=dojo.doc();
do{
var id="dj_unique_"+(++arguments.callee._idIncrement);
}while(_ea.getElementById(id));
return id;
};
dojo.dom.getUniqueId._idIncrement=0;
dojo.dom.firstElement=dojo.dom.getFirstChildElement=function(_ec,_ed){
var _ee=_ec.firstChild;
while(_ee&&_ee.nodeType!=dojo.dom.ELEMENT_NODE){
_ee=_ee.nextSibling;
}
if(_ed&&_ee&&_ee.tagName&&_ee.tagName.toLowerCase()!=_ed.toLowerCase()){
_ee=dojo.dom.nextElement(_ee,_ed);
}
return _ee;
};
dojo.dom.lastElement=dojo.dom.getLastChildElement=function(_ef,_f0){
var _f1=_ef.lastChild;
while(_f1&&_f1.nodeType!=dojo.dom.ELEMENT_NODE){
_f1=_f1.previousSibling;
}
if(_f0&&_f1&&_f1.tagName&&_f1.tagName.toLowerCase()!=_f0.toLowerCase()){
_f1=dojo.dom.prevElement(_f1,_f0);
}
return _f1;
};
dojo.dom.nextElement=dojo.dom.getNextSiblingElement=function(_f2,_f3){
if(!_f2){
return null;
}
do{
_f2=_f2.nextSibling;
}while(_f2&&_f2.nodeType!=dojo.dom.ELEMENT_NODE);
if(_f2&&_f3&&_f3.toLowerCase()!=_f2.tagName.toLowerCase()){
return dojo.dom.nextElement(_f2,_f3);
}
return _f2;
};
dojo.dom.prevElement=dojo.dom.getPreviousSiblingElement=function(_f4,_f5){
if(!_f4){
return null;
}
if(_f5){
_f5=_f5.toLowerCase();
}
do{
_f4=_f4.previousSibling;
}while(_f4&&_f4.nodeType!=dojo.dom.ELEMENT_NODE);
if(_f4&&_f5&&_f5.toLowerCase()!=_f4.tagName.toLowerCase()){
return dojo.dom.prevElement(_f4,_f5);
}
return _f4;
};
dojo.dom.moveChildren=function(_f6,_f7,_f8){
var _f9=0;
if(_f8){
while(_f6.hasChildNodes()&&_f6.firstChild.nodeType==dojo.dom.TEXT_NODE){
_f6.removeChild(_f6.firstChild);
}
while(_f6.hasChildNodes()&&_f6.lastChild.nodeType==dojo.dom.TEXT_NODE){
_f6.removeChild(_f6.lastChild);
}
}
while(_f6.hasChildNodes()){
_f7.appendChild(_f6.firstChild);
_f9++;
}
return _f9;
};
dojo.dom.copyChildren=function(_fa,_fb,_fc){
var _fd=_fa.cloneNode(true);
return this.moveChildren(_fd,_fb,_fc);
};
dojo.dom.replaceChildren=function(_fe,_ff){
var _100=[];
if(dojo.render.html.ie){
for(var i=0;i<_fe.childNodes.length;i++){
_100.push(_fe.childNodes[i]);
}
}
dojo.dom.removeChildren(_fe);
_fe.appendChild(_ff);
for(var i=0;i<_100.length;i++){
dojo.dom.destroyNode(_100[i]);
}
};
dojo.dom.removeChildren=function(node){
var _103=node.childNodes.length;
while(node.hasChildNodes()){
dojo.dom.removeNode(node.firstChild);
}
return _103;
};
dojo.dom.replaceNode=function(node,_105){
return node.parentNode.replaceChild(_105,node);
};
dojo.dom.destroyNode=function(node){
if(node.parentNode){
node=dojo.dom.removeNode(node);
}
if(node.nodeType!=3){
if(dojo.exists("dojo.event.browser.clean")){
dojo.event.browser.clean(node);
}
if(dojo.render.html.ie){
node.outerHTML="";
}
}
};
dojo.dom.removeNode=function(node){
if(node&&node.parentNode){
return node.parentNode.removeChild(node);
}
};
dojo.dom.getAncestors=function(node,_109,_10a){
var _10b=[];
var _10c=(_109&&(_109 instanceof Function||typeof _109=="function"));
while(node){
if(!_10c||_109(node)){
_10b.push(node);
}
if(_10a&&_10b.length>0){
return _10b[0];
}
node=node.parentNode;
}
if(_10a){
return null;
}
return _10b;
};
dojo.dom.getAncestorsByTag=function(node,tag,_10f){
tag=tag.toLowerCase();
return dojo.dom.getAncestors(node,function(el){
return ((el.tagName)&&(el.tagName.toLowerCase()==tag));
},_10f);
};
dojo.dom.getFirstAncestorByTag=function(node,tag){
return dojo.dom.getAncestorsByTag(node,tag,true);
};
dojo.dom.isDescendantOf=function(node,_114,_115){
if(_115&&node){
node=node.parentNode;
}
while(node){
if(node==_114){
return true;
}
node=node.parentNode;
}
return false;
};
dojo.dom.innerXML=function(node){
if(node.innerXML){
return node.innerXML;
}else{
if(node.xml){
return node.xml;
}else{
if(typeof XMLSerializer!="undefined"){
return (new XMLSerializer()).serializeToString(node);
}
}
}
};
dojo.dom.createDocument=function(){
var doc=null;
var _118=dojo.doc();
if(!dj_undef("ActiveXObject")){
var _119=["MSXML2","Microsoft","MSXML","MSXML3"];
for(var i=0;i<_119.length;i++){
try{
doc=new ActiveXObject(_119[i]+".XMLDOM");
}
catch(e){
}
if(doc){
break;
}
}
}else{
if((_118.implementation)&&(_118.implementation.createDocument)){
doc=_118.implementation.createDocument("","",null);
}
}
return doc;
};
dojo.dom.createDocumentFromText=function(str,_11c){
if(!_11c){
_11c="text/xml";
}
if(!dj_undef("DOMParser")){
var _11d=new DOMParser();
return _11d.parseFromString(str,_11c);
}else{
if(!dj_undef("ActiveXObject")){
var _11e=dojo.dom.createDocument();
if(_11e){
_11e.async=false;
_11e.loadXML(str);
return _11e;
}else{
dojo.debug("toXml didn't work?");
}
}else{
var _11f=dojo.doc();
if(_11f.createElement){
var tmp=_11f.createElement("xml");
tmp.innerHTML=str;
if(_11f.implementation&&_11f.implementation.createDocument){
var _121=_11f.implementation.createDocument("foo","",null);
for(var i=0;i<tmp.childNodes.length;i++){
_121.importNode(tmp.childNodes.item(i),true);
}
return _121;
}
return ((tmp.document)&&(tmp.document.firstChild?tmp.document.firstChild:tmp));
}
}
}
return null;
};
dojo.dom.prependChild=function(node,_124){
if(_124.firstChild){
_124.insertBefore(node,_124.firstChild);
}else{
_124.appendChild(node);
}
return true;
};
dojo.dom.insertBefore=function(node,ref,_127){
if((_127!=true)&&(node===ref||node.nextSibling===ref)){
return false;
}
var _128=ref.parentNode;
_128.insertBefore(node,ref);
return true;
};
dojo.dom.insertAfter=function(node,ref,_12b){
var pn=ref.parentNode;
if(ref==pn.lastChild){
if((_12b!=true)&&(node===ref)){
return false;
}
pn.appendChild(node);
}else{
return this.insertBefore(node,ref.nextSibling,_12b);
}
return true;
};
dojo.dom.insertAtPosition=function(node,ref,_12f){
if((!node)||(!ref)||(!_12f)){
return false;
}
switch(_12f.toLowerCase()){
case "before":
return dojo.dom.insertBefore(node,ref);
case "after":
return dojo.dom.insertAfter(node,ref);
case "first":
if(ref.firstChild){
return dojo.dom.insertBefore(node,ref.firstChild);
}else{
ref.appendChild(node);
return true;
}
break;
default:
ref.appendChild(node);
return true;
}
};
dojo.dom.insertAtIndex=function(node,_131,_132){
var _133=_131.childNodes;
if(!_133.length||_133.length==_132){
_131.appendChild(node);
return true;
}
if(_132==0){
return dojo.dom.prependChild(node,_131);
}
return dojo.dom.insertAfter(node,_133[_132-1]);
};
dojo.dom.textContent=function(node,text){
if(arguments.length>1){
var _136=dojo.doc();
dojo.dom.replaceChildren(node,_136.createTextNode(text));
return text;
}else{
if(node["textContent"]!=undefined){
return node.textContent;
}
var _137="";
if(node==null){
return _137;
}
var i=0,n;
while(n=node.childNodes[i++]){
switch(n.nodeType){
case 1:
case 5:
_137+=dojo.dom.textContent(n);
break;
case 3:
case 2:
case 4:
_137+=n.nodeValue;
break;
default:
break;
}
}
return _137;
}
};
dojo.dom.hasParent=function(node){
return Boolean(node&&node.parentNode&&dojo.dom.isNode(node.parentNode));
};
dojo.dom.isTag=function(node){
if(node&&node.tagName){
for(var i=1;i<arguments.length;i++){
if(node.tagName==String(arguments[i])){
return String(arguments[i]);
}
}
}
return "";
};
dojo.dom.setAttributeNS=function(elem,_13e,_13f,_140){
if(elem==null||((elem==undefined)&&(typeof elem=="undefined"))){
dojo.raise("No element given to dojo.dom.setAttributeNS");
}
if(!((elem.setAttributeNS==undefined)&&(typeof elem.setAttributeNS=="undefined"))){
elem.setAttributeNS(_13e,_13f,_140);
}else{
var _141=elem.ownerDocument;
var _142=_141.createNode(2,_13f,_13e);
_142.nodeValue=_140;
elem.setAttributeNode(_142);
}
};
dojo.provide("dojo.html.common");
dojo.lang.mixin(dojo.html,dojo.dom);
dojo.html.getEventTarget=function(evt){
if(!evt){
evt=dojo.global().event||{};
}
var t=(evt.srcElement?evt.srcElement:(evt.target?evt.target:null));
while((t)&&(t.nodeType!=1)){
t=t.parentNode;
}
return t;
};
dojo.html.getViewport=function(){
var _145=dojo.global();
var _146=dojo.doc();
var w=0;
var h=0;
if(dojo.render.html.mozilla){
w=_146.documentElement.clientWidth;
h=_145.innerHeight;
}else{
if(!dojo.render.html.opera&&_145.innerWidth){
w=_145.innerWidth;
h=_145.innerHeight;
}else{
if(!dojo.render.html.opera&&dojo.exists("documentElement.clientWidth",_146)){
var w2=_146.documentElement.clientWidth;
if(!w||w2&&w2<w){
w=w2;
}
h=_146.documentElement.clientHeight;
}else{
if(dojo.body().clientWidth){
w=dojo.body().clientWidth;
h=dojo.body().clientHeight;
}
}
}
}
return {width:w,height:h};
};
dojo.html.getScroll=function(){
var _14a=dojo.global();
var _14b=dojo.doc();
var top=_14a.pageYOffset||_14b.documentElement.scrollTop||dojo.body().scrollTop||0;
var left=_14a.pageXOffset||_14b.documentElement.scrollLeft||dojo.body().scrollLeft||0;
return {top:top,left:left,offset:{x:left,y:top}};
};
dojo.html.getParentByType=function(node,type){
var _150=dojo.doc();
var _151=dojo.byId(node);
type=type.toLowerCase();
while((_151)&&(_151.nodeName.toLowerCase()!=type)){
if(_151==(_150["body"]||_150["documentElement"])){
return null;
}
_151=_151.parentNode;
}
return _151;
};
dojo.html.getAttribute=function(node,attr){
node=dojo.byId(node);
if((!node)||(!node.getAttribute)){
return null;
}
var ta=typeof attr=="string"?attr:new String(attr);
var v=node.getAttribute(ta.toUpperCase());
if((v)&&(typeof v=="string")&&(v!="")){
return v;
}
if(v&&v.value){
return v.value;
}
if((node.getAttributeNode)&&(node.getAttributeNode(ta))){
return (node.getAttributeNode(ta)).value;
}else{
if(node.getAttribute(ta)){
return node.getAttribute(ta);
}else{
if(node.getAttribute(ta.toLowerCase())){
return node.getAttribute(ta.toLowerCase());
}
}
}
return null;
};
dojo.html.hasAttribute=function(node,attr){
return dojo.html.getAttribute(dojo.byId(node),attr)?true:false;
};
dojo.html.getCursorPosition=function(e){
e=e||dojo.global().event;
var _159={x:0,y:0};
if(e.pageX||e.pageY){
_159.x=e.pageX;
_159.y=e.pageY;
}else{
var de=dojo.doc().documentElement;
var db=dojo.body();
_159.x=e.clientX+((de||db)["scrollLeft"])-((de||db)["clientLeft"]);
_159.y=e.clientY+((de||db)["scrollTop"])-((de||db)["clientTop"]);
}
return _159;
};
dojo.html.isTag=function(node){
node=dojo.byId(node);
if(node&&node.tagName){
for(var i=1;i<arguments.length;i++){
if(node.tagName.toLowerCase()==String(arguments[i]).toLowerCase()){
return String(arguments[i]).toLowerCase();
}
}
}
return "";
};
if(dojo.render.html.ie&&!dojo.render.html.ie70){
if(window.location.href.substr(0,6).toLowerCase()!="https:"){
(function(){
var _15e=dojo.doc().createElement("script");
_15e.src="javascript:'dojo.html.createExternalElement=function(doc, tag){ return doc.createElement(tag); }'";
dojo.doc().getElementsByTagName("head")[0].appendChild(_15e);
})();
}
}else{
dojo.html.createExternalElement=function(doc,tag){
return doc.createElement(tag);
};
}
dojo.provide("dojo.uri.Uri");
dojo.uri=new function(){
this.dojoUri=function(uri){
return new dojo.uri.Uri(dojo.hostenv.getBaseScriptUri(),uri);
};
this.moduleUri=function(_162,uri){
var loc=dojo.hostenv.getModuleSymbols(_162).join("/");
if(!loc){
return null;
}
if(loc.lastIndexOf("/")!=loc.length-1){
loc+="/";
}
var _165=loc.indexOf(":");
var _166=loc.indexOf("/");
if(loc.charAt(0)!="/"&&(_165==-1||_165>_166)){
loc=dojo.hostenv.getBaseScriptUri()+loc;
}
return new dojo.uri.Uri(loc,uri);
};
this.Uri=function(){
var uri=arguments[0];
for(var i=1;i<arguments.length;i++){
if(!arguments[i]){
continue;
}
var _169=new dojo.uri.Uri(arguments[i].toString());
var _16a=new dojo.uri.Uri(uri.toString());
if((_169.path=="")&&(_169.scheme==null)&&(_169.authority==null)&&(_169.query==null)){
if(_169.fragment!=null){
_16a.fragment=_169.fragment;
}
_169=_16a;
}else{
if(_169.scheme==null){
_169.scheme=_16a.scheme;
if(_169.authority==null){
_169.authority=_16a.authority;
if(_169.path.charAt(0)!="/"){
var path=_16a.path.substring(0,_16a.path.lastIndexOf("/")+1)+_169.path;
var segs=path.split("/");
for(var j=0;j<segs.length;j++){
if(segs[j]=="."){
if(j==segs.length-1){
segs[j]="";
}else{
segs.splice(j,1);
j--;
}
}else{
if(j>0&&!(j==1&&segs[0]=="")&&segs[j]==".."&&segs[j-1]!=".."){
if(j==segs.length-1){
segs.splice(j,1);
segs[j-1]="";
}else{
segs.splice(j-1,2);
j-=2;
}
}
}
}
_169.path=segs.join("/");
}
}
}
}
uri="";
if(_169.scheme!=null){
uri+=_169.scheme+":";
}
if(_169.authority!=null){
uri+="//"+_169.authority;
}
uri+=_169.path;
if(_169.query!=null){
uri+="?"+_169.query;
}
if(_169.fragment!=null){
uri+="#"+_169.fragment;
}
}
this.uri=uri.toString();
var _16e="^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?$";
var r=this.uri.match(new RegExp(_16e));
this.scheme=r[2]||(r[1]?"":null);
this.authority=r[4]||(r[3]?"":null);
this.path=r[5];
this.query=r[7]||(r[6]?"":null);
this.fragment=r[9]||(r[8]?"":null);
if(this.authority!=null){
_16e="^((([^:]+:)?([^@]+))@)?([^:]*)(:([0-9]+))?$";
r=this.authority.match(new RegExp(_16e));
this.user=r[3]||null;
this.password=r[4]||null;
this.host=r[5];
this.port=r[7]||null;
}
this.toString=function(){
return this.uri;
};
};
};
dojo.provide("dojo.html.style");
dojo.html.getClass=function(node){
node=dojo.byId(node);
if(!node){
return "";
}
var cs="";
if(node.className){
cs=node.className;
}else{
if(dojo.html.hasAttribute(node,"class")){
cs=dojo.html.getAttribute(node,"class");
}
}
return cs.replace(/^\s+|\s+$/g,"");
};
dojo.html.getClasses=function(node){
var c=dojo.html.getClass(node);
return (c=="")?[]:c.split(/\s+/g);
};
dojo.html.hasClass=function(node,_175){
return (new RegExp("(^|\\s+)"+_175+"(\\s+|$)")).test(dojo.html.getClass(node));
};
dojo.html.prependClass=function(node,_177){
_177+=" "+dojo.html.getClass(node);
return dojo.html.setClass(node,_177);
};
dojo.html.addClass=function(node,_179){
if(dojo.html.hasClass(node,_179)){
return false;
}
_179=(dojo.html.getClass(node)+" "+_179).replace(/^\s+|\s+$/g,"");
return dojo.html.setClass(node,_179);
};
dojo.html.setClass=function(node,_17b){
node=dojo.byId(node);
var cs=new String(_17b);
try{
if(typeof node.className=="string"){
node.className=cs;
}else{
if(node.setAttribute){
node.setAttribute("class",_17b);
node.className=cs;
}else{
return false;
}
}
}
catch(e){
dojo.debug("dojo.html.setClass() failed",e);
}
return true;
};
dojo.html.removeClass=function(node,_17e,_17f){
try{
if(!_17f){
var _180=dojo.html.getClass(node).replace(new RegExp("(^|\\s+)"+_17e+"(\\s+|$)"),"$1$2");
}else{
var _180=dojo.html.getClass(node).replace(_17e,"");
}
dojo.html.setClass(node,_180);
}
catch(e){
dojo.debug("dojo.html.removeClass() failed",e);
}
return true;
};
dojo.html.replaceClass=function(node,_182,_183){
dojo.html.removeClass(node,_183);
dojo.html.addClass(node,_182);
};
dojo.html.classMatchType={ContainsAll:0,ContainsAny:1,IsOnly:2};
dojo.html.getElementsByClass=function(_184,_185,_186,_187,_188){
_188=false;
var _189=dojo.doc();
_185=dojo.byId(_185)||_189;
var _18a=_184.split(/\s+/g);
var _18b=[];
if(_187!=1&&_187!=2){
_187=0;
}
var _18c=new RegExp("(\\s|^)(("+_18a.join(")|(")+"))(\\s|$)");
var _18d=_18a.join(" ").length;
var _18e=[];
if(!_188&&_189.evaluate){
var _18f=".//"+(_186||"*")+"[contains(";
if(_187!=dojo.html.classMatchType.ContainsAny){
_18f+="concat(' ',@class,' '), ' "+_18a.join(" ') and contains(concat(' ',@class,' '), ' ")+" ')";
if(_187==2){
_18f+=" and string-length(@class)="+_18d+"]";
}else{
_18f+="]";
}
}else{
_18f+="concat(' ',@class,' '), ' "+_18a.join(" ') or contains(concat(' ',@class,' '), ' ")+" ')]";
}
var _190=_189.evaluate(_18f,_185,null,XPathResult.ANY_TYPE,null);
var _191=_190.iterateNext();
while(_191){
try{
_18e.push(_191);
_191=_190.iterateNext();
}
catch(e){
break;
}
}
return _18e;
}else{
if(!_186){
_186="*";
}
_18e=_185.getElementsByTagName(_186);
var node,i=0;
outer:
while(node=_18e[i++]){
var _194=dojo.html.getClasses(node);
if(_194.length==0){
continue outer;
}
var _195=0;
for(var j=0;j<_194.length;j++){
if(_18c.test(_194[j])){
if(_187==dojo.html.classMatchType.ContainsAny){
_18b.push(node);
continue outer;
}else{
_195++;
}
}else{
if(_187==dojo.html.classMatchType.IsOnly){
continue outer;
}
}
}
if(_195==_18a.length){
if((_187==dojo.html.classMatchType.IsOnly)&&(_195==_194.length)){
_18b.push(node);
}else{
if(_187==dojo.html.classMatchType.ContainsAll){
_18b.push(node);
}
}
}
}
return _18b;
}
};
dojo.html.getElementsByClassName=dojo.html.getElementsByClass;
dojo.html.toCamelCase=function(_197){
var arr=_197.split("-"),cc=arr[0];
for(var i=1;i<arr.length;i++){
cc+=arr[i].charAt(0).toUpperCase()+arr[i].substring(1);
}
return cc;
};
dojo.html.toSelectorCase=function(_19b){
return _19b.replace(/([A-Z])/g,"-$1").toLowerCase();
};
if(dojo.render.html.ie){
dojo.html.getComputedStyle=function(node,_19d,_19e){
node=dojo.byId(node);
if(!node||!node.currentStyle){
return _19e;
}
return node.currentStyle[dojo.html.toCamelCase(_19d)];
};
dojo.html.getComputedStyles=function(node){
return node.currentStyle;
};
}else{
dojo.html.getComputedStyle=function(node,_1a1,_1a2){
node=dojo.byId(node);
if(!node||!node.style){
return _1a2;
}
var s=node.ownerDocument.defaultView.getComputedStyle(node,null);
return (s&&s[dojo.html.toCamelCase(_1a1)])||"";
};
dojo.html.getComputedStyles=function(node){
return node.ownerDocument.defaultView.getComputedStyle(node,null);
};
}
dojo.html.getStyleProperty=function(node,_1a6){
node=dojo.byId(node);
return (node&&node.style?node.style[dojo.html.toCamelCase(_1a6)]:undefined);
};
dojo.html.getStyle=function(node,_1a8){
var _1a9=dojo.html.getStyleProperty(node,_1a8);
return (_1a9?_1a9:dojo.html.getComputedStyle(node,_1a8));
};
dojo.html.setStyle=function(node,_1ab,_1ac){
node=dojo.byId(node);
if(node&&node.style){
var _1ad=dojo.html.toCamelCase(_1ab);
node.style[_1ad]=_1ac;
}
};
dojo.html.setStyleText=function(_1ae,text){
try{
_1ae.style.cssText=text;
}
catch(e){
_1ae.setAttribute("style",text);
}
};
dojo.html.copyStyle=function(_1b0,_1b1){
if(!_1b1.style.cssText){
_1b0.setAttribute("style",_1b1.getAttribute("style"));
}else{
_1b0.style.cssText=_1b1.style.cssText;
}
dojo.html.addClass(_1b0,dojo.html.getClass(_1b1));
};
dojo.html.getUnitValue=function(node,_1b3,_1b4){
var s=dojo.html.getComputedStyle(node,_1b3);
if((!s)||((s=="auto")&&(_1b4))){
return {value:0,units:"px"};
}
var _1b6=s.match(/(\-?[\d.]+)([a-z%]*)/i);
if(!_1b6){
return dojo.html.getUnitValue.bad;
}
return {value:Number(_1b6[1]),units:_1b6[2].toLowerCase()};
};
dojo.html.getUnitValue.bad={value:NaN,units:""};
if(dojo.render.html.ie){
dojo.html.toPixelValue=function(_1b7,_1b8){
if(!_1b8){
return 0;
}
if(_1b8.slice(-2)=="px"){
return parseFloat(_1b8);
}
var _1b9=0;
with(_1b7){
var _1ba=style.left;
var _1bb=runtimeStyle.left;
runtimeStyle.left=currentStyle.left;
try{
style.left=_1b8||0;
_1b9=style.pixelLeft;
style.left=_1ba;
runtimeStyle.left=_1bb;
}
catch(e){
}
}
return _1b9;
};
}else{
dojo.html.toPixelValue=function(_1bc,_1bd){
return (_1bd&&(_1bd.slice(-2)=="px")?parseFloat(_1bd):0);
};
}
dojo.html.getPixelValue=function(node,_1bf,_1c0){
return dojo.html.toPixelValue(node,dojo.html.getComputedStyle(node,_1bf));
};
dojo.html.setPositivePixelValue=function(node,_1c2,_1c3){
if(isNaN(_1c3)){
return false;
}
node.style[_1c2]=Math.max(0,_1c3)+"px";
return true;
};
dojo.html.styleSheet=null;
dojo.html.insertCssRule=function(_1c4,_1c5,_1c6){
if(!dojo.html.styleSheet){
if(document.createStyleSheet){
dojo.html.styleSheet=document.createStyleSheet();
}else{
if(document.styleSheets[0]){
dojo.html.styleSheet=document.styleSheets[0];
}else{
return null;
}
}
}
var ss=dojo.html.styleSheet;
if(arguments.length<3){
if(ss.cssRules){
_1c6=ss.cssRules.length;
}else{
if(ss.rules){
_1c6=ss.rules.length;
}else{
return null;
}
}
}
if(ss.insertRule){
var rule=_1c4+" { "+_1c5+" }";
return ss.insertRule(rule,_1c6);
}else{
if(ss.addRule){
return ss.addRule(_1c4,_1c5,_1c6);
}else{
return null;
}
}
};
dojo.html.removeCssRule=function(_1c9){
var ss=dojo.html.styleSheet;
if(!ss){
dojo.debug("no stylesheet defined for removing rules");
return false;
}
if(dojo.render.html.ie){
if(!_1c9){
_1c9=ss.rules.length;
ss.removeRule(_1c9);
}
}else{
if(document.styleSheets[0]){
if(!_1c9){
_1c9=ss.cssRules.length;
}
ss.deleteRule(_1c9);
}
}
return true;
};
dojo.html._insertedCssFiles=[];
dojo.html.insertCssFile=function(URI,doc,_1cd,_1ce){
if(!URI){
return;
}
if(!doc){
doc=document;
}
var _1cf=dojo.hostenv.getText(URI,false,_1ce);
if(_1cf===null){
return;
}
_1cf=dojo.html.fixPathsInCssText(_1cf,URI);
if(_1cd){
var idx=-1,node,ent=dojo.html._insertedCssFiles;
for(var i=0;i<ent.length;i++){
if((ent[i].doc==doc)&&(ent[i].cssText==_1cf)){
idx=i;
node=ent[i].nodeRef;
break;
}
}
if(node){
var _1d4=doc.getElementsByTagName("style");
for(var i=0;i<_1d4.length;i++){
if(_1d4[i]==node){
return;
}
}
dojo.html._insertedCssFiles.shift(idx,1);
}
}
var _1d5=dojo.html.insertCssText(_1cf,doc);
dojo.html._insertedCssFiles.push({"doc":doc,"cssText":_1cf,"nodeRef":_1d5});
if(_1d5&&djConfig.isDebug){
_1d5.setAttribute("dbgHref",URI);
}
return _1d5;
};
dojo.html.insertCssText=function(_1d6,doc,URI){
if(!_1d6){
return;
}
if(!doc){
doc=document;
}
if(URI){
_1d6=dojo.html.fixPathsInCssText(_1d6,URI);
}
var _1d9=doc.createElement("style");
_1d9.setAttribute("type","text/css");
var head=doc.getElementsByTagName("head")[0];
if(!head){
dojo.debug("No head tag in document, aborting styles");
return;
}else{
head.appendChild(_1d9);
}
if(_1d9.styleSheet){
var _1db=function(){
try{
_1d9.styleSheet.cssText=_1d6;
}
catch(e){
dojo.debug(e);
}
};
if(_1d9.styleSheet.disabled){
setTimeout(_1db,10);
}else{
_1db();
}
}else{
var _1dc=doc.createTextNode(_1d6);
_1d9.appendChild(_1dc);
}
return _1d9;
};
dojo.html.fixPathsInCssText=function(_1dd,URI){
if(!_1dd||!URI){
return;
}
var _1df,str="",url="",_1e2="[\\t\\s\\w\\(\\)\\/\\.\\\\'\"-:#=&?~]+";
var _1e3=new RegExp("url\\(\\s*("+_1e2+")\\s*\\)");
var _1e4=/(file|https?|ftps?):\/\//;
regexTrim=new RegExp("^[\\s]*(['\"]?)("+_1e2+")\\1[\\s]*?$");
if(dojo.render.html.ie55||dojo.render.html.ie60){
var _1e5=new RegExp("AlphaImageLoader\\((.*)src=['\"]("+_1e2+")['\"]");
while(_1df=_1e5.exec(_1dd)){
url=_1df[2].replace(regexTrim,"$2");
if(!_1e4.exec(url)){
url=(new dojo.uri.Uri(URI,url).toString());
}
str+=_1dd.substring(0,_1df.index)+"AlphaImageLoader("+_1df[1]+"src='"+url+"'";
_1dd=_1dd.substr(_1df.index+_1df[0].length);
}
_1dd=str+_1dd;
str="";
}
while(_1df=_1e3.exec(_1dd)){
url=_1df[1].replace(regexTrim,"$2");
if(!_1e4.exec(url)){
url=(new dojo.uri.Uri(URI,url).toString());
}
str+=_1dd.substring(0,_1df.index)+"url("+url+")";
_1dd=_1dd.substr(_1df.index+_1df[0].length);
}
return str+_1dd;
};
dojo.html.setActiveStyleSheet=function(_1e6){
var i=0,a,els=dojo.doc().getElementsByTagName("link");
while(a=els[i++]){
if(a.getAttribute("rel").indexOf("style")!=-1&&a.getAttribute("title")){
a.disabled=true;
if(a.getAttribute("title")==_1e6){
a.disabled=false;
}
}
}
};
dojo.html.getActiveStyleSheet=function(){
var i=0,a,els=dojo.doc().getElementsByTagName("link");
while(a=els[i++]){
if((a.getAttribute("rel").indexOf("style")!=-1)&&(a.getAttribute("title"))&&(!a.disabled)){
return a.getAttribute("title");
}
}
return null;
};
dojo.html.getPreferredStyleSheet=function(){
var i=0,a,els=dojo.doc().getElementsByTagName("link");
while(a=els[i++]){
if((a.getAttribute("rel").indexOf("style")!=-1)&&(a.getAttribute("rel").indexOf("alt")==-1)&&(a.getAttribute("title"))){
return a.getAttribute("title");
}
}
return null;
};
dojo.html.applyBrowserClass=function(node){
var drh=dojo.render.html;
var _1f2={dj_ie:drh.ie,dj_ie55:drh.ie55,dj_ie6:drh.ie60,dj_ie7:drh.ie70,dj_iequirks:drh.ie&&drh.quirks,dj_opera:drh.opera,dj_opera8:drh.opera&&(Math.floor(dojo.render.version)==8),dj_opera9:drh.opera&&(Math.floor(dojo.render.version)==9),dj_khtml:drh.khtml,dj_safari:drh.safari,dj_gecko:drh.mozilla};
for(var p in _1f2){
if(_1f2[p]){
dojo.html.addClass(node,p);
}
}
};
dojo.kwCompoundRequire({common:["dojo.html.common","dojo.html.style"]});
dojo.provide("dojo.html.*");
dojo.provide("dojo.html.selection");
dojo.html.selectionType={NONE:0,TEXT:1,CONTROL:2};
dojo.html.clearSelection=function(){
var _1f4=dojo.global();
var _1f5=dojo.doc();
try{
if(_1f4["getSelection"]){
if(dojo.render.html.safari){
_1f4.getSelection().collapse();
}else{
_1f4.getSelection().removeAllRanges();
}
}else{
if(_1f5.selection){
if(_1f5.selection.empty){
_1f5.selection.empty();
}else{
if(_1f5.selection.clear){
_1f5.selection.clear();
}
}
}
}
return true;
}
catch(e){
dojo.debug(e);
return false;
}
};
dojo.html.disableSelection=function(_1f6){
_1f6=dojo.byId(_1f6)||dojo.body();
var h=dojo.render.html;
if(h.mozilla){
_1f6.style.MozUserSelect="none";
}else{
if(h.safari){
_1f6.style.KhtmlUserSelect="none";
}else{
if(h.ie){
_1f6.unselectable="on";
}else{
return false;
}
}
}
return true;
};
dojo.html.enableSelection=function(_1f8){
_1f8=dojo.byId(_1f8)||dojo.body();
var h=dojo.render.html;
if(h.mozilla){
_1f8.style.MozUserSelect="";
}else{
if(h.safari){
_1f8.style.KhtmlUserSelect="";
}else{
if(h.ie){
_1f8.unselectable="off";
}else{
return false;
}
}
}
return true;
};
dojo.html.selectInputText=function(_1fa){
var _1fb=dojo.global();
var _1fc=dojo.doc();
_1fa=dojo.byId(_1fa);
if(_1fc["selection"]&&dojo.body()["createTextRange"]){
var _1fd=_1fa.createTextRange();
_1fd.moveStart("character",0);
_1fd.moveEnd("character",_1fa.value.length);
_1fd.select();
}else{
if(_1fb["getSelection"]){
var _1fe=_1fb.getSelection();
_1fa.setSelectionRange(0,_1fa.value.length);
}
}
_1fa.focus();
};
dojo.lang.mixin(dojo.html.selection,{getType:function(){
if(dojo.doc()["selection"]){
return dojo.html.selectionType[dojo.doc().selection.type.toUpperCase()];
}else{
var _1ff=dojo.html.selectionType.TEXT;
var oSel;
try{
oSel=dojo.global().getSelection();
}
catch(e){
}
if(oSel&&oSel.rangeCount==1){
var _201=oSel.getRangeAt(0);
if(_201.startContainer==_201.endContainer&&(_201.endOffset-_201.startOffset)==1&&_201.startContainer.nodeType!=dojo.dom.TEXT_NODE){
_1ff=dojo.html.selectionType.CONTROL;
}
}
return _1ff;
}
},isCollapsed:function(){
var _202=dojo.global();
var _203=dojo.doc();
if(_203["selection"]){
return _203.selection.createRange().text=="";
}else{
if(_202["getSelection"]){
var _204=_202.getSelection();
if(dojo.lang.isString(_204)){
return _204=="";
}else{
return _204.isCollapsed||_204.toString()=="";
}
}
}
},getSelectedElement:function(){
if(dojo.html.selection.getType()==dojo.html.selectionType.CONTROL){
if(dojo.doc()["selection"]){
var _205=dojo.doc().selection.createRange();
if(_205&&_205.item){
return dojo.doc().selection.createRange().item(0);
}
}else{
var _206=dojo.global().getSelection();
return _206.anchorNode.childNodes[_206.anchorOffset];
}
}
},getParentElement:function(){
if(dojo.html.selection.getType()==dojo.html.selectionType.CONTROL){
var p=dojo.html.selection.getSelectedElement();
if(p){
return p.parentNode;
}
}else{
if(dojo.doc()["selection"]){
return dojo.doc().selection.createRange().parentElement();
}else{
var _208=dojo.global().getSelection();
if(_208){
var node=_208.anchorNode;
while(node&&node.nodeType!=dojo.dom.ELEMENT_NODE){
node=node.parentNode;
}
return node;
}
}
}
},getSelectedText:function(){
if(dojo.doc()["selection"]){
if(dojo.html.selection.getType()==dojo.html.selectionType.CONTROL){
return null;
}
return dojo.doc().selection.createRange().text;
}else{
var _20a=dojo.global().getSelection();
if(_20a){
return _20a.toString();
}
}
},getSelectedHtml:function(){
if(dojo.doc()["selection"]){
if(dojo.html.selection.getType()==dojo.html.selectionType.CONTROL){
return null;
}
return dojo.doc().selection.createRange().htmlText;
}else{
var _20b=dojo.global().getSelection();
if(_20b&&_20b.rangeCount){
var frag=_20b.getRangeAt(0).cloneContents();
var div=document.createElement("div");
div.appendChild(frag);
return div.innerHTML;
}
return null;
}
},hasAncestorElement:function(_20e){
return (dojo.html.selection.getAncestorElement.apply(this,arguments)!=null);
},getAncestorElement:function(_20f){
var node=dojo.html.selection.getSelectedElement()||dojo.html.selection.getParentElement();
return dojo.html.selection.getParentOfType(node,arguments);
},getParentOfType:function(node,tags){
while(node){
if(dojo.html.selection.isTag(node,tags).length>0){
return node;
}
node=node.parentNode;
}
return null;
},isTag:function(node,tags){
if(node&&node.tagName){
for(var i=0;i<tags.length;i++){
if(node.tagName.toLowerCase()==String(tags[i]).toLowerCase()){
return String(tags[i]).toLowerCase();
}
}
}
return "";
},selectElement:function(_216){
var _217=dojo.global();
var _218=dojo.doc();
_216=dojo.byId(_216);
if(_218.selection&&dojo.body().createTextRange){
try{
var _219=dojo.body().createControlRange();
_219.addElement(_216);
_219.select();
}
catch(e){
dojo.html.selection.selectElementChildren(_216);
}
}else{
if(_217["getSelection"]){
var _21a=_217.getSelection();
if(_21a["removeAllRanges"]){
var _219=_218.createRange();
_219.selectNode(_216);
_21a.removeAllRanges();
_21a.addRange(_219);
}
}
}
},selectElementChildren:function(_21b){
var _21c=dojo.global();
var _21d=dojo.doc();
_21b=dojo.byId(_21b);
if(_21d.selection&&dojo.body().createTextRange){
var _21e=_21b.ownerDocument.body.createTextRange();
_21e.moveToElementText(_21b);
_21e.select();
}else{
if(_21c["getSelection"]){
var _21f=_21c.getSelection();
if(_21f["setBaseAndExtent"]){
_21f.setBaseAndExtent(_21b,0,_21b,_21b.innerText.length-1);
}else{
if(_21f["selectAllChildren"]){
_21f.selectAllChildren(_21b);
}
}
}
}
},getBookmark:function(){
var _220;
var _221=dojo.doc();
if(_221["selection"]){
var _222=_221.selection.createRange();
if(_221.selection.type.toUpperCase()=="CONTROL"){
if(_222.length){
_220=[];
var i=0;
while(i<_222.length){
_220.push(_222.item(i++));
}
}else{
_220=null;
}
}else{
_220=_222.getBookmark();
}
}else{
var _224;
try{
_224=dojo.global().getSelection();
}
catch(e){
}
if(_224){
var _222=_224.getRangeAt(0);
_220=_222.cloneRange();
}else{
dojo.debug("No idea how to store the current selection for this browser!");
}
}
return _220;
},moveToBookmark:function(_225){
var _226=dojo.doc();
if(_226["selection"]){
if(dojo.lang.isArray(_225)){
var _227=_226.body.createControlRange();
var i=0;
while(i<_225.length){
_227.addElement(_225[i++]);
}
_227.select();
}else{
var _227=_226.selection.createRange();
_227.moveToBookmark(_225);
_227.select();
}
}else{
var _229;
try{
_229=dojo.global().getSelection();
}
catch(e){
}
if(_229&&_229["removeAllRanges"]){
_229.removeAllRanges();
_229.addRange(_225);
}else{
dojo.debug("No idea how to restore selection for this browser!");
}
}
},collapse:function(_22a){
if(dojo.global()["getSelection"]){
var _22b=dojo.global().getSelection();
if(_22b.removeAllRanges){
if(_22a){
_22b.collapseToStart();
}else{
_22b.collapseToEnd();
}
}else{
dojo.global().getSelection().collapse(_22a);
}
}else{
if(dojo.doc().selection){
var _22c=dojo.doc().selection.createRange();
_22c.collapse(_22a);
_22c.select();
}
}
},remove:function(){
if(dojo.doc().selection){
var _22d=dojo.doc().selection;
if(_22d.type.toUpperCase()!="NONE"){
_22d.clear();
}
return _22d;
}else{
var _22d=dojo.global().getSelection();
_22d.deleteFromDocument();
return _22d;
}
}});
dojo.provide("dojo.string.common");
dojo.string.trim=function(str,wh){
if(!str.replace){
return str;
}
if(!str.length){
return str;
}
var re=(wh>0)?(/^\s+/):(wh<0)?(/\s+$/):(/^\s+|\s+$/g);
return str.replace(re,"");
};
dojo.string.trimStart=function(str){
return dojo.string.trim(str,1);
};
dojo.string.trimEnd=function(str){
return dojo.string.trim(str,-1);
};
dojo.string.repeat=function(str,_234,_235){
var out="";
for(var i=0;i<_234;i++){
out+=str;
if(_235&&i<_234-1){
out+=_235;
}
}
return out;
};
dojo.string.pad=function(str,len,c,dir){
var out=String(str);
if(!c){
c="0";
}
if(!dir){
dir=1;
}
while(out.length<len){
if(dir>0){
out=c+out;
}else{
out+=c;
}
}
return out;
};
dojo.string.padLeft=function(str,len,c){
return dojo.string.pad(str,len,c,1);
};
dojo.string.padRight=function(str,len,c){
return dojo.string.pad(str,len,c,-1);
};
dojo.provide("dojo.string");
dojo.provide("dojo.lang.extras");
dojo.lang.setTimeout=function(func,_244){
var _245=window,_246=2;
if(!dojo.lang.isFunction(func)){
_245=func;
func=_244;
_244=arguments[2];
_246++;
}
if(dojo.lang.isString(func)){
func=_245[func];
}
var args=[];
for(var i=_246;i<arguments.length;i++){
args.push(arguments[i]);
}
return dojo.global().setTimeout(function(){
func.apply(_245,args);
},_244);
};
dojo.lang.clearTimeout=function(_249){
dojo.global().clearTimeout(_249);
};
dojo.lang.getNameInObj=function(ns,item){
if(!ns){
ns=dj_global;
}
for(var x in ns){
if(ns[x]===item){
return new String(x);
}
}
return null;
};
dojo.lang.shallowCopy=function(obj,deep){
var i,ret;
if(obj===null){
return null;
}
if(dojo.lang.isObject(obj)){
ret=new obj.constructor();
for(i in obj){
if(dojo.lang.isUndefined(ret[i])){
ret[i]=deep?dojo.lang.shallowCopy(obj[i],deep):obj[i];
}
}
}else{
if(dojo.lang.isArray(obj)){
ret=[];
for(i=0;i<obj.length;i++){
ret[i]=deep?dojo.lang.shallowCopy(obj[i],deep):obj[i];
}
}else{
ret=obj;
}
}
return ret;
};
dojo.lang.firstValued=function(){
for(var i=0;i<arguments.length;i++){
if(typeof arguments[i]!="undefined"){
return arguments[i];
}
}
return undefined;
};
dojo.lang.getObjPathValue=function(_252,_253,_254){
dojo.deprecated("dojo.lang.getObjPathValue","use dojo.getObject","0.6");
with(dojo.parseObjPath(_252,_253,_254)){
return dojo.evalProp(prop,obj,_254);
}
};
dojo.lang.setObjPathValue=function(_255,_256,_257,_258){
dojo.deprecated("dojo.lang.setObjPathValue","use dojo.parseObjPath and the '=' operator","0.6");
if(arguments.length<4){
_258=true;
}
with(dojo.parseObjPath(_255,_257,_258)){
if(obj&&(_258||(prop in obj))){
obj[prop]=_256;
}
}
};
dojo.provide("dojo.io.common");
dojo.io.transports=[];
dojo.io.hdlrFuncNames=["load","error","timeout"];
dojo.io.Request=function(url,_25a,_25b,_25c){
if((arguments.length==1)&&(arguments[0].constructor==Object)){
this.fromKwArgs(arguments[0]);
}else{
this.url=url;
if(_25a){
this.mimetype=_25a;
}
if(_25b){
this.transport=_25b;
}
if(arguments.length>=4){
this.changeUrl=_25c;
}
}
};
dojo.lang.extend(dojo.io.Request,{url:"",mimetype:"text/plain",method:"GET",content:undefined,transport:undefined,changeUrl:undefined,formNode:undefined,sync:false,bindSuccess:false,useCache:false,preventCache:false,jsonFilter:function(_25d){
if((this.mimetype=="text/json-comment-filtered")||(this.mimetype=="application/json-comment-filtered")){
var _25e=_25d.indexOf("/*");
var _25f=_25d.lastIndexOf("*/");
if((_25e==-1)||(_25f==-1)){
dojo.debug("your JSON wasn't comment filtered!");
return "";
}
return _25d.substring(_25e+2,_25f);
}
dojo.debug("please consider using a mimetype of text/json-comment-filtered to avoid potential security issues with JSON endpoints");
return _25d;
},load:function(type,data,_262,_263){
},error:function(type,_265,_266,_267){
},timeout:function(type,_269,_26a,_26b){
},handle:function(type,data,_26e,_26f){
},timeoutSeconds:0,abort:function(){
},fromKwArgs:function(_270){
if(_270["url"]){
_270.url=_270.url.toString();
}
if(_270["formNode"]){
_270.formNode=dojo.byId(_270.formNode);
}
if(!_270["method"]&&_270["formNode"]&&_270["formNode"].method){
_270.method=_270["formNode"].method;
}
if(!_270["handle"]&&_270["handler"]){
_270.handle=_270.handler;
}
if(!_270["load"]&&_270["loaded"]){
_270.load=_270.loaded;
}
if(!_270["changeUrl"]&&_270["changeURL"]){
_270.changeUrl=_270.changeURL;
}
_270.encoding=dojo.lang.firstValued(_270["encoding"],djConfig["bindEncoding"],"");
_270.sendTransport=dojo.lang.firstValued(_270["sendTransport"],djConfig["ioSendTransport"],false);
var _271=dojo.lang.isFunction;
for(var x=0;x<dojo.io.hdlrFuncNames.length;x++){
var fn=dojo.io.hdlrFuncNames[x];
if(_270[fn]&&_271(_270[fn])){
continue;
}
if(_270["handle"]&&_271(_270["handle"])){
_270[fn]=_270.handle;
}
}
dojo.lang.mixin(this,_270);
}});
dojo.io.Error=function(msg,type,num){
this.message=msg;
this.type=type||"unknown";
this.number=num||0;
};
dojo.io.transports.addTransport=function(name){
this.push(name);
this[name]=dojo.io[name];
};
dojo.io.bind=function(_278){
if(!(_278 instanceof dojo.io.Request)){
try{
_278=new dojo.io.Request(_278);
}
catch(e){
dojo.debug(e);
}
}
var _279="";
if(_278["transport"]){
_279=_278["transport"];
if(!this[_279]){
dojo.io.sendBindError(_278,"No dojo.io.bind() transport with name '"+_278["transport"]+"'.");
return _278;
}
if(!this[_279].canHandle(_278)){
dojo.io.sendBindError(_278,"dojo.io.bind() transport with name '"+_278["transport"]+"' cannot handle this type of request.");
return _278;
}
}else{
for(var x=0;x<dojo.io.transports.length;x++){
var tmp=dojo.io.transports[x];
if((this[tmp])&&(this[tmp].canHandle(_278))){
_279=tmp;
break;
}
}
if(_279==""){
dojo.io.sendBindError(_278,"None of the loaded transports for dojo.io.bind()"+" can handle the request.");
return _278;
}
}
this[_279].bind(_278);
_278.bindSuccess=true;
return _278;
};
dojo.io.sendBindError=function(_27c,_27d){
if((typeof _27c.error=="function"||typeof _27c.handle=="function")&&(typeof setTimeout=="function"||typeof setTimeout=="object")){
var _27e=new dojo.io.Error(_27d);
setTimeout(function(){
_27c[(typeof _27c.error=="function")?"error":"handle"]("error",_27e,null,_27c);
},50);
}else{
dojo.raise(_27d);
}
};
dojo.io.queueBind=function(_27f){
if(!(_27f instanceof dojo.io.Request)){
try{
_27f=new dojo.io.Request(_27f);
}
catch(e){
dojo.debug(e);
}
}
var _280=_27f.load;
_27f.load=function(){
dojo.io._queueBindInFlight=false;
var ret=_280.apply(this,arguments);
dojo.io._dispatchNextQueueBind();
return ret;
};
var _282=_27f.error;
_27f.error=function(){
dojo.io._queueBindInFlight=false;
var ret=_282.apply(this,arguments);
dojo.io._dispatchNextQueueBind();
return ret;
};
dojo.io._bindQueue.push(_27f);
dojo.io._dispatchNextQueueBind();
return _27f;
};
dojo.io._dispatchNextQueueBind=function(){
if(!dojo.io._queueBindInFlight){
dojo.io._queueBindInFlight=true;
if(dojo.io._bindQueue.length>0){
dojo.io.bind(dojo.io._bindQueue.shift());
}else{
dojo.io._queueBindInFlight=false;
}
}
};
dojo.io._bindQueue=[];
dojo.io._queueBindInFlight=false;
dojo.io.argsFromMap=function(map,_285,last){
var enc=/utf/i.test(_285||"")?encodeURIComponent:dojo.string.encodeAscii;
var _288=[];
var _289=new Object();
for(var name in map){
var _28b=function(elt){
var val=enc(name)+"="+enc(elt);
_288[(last==name)?"push":"unshift"](val);
};
if(!_289[name]){
var _28e=map[name];
if(dojo.lang.isArray(_28e)){
dojo.lang.forEach(_28e,_28b);
}else{
_28b(_28e);
}
}
}
return _288.join("&");
};
dojo.io.setIFrameSrc=function(_28f,src,_291){
try{
var r=dojo.render.html;
if(!_291){
if(r.safari){
_28f.location=src;
}else{
frames[_28f.name].location=src;
}
}else{
var idoc;
if(r.ie){
idoc=_28f.contentWindow.document;
}else{
if(r.safari){
idoc=_28f.document;
}else{
idoc=_28f.contentWindow;
}
}
if(!idoc){
_28f.location=src;
return;
}else{
idoc.location.replace(src);
}
}
}
catch(e){
dojo.debug(e);
dojo.debug("setIFrameSrc: "+e);
}
};
dojo.provide("dojo.lang.array");
dojo.lang.mixin(dojo.lang,{has:function(obj,name){
try{
return typeof obj[name]!="undefined";
}
catch(e){
return false;
}
},isEmpty:function(obj){
if(dojo.lang.isArrayLike(obj)||dojo.lang.isString(obj)){
return obj.length===0;
}else{
if(dojo.lang.isObject(obj)){
var tmp={};
for(var x in obj){
if(obj[x]&&(!tmp[x])){
return false;
}
}
return true;
}
}
},map:function(arr,obj,_29b){
var _29c=dojo.lang.isString(arr);
if(_29c){
arr=arr.split("");
}
if(dojo.lang.isFunction(obj)&&(!_29b)){
_29b=obj;
obj=dj_global;
}else{
if(dojo.lang.isFunction(obj)&&_29b){
var _29d=obj;
obj=_29b;
_29b=_29d;
}
}
if(Array.map){
var _29e=Array.map(arr,_29b,obj);
}else{
var _29e=[];
for(var i=0;i<arr.length;++i){
_29e.push(_29b.call(obj,arr[i]));
}
}
if(_29c){
return _29e.join("");
}else{
return _29e;
}
},reduce:function(arr,_2a1,_2a2,_2a3){
var _2a4=_2a2;
if(arguments.length==2){
_2a4=arr[0];
arr=arr.slice(1);
}
var ob=_2a3||dj_global;
dojo.lang.map(arr,function(val){
_2a4=_2a1.call(ob,_2a4,val);
});
return _2a4;
},forEach:function(_2a7,_2a8,_2a9){
if(dojo.lang.isString(_2a7)){
_2a7=_2a7.split("");
}
if(Array.forEach){
Array.forEach(_2a7,_2a8,_2a9);
}else{
if(!_2a9){
_2a9=dj_global;
}
for(var i=0,l=_2a7.length;i<l;i++){
_2a8.call(_2a9,_2a7[i],i,_2a7);
}
}
},_everyOrSome:function(_2ac,arr,_2ae,_2af){
if(dojo.lang.isString(arr)){
arr=arr.split("");
}
if(Array.every){
return Array[_2ac?"every":"some"](arr,_2ae,_2af);
}else{
if(!_2af){
_2af=dj_global;
}
for(var i=0,l=arr.length;i<l;i++){
var _2b2=_2ae.call(_2af,arr[i],i,arr);
if(_2ac&&!_2b2){
return false;
}else{
if((!_2ac)&&(_2b2)){
return true;
}
}
}
return Boolean(_2ac);
}
},every:function(arr,_2b4,_2b5){
return this._everyOrSome(true,arr,_2b4,_2b5);
},some:function(arr,_2b7,_2b8){
return this._everyOrSome(false,arr,_2b7,_2b8);
},filter:function(arr,_2ba,_2bb){
var _2bc=dojo.lang.isString(arr);
if(_2bc){
arr=arr.split("");
}
var _2bd;
if(Array.filter){
_2bd=Array.filter(arr,_2ba,_2bb);
}else{
if(!_2bb){
if(arguments.length>=3){
dojo.raise("thisObject doesn't exist!");
}
_2bb=dj_global;
}
_2bd=[];
for(var i=0;i<arr.length;i++){
if(_2ba.call(_2bb,arr[i],i,arr)){
_2bd.push(arr[i]);
}
}
}
if(_2bc){
return _2bd.join("");
}else{
return _2bd;
}
},unnest:function(){
var out=[];
for(var i=0;i<arguments.length;i++){
if(dojo.lang.isArrayLike(arguments[i])){
var add=dojo.lang.unnest.apply(this,arguments[i]);
out=out.concat(add);
}else{
out.push(arguments[i]);
}
}
return out;
},toArray:function(_2c2,_2c3){
var _2c4=[];
for(var i=_2c3||0;i<_2c2.length;i++){
_2c4.push(_2c2[i]);
}
return _2c4;
}});
dojo.provide("dojo.lang.func");
dojo.lang.hitch=function(_2c6,_2c7){
var args=[];
for(var x=2;x<arguments.length;x++){
args.push(arguments[x]);
}
var fcn=(dojo.lang.isString(_2c7)?_2c6[_2c7]:_2c7)||function(){
};
return function(){
var ta=args.concat([]);
for(var x=0;x<arguments.length;x++){
ta.push(arguments[x]);
}
return fcn.apply(_2c6,ta);
};
};
dojo.lang.anonCtr=0;
dojo.lang.anon={};
dojo.lang.nameAnonFunc=function(_2cd,_2ce,_2cf){
var isIE=(dojo.render.html.capable&&dojo.render.html["ie"]);
var jpn="$joinpoint";
var nso=(_2ce||dojo.lang.anon);
if(isIE){
var cn=_2cd["__dojoNameCache"];
if(cn&&nso[cn]===_2cd){
return _2cd["__dojoNameCache"];
}else{
if(cn){
var _2d4=cn.indexOf(jpn);
if(_2d4!=-1){
return cn.substring(0,_2d4);
}
}
}
}
if((_2cf)||((dj_global["djConfig"])&&(djConfig["slowAnonFuncLookups"]==true))){
for(var x in nso){
try{
if(nso[x]===_2cd){
if(isIE){
_2cd["__dojoNameCache"]=x;
var _2d4=x.indexOf(jpn);
if(_2d4!=-1){
x=x.substring(0,_2d4);
}
}
return x;
}
}
catch(e){
}
}
}
var ret="__"+dojo.lang.anonCtr++;
while(typeof nso[ret]!="undefined"){
ret="__"+dojo.lang.anonCtr++;
}
nso[ret]=_2cd;
return ret;
};
dojo.lang.forward=function(_2d7){
return function(){
return this[_2d7].apply(this,arguments);
};
};
dojo.lang.curry=function(_2d8,func){
var _2da=[];
_2d8=_2d8||dj_global;
if(dojo.lang.isString(func)){
func=_2d8[func];
}
for(var x=2;x<arguments.length;x++){
_2da.push(arguments[x]);
}
var _2dc=(func["__preJoinArity"]||func.length)-_2da.length;
function gather(_2dd,_2de,_2df){
var _2e0=_2df;
var _2e1=_2de.slice(0);
for(var x=0;x<_2dd.length;x++){
_2e1.push(_2dd[x]);
}
_2df=_2df-_2dd.length;
if(_2df<=0){
var res=func.apply(_2d8,_2e1);
_2df=_2e0;
return res;
}else{
return function(){
return gather(arguments,_2e1,_2df);
};
}
}
return gather([],_2da,_2dc);
};
dojo.lang.curryArguments=function(_2e4,func,args,_2e7){
var _2e8=[];
var x=_2e7||0;
for(x=_2e7;x<args.length;x++){
_2e8.push(args[x]);
}
return dojo.lang.curry.apply(dojo.lang,[_2e4,func].concat(_2e8));
};
dojo.lang.tryThese=function(){
for(var x=0;x<arguments.length;x++){
try{
if(typeof arguments[x]=="function"){
var ret=(arguments[x]());
if(ret){
return ret;
}
}
}
catch(e){
dojo.debug(e);
}
}
};
dojo.lang.delayThese=function(farr,cb,_2ee,_2ef){
if(!farr.length){
if(typeof _2ef=="function"){
_2ef();
}
return;
}
if((typeof _2ee=="undefined")&&(typeof cb=="number")){
_2ee=cb;
cb=function(){
};
}else{
if(!cb){
cb=function(){
};
if(!_2ee){
_2ee=0;
}
}
}
setTimeout(function(){
(farr.shift())();
cb();
dojo.lang.delayThese(farr,cb,_2ee,_2ef);
},_2ee);
};
dojo.provide("dojo.string.extras");
dojo.string.substitute=function(_2f0,map,_2f2,_2f3){
return _2f0.replace(/\$\{([^\s\:\}]+)(?:\:(\S+))?\}/g,function(_2f4,key,_2f6){
var _2f7=dojo.getObject(key,false,map).toString();
if(_2f6){
_2f7=dojo.getObject(_2f6,false,_2f3)(_2f7);
}
if(_2f2){
_2f7=_2f2(_2f7);
}
return _2f7;
});
};
dojo.string.capitalize=function(str){
if(!dojo.lang.isString(str)){
return "";
}
return str.replace(/[^\s]+/g,function(word){
return word.substring(0,1).toUpperCase()+word.substring(1);
});
};
dojo.string.isBlank=function(str){
if(!dojo.lang.isString(str)){
return true;
}
return (dojo.string.trim(str).length==0);
};
dojo.string.encodeAscii=function(str){
if(!dojo.lang.isString(str)){
return str;
}
var ret="";
var _2fd=escape(str);
var _2fe,re=/%u([0-9A-F]{4})/i;
while((_2fe=_2fd.match(re))){
var num=Number("0x"+_2fe[1]);
var _301=escape("&#"+num+";");
ret+=_2fd.substring(0,_2fe.index)+_301;
_2fd=_2fd.substring(_2fe.index+_2fe[0].length);
}
ret+=_2fd.replace(/\+/g,"%2B");
return ret;
};
dojo.string.escape=function(type,str){
var args=dojo.lang.toArray(arguments,1);
switch(type.toLowerCase()){
case "xml":
case "html":
case "xhtml":
return dojo.string.escapeXml.apply(this,args);
case "sql":
return dojo.string.escapeSql.apply(this,args);
case "regexp":
case "regex":
return dojo.string.escapeRegExp.apply(this,args);
case "javascript":
case "jscript":
case "js":
return dojo.string.escapeJavaScript.apply(this,args);
case "ascii":
return dojo.string.encodeAscii.apply(this,args);
default:
return str;
}
};
dojo.string.escapeXml=function(str,_306){
str=str.replace(/&/gm,"&amp;").replace(/</gm,"&lt;").replace(/>/gm,"&gt;").replace(/"/gm,"&quot;");
if(!_306){
str=str.replace(/'/gm,"&#39;");
}
return str;
};
dojo.string.escapeSql=function(str){
return str.replace(/'/gm,"''");
};
dojo.string.escapeRegExp=function(str,_309){
return str.replace(/([\.$?*!=:|{}\(\)\[\]\\\/^])/g,function(ch){
if(_309&&_309.indexOf(ch)!=-1){
return ch;
}
return "\\"+ch;
});
};
dojo.string.escapeJavaScript=function(str){
return str.replace(/(["'\f\b\n\t\r])/gm,"\\$1");
};
dojo.string.escapeString=function(str){
return ("\""+str.replace(/(["\\])/g,"\\$1")+"\"").replace(/[\f]/g,"\\f").replace(/[\b]/g,"\\b").replace(/[\n]/g,"\\n").replace(/[\t]/g,"\\t").replace(/[\r]/g,"\\r");
};
dojo.string.summary=function(str,len){
if(!len||str.length<=len){
return str;
}
return str.substring(0,len).replace(/\.+$/,"")+"...";
};
dojo.string.endsWith=function(str,end,_311){
if(_311){
str=str.toLowerCase();
end=end.toLowerCase();
}
if((str.length-end.length)<0){
return false;
}
return str.lastIndexOf(end)==str.length-end.length;
};
dojo.string.endsWithAny=function(str){
for(var i=1;i<arguments.length;i++){
if(dojo.string.endsWith(str,arguments[i])){
return true;
}
}
return false;
};
dojo.string.startsWith=function(str,_315,_316){
if(_316){
str=str.toLowerCase();
_315=_315.toLowerCase();
}
return str.indexOf(_315)==0;
};
dojo.string.startsWithAny=function(str){
for(var i=1;i<arguments.length;i++){
if(dojo.string.startsWith(str,arguments[i])){
return true;
}
}
return false;
};
dojo.string.has=function(str){
for(var i=1;i<arguments.length;i++){
if(str.indexOf(arguments[i])>-1){
return true;
}
}
return false;
};
dojo.string.normalizeNewlines=function(text,_31c){
if(_31c=="\n"){
text=text.replace(/\r\n/g,"\n");
text=text.replace(/\r/g,"\n");
}else{
if(_31c=="\r"){
text=text.replace(/\r\n/g,"\r");
text=text.replace(/\n/g,"\r");
}else{
text=text.replace(/([^\r])\n/g,"$1\r\n").replace(/\r([^\n])/g,"\r\n$1");
}
}
return text;
};
dojo.string.splitEscaped=function(str,_31e){
var _31f=[];
for(var i=0,_321=0;i<str.length;i++){
if(str.charAt(i)=="\\"){
i++;
continue;
}
if(str.charAt(i)==_31e){
_31f.push(str.substring(_321,i));
_321=i+1;
}
}
_31f.push(str.substr(_321));
return _31f;
};
dojo.provide("dojo.undo.browser");
try{
if((!djConfig["preventBackButtonFix"])&&(!dojo.hostenv.post_load_)){
document.write("<iframe style='border: 0px; width: 1px; height: 1px; position: absolute; bottom: 0px; right: 0px; visibility: visible;' name='djhistory' id='djhistory' src='"+(djConfig["dojoIframeHistoryUrl"]||dojo.hostenv.getBaseScriptUri()+"iframe_history.html")+"'></iframe>");
}
}
catch(e){
}
if(dojo.render.html.opera){
dojo.debug("Opera is not supported with dojo.undo.browser, so back/forward detection will not work.");
}
dojo.undo.browser={initialHref:(!dj_undef("window"))?window.location.href:"",initialHash:(!dj_undef("window"))?window.location.hash:"",moveForward:false,historyStack:[],forwardStack:[],historyIframe:null,bookmarkAnchor:null,locationTimer:null,setInitialState:function(args){
this.initialState=this._createState(this.initialHref,args,this.initialHash);
},addToHistory:function(args){
this.forwardStack=[];
var hash=null;
var url=null;
if(!this.historyIframe){
if(djConfig["useXDomain"]&&!djConfig["dojoIframeHistoryUrl"]){
dojo.debug("dojo.undo.browser: When using cross-domain Dojo builds,"+" please save iframe_history.html to your domain and set djConfig.dojoIframeHistoryUrl"+" to the path on your domain to iframe_history.html");
}
this.historyIframe=window.frames["djhistory"];
}
if(!this.bookmarkAnchor){
this.bookmarkAnchor=document.createElement("a");
dojo.body().appendChild(this.bookmarkAnchor);
this.bookmarkAnchor.style.display="none";
}
if(args["changeUrl"]){
hash="#"+((args["changeUrl"]!==true)?args["changeUrl"]:(new Date()).getTime());
if(this.historyStack.length==0&&this.initialState.urlHash==hash){
this.initialState=this._createState(url,args,hash);
return;
}else{
if(this.historyStack.length>0&&this.historyStack[this.historyStack.length-1].urlHash==hash){
this.historyStack[this.historyStack.length-1]=this._createState(url,args,hash);
return;
}
}
this.changingUrl=true;
setTimeout("window.location.href = '"+hash+"'; dojo.undo.browser.changingUrl = false;",1);
this.bookmarkAnchor.href=hash;
if(dojo.render.html.ie){
url=this._loadIframeHistory();
var _326=args["back"]||args["backButton"]||args["handle"];
var tcb=function(_328){
if(window.location.hash!=""){
setTimeout("window.location.href = '"+hash+"';",1);
}
_326.apply(this,[_328]);
};
if(args["back"]){
args.back=tcb;
}else{
if(args["backButton"]){
args.backButton=tcb;
}else{
if(args["handle"]){
args.handle=tcb;
}
}
}
var _329=args["forward"]||args["forwardButton"]||args["handle"];
var tfw=function(_32b){
if(window.location.hash!=""){
window.location.href=hash;
}
if(_329){
_329.apply(this,[_32b]);
}
};
if(args["forward"]){
args.forward=tfw;
}else{
if(args["forwardButton"]){
args.forwardButton=tfw;
}else{
if(args["handle"]){
args.handle=tfw;
}
}
}
}else{
if(dojo.render.html.moz){
if(!this.locationTimer){
this.locationTimer=setInterval("dojo.undo.browser.checkLocation();",200);
}
}
}
}else{
url=this._loadIframeHistory();
}
this.historyStack.push(this._createState(url,args,hash));
},checkLocation:function(){
if(!this.changingUrl){
var hsl=this.historyStack.length;
if((window.location.hash==this.initialHash||window.location.href==this.initialHref)&&(hsl==1)){
this.handleBackButton();
return;
}
if(this.forwardStack.length>0){
if(this.forwardStack[this.forwardStack.length-1].urlHash==window.location.hash){
this.handleForwardButton();
return;
}
}
if((hsl>=2)&&(this.historyStack[hsl-2])){
if(this.historyStack[hsl-2].urlHash==window.location.hash){
this.handleBackButton();
return;
}
}
}
},iframeLoaded:function(evt,_32e){
if(!dojo.render.html.opera){
var _32f=this._getUrlQuery(_32e.href);
if(_32f==null){
if(this.historyStack.length==1){
this.handleBackButton();
}
return;
}
if(this.moveForward){
this.moveForward=false;
return;
}
if(this.historyStack.length>=2&&_32f==this._getUrlQuery(this.historyStack[this.historyStack.length-2].url)){
this.handleBackButton();
}else{
if(this.forwardStack.length>0&&_32f==this._getUrlQuery(this.forwardStack[this.forwardStack.length-1].url)){
this.handleForwardButton();
}
}
}
},handleBackButton:function(){
var _330=this.historyStack.pop();
if(!_330){
return;
}
var last=this.historyStack[this.historyStack.length-1];
if(!last&&this.historyStack.length==0){
last=this.initialState;
}
if(last){
if(last.kwArgs["back"]){
last.kwArgs["back"]();
}else{
if(last.kwArgs["backButton"]){
last.kwArgs["backButton"]();
}else{
if(last.kwArgs["handle"]){
last.kwArgs.handle("back");
}
}
}
}
this.forwardStack.push(_330);
},handleForwardButton:function(){
var last=this.forwardStack.pop();
if(!last){
return;
}
if(last.kwArgs["forward"]){
last.kwArgs.forward();
}else{
if(last.kwArgs["forwardButton"]){
last.kwArgs.forwardButton();
}else{
if(last.kwArgs["handle"]){
last.kwArgs.handle("forward");
}
}
}
this.historyStack.push(last);
},_createState:function(url,args,hash){
return {"url":url,"kwArgs":args,"urlHash":hash};
},_getUrlQuery:function(url){
var _337=url.split("?");
if(_337.length<2){
return null;
}else{
return _337[1];
}
},_loadIframeHistory:function(){
var url=(djConfig["dojoIframeHistoryUrl"]||dojo.hostenv.getBaseScriptUri()+"iframe_history.html")+"?"+(new Date()).getTime();
this.moveForward=true;
dojo.io.setIFrameSrc(this.historyIframe,url,false);
return url;
}};
dojo.provide("dojo.io.BrowserIO");
if(!dj_undef("window")){
dojo.io.checkChildrenForFile=function(node){
var _33a=false;
var _33b=node.getElementsByTagName("input");
dojo.lang.forEach(_33b,function(_33c){
if(_33a){
return;
}
if(_33c.getAttribute("type")=="file"){
_33a=true;
}
});
return _33a;
};
dojo.io.formHasFile=function(_33d){
return dojo.io.checkChildrenForFile(_33d);
};
dojo.io.updateNode=function(node,_33f){
node=dojo.byId(node);
var args=_33f;
if(dojo.lang.isString(_33f)){
args={url:_33f};
}
args.mimetype="text/html";
args.load=function(t,d,e){
while(node.firstChild){
dojo.dom.destroyNode(node.firstChild);
}
node.innerHTML=d;
};
dojo.io.bind(args);
};
dojo.io.formFilter=function(node){
var type=(node.type||"").toLowerCase();
return !node.disabled&&node.name&&!dojo.lang.inArray(["file","submit","image","reset","button"],type);
};
dojo.io.encodeForm=function(_346,_347,_348){
if((!_346)||(!_346.tagName)||(!_346.tagName.toLowerCase()=="form")){
dojo.raise("Attempted to encode a non-form element.");
}
if(!_348){
_348=dojo.io.formFilter;
}
var enc=/utf/i.test(_347||"")?encodeURIComponent:dojo.string.encodeAscii;
var _34a=[];
for(var i=0;i<_346.elements.length;i++){
var elm=_346.elements[i];
if(!elm||elm.tagName.toLowerCase()=="fieldset"||!_348(elm)){
continue;
}
var name=enc(elm.name);
var type=elm.type.toLowerCase();
if(type=="select-multiple"){
for(var j=0;j<elm.options.length;j++){
if(elm.options[j].selected){
_34a.push(name+"="+enc(elm.options[j].value));
}
}
}else{
if(dojo.lang.inArray(["radio","checkbox"],type)){
if(elm.checked){
_34a.push(name+"="+enc(elm.value));
}
}else{
_34a.push(name+"="+enc(elm.value));
}
}
}
var _350=_346.getElementsByTagName("input");
for(var i=0;i<_350.length;i++){
var _351=_350[i];
if(_351.type.toLowerCase()=="image"&&_351.form==_346&&_348(_351)){
var name=enc(_351.name);
_34a.push(name+"="+enc(_351.value));
_34a.push(name+".x=0");
_34a.push(name+".y=0");
}
}
return _34a.join("&")+"&";
};
dojo.io.FormBind=function(args){
this.bindArgs={};
if(args&&args.formNode){
this.init(args);
}else{
if(args){
this.init({formNode:args});
}
}
};
dojo.lang.extend(dojo.io.FormBind,{form:null,bindArgs:null,clickedButton:null,init:function(args){
var form=dojo.byId(args.formNode);
if(!form||!form.tagName||form.tagName.toLowerCase()!="form"){
throw new Error("FormBind: Couldn't apply, invalid form");
}else{
if(this.form==form){
return;
}else{
if(this.form){
throw new Error("FormBind: Already applied to a form");
}
}
}
dojo.lang.mixin(this.bindArgs,args);
this.form=form;
this.connect(form,"onsubmit","submit");
for(var i=0;i<form.elements.length;i++){
var node=form.elements[i];
if(node&&node.type&&dojo.lang.inArray(["submit","button"],node.type.toLowerCase())){
this.connect(node,"onclick","click");
}
}
var _357=form.getElementsByTagName("input");
for(var i=0;i<_357.length;i++){
var _358=_357[i];
if(_358.type.toLowerCase()=="image"&&_358.form==form){
this.connect(_358,"onclick","click");
}
}
},onSubmit:function(form){
return true;
},submit:function(e){
e.preventDefault();
if(this.onSubmit(this.form)){
dojo.io.bind(dojo.lang.mixin(this.bindArgs,{formFilter:dojo.lang.hitch(this,"formFilter")}));
}
},click:function(e){
var node=e.currentTarget;
if(node.disabled){
return;
}
this.clickedButton=node;
},formFilter:function(node){
var type=(node.type||"").toLowerCase();
var _35f=false;
if(node.disabled||!node.name){
_35f=false;
}else{
if(dojo.lang.inArray(["submit","button","image"],type)){
if(!this.clickedButton){
this.clickedButton=node;
}
_35f=node==this.clickedButton;
}else{
_35f=!dojo.lang.inArray(["file","submit","reset","button"],type);
}
}
return _35f;
},connect:function(_360,_361,_362){
if(dojo.getObject("dojo.event.connect")){
dojo.event.connect(_360,_361,this,_362);
}else{
var fcn=dojo.lang.hitch(this,_362);
_360[_361]=function(e){
if(!e){
e=window.event;
}
if(!e.currentTarget){
e.currentTarget=e.srcElement;
}
if(!e.preventDefault){
e.preventDefault=function(){
window.event.returnValue=false;
};
}
fcn(e);
};
}
}});
dojo.io.XMLHTTPTransport=new function(){
var _365=this;
var _366={};
this.useCache=false;
this.preventCache=false;
function getCacheKey(url,_368,_369){
return url+"|"+_368+"|"+_369.toLowerCase();
}
function addToCache(url,_36b,_36c,http){
_366[getCacheKey(url,_36b,_36c)]=http;
}
function getFromCache(url,_36f,_370){
return _366[getCacheKey(url,_36f,_370)];
}
this.clearCache=function(){
_366={};
};
function doLoad(_371,http,url,_374,_375){
if(((http.status>=200)&&(http.status<300))||(http.status==304)||(http.status==1223)||(location.protocol=="file:"&&(http.status==0||http.status==undefined))||(location.protocol=="chrome:"&&(http.status==0||http.status==undefined))){
var ret;
if(_371.method.toLowerCase()=="head"){
var _377=http.getAllResponseHeaders();
ret={};
ret.toString=function(){
return _377;
};
var _378=_377.split(/[\r\n]+/g);
for(var i=0;i<_378.length;i++){
var pair=_378[i].match(/^([^:]+)\s*:\s*(.+)$/i);
if(pair){
ret[pair[1]]=pair[2];
}
}
}else{
if(_371.mimetype=="text/javascript"){
try{
ret=dj_eval(http.responseText);
}
catch(e){
dojo.debug(e);
dojo.debug(http.responseText);
ret=null;
}
}else{
if(_371.mimetype.substr(0,9)=="text/json"||_371.mimetype.substr(0,16)=="application/json"){
try{
ret=dj_eval("("+_371.jsonFilter(http.responseText)+")");
}
catch(e){
dojo.debug(e);
dojo.debug(http.responseText);
ret=false;
}
}else{
if((_371.mimetype=="application/xml")||(_371.mimetype=="text/xml")){
ret=http.responseXML;
if(!ret||typeof ret=="string"||!http.getResponseHeader("Content-Type")){
ret=dojo.dom.createDocumentFromText(http.responseText);
}
}else{
ret=http.responseText;
}
}
}
}
if(_375){
addToCache(url,_374,_371.method,http);
}
try{
dojo.hostenv.insideBindHandler=true;
_371[(typeof _371.load=="function")?"load":"handle"]("load",ret,http,_371);
dojo.hostenv.insideBindHandler=false;
if(dojo.hostenv.post_load_){
dojo.hostenv.callLoaded();
}
}
finally{
dojo.hostenv.insideBindHandler=false;
}
}else{
var _37b=new dojo.io.Error("XMLHttpTransport Error: "+http.status+" "+http.statusText);
_371[(typeof _371.error=="function")?"error":"handle"]("error",_37b,http,_371);
}
}
function setHeaders(http,_37d){
if(_37d["headers"]){
for(var _37e in _37d["headers"]){
if(_37e.toLowerCase()=="content-type"&&!_37d["contentType"]){
_37d["contentType"]=_37d["headers"][_37e];
}else{
http.setRequestHeader(_37e,_37d["headers"][_37e]);
}
}
}
}
this.inFlight=[];
this.inFlightTimer=null;
this.startWatchingInFlight=function(){
if(!this.inFlightTimer){
this.inFlightTimer=setTimeout("dojo.io.XMLHTTPTransport.watchInFlight();",10);
}
};
this.watchInFlight=function(){
var now=null;
if(!dojo.hostenv._blockAsync&&!_365._blockAsync){
for(var x=this.inFlight.length-1;x>=0;x--){
try{
var tif=this.inFlight[x];
if(!tif||tif.http._aborted||!tif.http.readyState){
this.inFlight.splice(x,1);
continue;
}
if(4==tif.http.readyState){
this.inFlight.splice(x,1);
doLoad(tif.req,tif.http,tif.url,tif.query,tif.useCache);
}else{
if(tif.startTime){
if(!now){
now=(new Date()).getTime();
}
if(tif.startTime+(tif.req.timeoutSeconds*1000)<now){
if(typeof tif.http.abort=="function"){
tif.http.abort();
}
this.inFlight.splice(x,1);
tif.req[(typeof tif.req.timeout=="function")?"timeout":"handle"]("timeout",null,tif.http,tif.req);
}
}
}
}
catch(e){
try{
var _382=new dojo.io.Error("XMLHttpTransport.watchInFlight Error: "+e);
tif.req[(typeof tif.req.error=="function")?"error":"handle"]("error",_382,tif.http,tif.req);
}
catch(e2){
dojo.debug("XMLHttpTransport error callback failed: "+e2);
}
}
}
}
clearTimeout(this.inFlightTimer);
if(this.inFlight.length==0){
this.inFlightTimer=null;
return;
}
this.inFlightTimer=setTimeout("dojo.io.XMLHTTPTransport.watchInFlight();",10);
};
var _383=dojo.hostenv.getXmlhttpObject()?true:false;
this.canHandle=function(_384){
var mlc=_384["mimetype"].toLowerCase()||"";
return _383&&((dojo.lang.inArray(["text/plain","text/html","application/xml","text/xml","text/javascript"],mlc))||(mlc.substr(0,9)=="text/json"||mlc.substr(0,16)=="application/json"))&&!(_384["formNode"]&&dojo.io.formHasFile(_384["formNode"]));
};
this.multipartBoundary="45309FFF-BD65-4d50-99C9-36986896A96F";
this.bind=function(_386){
var url=_386.url;
var _388="";
if(_386["formNode"]){
var ta=_386.formNode.getAttribute("action");
if(typeof (ta)!="string"){
ta=_386.formNode.attributes.action.value;
}
if((ta)&&(!_386["url"])){
url=ta;
}
var tp=_386.formNode.getAttribute("method");
if((tp)&&(!_386["method"])){
_386.method=tp;
}
_388+=dojo.io.encodeForm(_386.formNode,_386.encoding,_386["formFilter"]);
}
if(url.indexOf("#")>-1){
dojo.debug("Warning: dojo.io.bind: stripping hash values from url:",url);
url=url.split("#")[0];
}
if(_386["file"]){
_386.method="post";
}
if(!_386["method"]){
_386.method="get";
}
if(_386.method.toLowerCase()=="get"){
_386.multipart=false;
}else{
if(_386["file"]){
_386.multipart=true;
}else{
if(!_386["multipart"]){
_386.multipart=false;
}
}
}
if(_386["backButton"]||_386["back"]||_386["changeUrl"]){
dojo.undo.browser.addToHistory(_386);
}
var _38b=_386["content"]||{};
if(_386.sendTransport){
_38b["dojo.transport"]="xmlhttp";
}
do{
if(_386.postContent){
_388=_386.postContent;
break;
}
if(_38b){
_388+=dojo.io.argsFromMap(_38b,_386.encoding);
}
if(_386.method.toLowerCase()=="get"||!_386.multipart){
break;
}
var t=[];
if(_388.length){
var q=_388.split("&");
for(var i=0;i<q.length;++i){
if(q[i].length){
var p=q[i].split("=");
t.push("--"+this.multipartBoundary,"Content-Disposition: form-data; name=\""+p[0]+"\"","",p[1]);
}
}
}
if(_386.file){
if(dojo.lang.isArray(_386.file)){
for(var i=0;i<_386.file.length;++i){
var o=_386.file[i];
t.push("--"+this.multipartBoundary,"Content-Disposition: form-data; name=\""+o.name+"\"; filename=\""+("fileName" in o?o.fileName:o.name)+"\"","Content-Type: "+("contentType" in o?o.contentType:"application/octet-stream"),"",o.content);
}
}else{
var o=_386.file;
t.push("--"+this.multipartBoundary,"Content-Disposition: form-data; name=\""+o.name+"\"; filename=\""+("fileName" in o?o.fileName:o.name)+"\"","Content-Type: "+("contentType" in o?o.contentType:"application/octet-stream"),"",o.content);
}
}
if(t.length){
t.push("--"+this.multipartBoundary+"--","");
_388=t.join("\r\n");
}
}while(false);
var _391=_386["sync"]?false:true;
var _392=_386["preventCache"]||(this.preventCache==true&&_386["preventCache"]!=false);
var _393=_386["useCache"]==true||(this.useCache==true&&_386["useCache"]!=false);
if(!_392&&_393){
var _394=getFromCache(url,_388,_386.method);
if(_394){
doLoad(_386,_394,url,_388,false);
return;
}
}
var http=dojo.hostenv.getXmlhttpObject(_386);
var _396=false;
if(_391){
var _397=this.inFlight.push({"req":_386,"http":http,"url":url,"query":_388,"useCache":_393,"startTime":_386.timeoutSeconds?(new Date()).getTime():0});
this.startWatchingInFlight();
}else{
_365._blockAsync=true;
}
if(_386.method.toLowerCase()=="post"){
if(!_386.user){
http.open("POST",url,_391);
}else{
http.open("POST",url,_391,_386.user,_386.password);
}
setHeaders(http,_386);
http.setRequestHeader("Content-Type",_386.multipart?("multipart/form-data; boundary="+this.multipartBoundary):(_386.contentType||"application/x-www-form-urlencoded"));
try{
http.send(_388);
}
catch(e){
if(typeof http.abort=="function"){
http.abort();
}
doLoad(_386,{status:404},url,_388,_393);
}
}else{
var _398=url;
if(_388!=""){
_398+=(_398.indexOf("?")>-1?"&":"?")+_388;
}
if(_392){
_398+=(dojo.string.endsWithAny(_398,"?","&")?"":(_398.indexOf("?")>-1?"&":"?"))+"dojo.preventCache="+new Date().valueOf();
}
if(!_386.user){
http.open(_386.method.toUpperCase(),_398,_391);
}else{
http.open(_386.method.toUpperCase(),_398,_391,_386.user,_386.password);
}
setHeaders(http,_386);
try{
http.send(null);
}
catch(e){
if(typeof http.abort=="function"){
http.abort();
}
doLoad(_386,{status:404},url,_388,_393);
}
}
if(!_391){
doLoad(_386,http,url,_388,_393);
_365._blockAsync=false;
}
_386.abort=function(){
try{
http._aborted=true;
}
catch(e){
}
return http.abort();
};
return;
};
dojo.io.transports.addTransport("XMLHTTPTransport");
};
}
dojo.provide("dojo.io.cookie");
dojo.io.cookie.setCookie=function(name,_39a,days,path,_39d,_39e){
var _39f=-1;
if((typeof days=="number")&&(days>=0)){
var d=new Date();
d.setTime(d.getTime()+(days*24*60*60*1000));
_39f=d.toGMTString();
}
_39a=escape(_39a);
document.cookie=name+"="+_39a+";"+(_39f!=-1?" expires="+_39f+";":"")+(path?"path="+path:"")+(_39d?"; domain="+_39d:"")+(_39e?"; secure":"");
};
dojo.io.cookie.set=dojo.io.cookie.setCookie;
dojo.io.cookie.getCookie=function(name){
var idx=document.cookie.lastIndexOf(name+"=");
if(idx==-1){
return null;
}
var _3a3=document.cookie.substring(idx+name.length+1);
var end=_3a3.indexOf(";");
if(end==-1){
end=_3a3.length;
}
_3a3=_3a3.substring(0,end);
_3a3=unescape(_3a3);
return _3a3;
};
dojo.io.cookie.get=dojo.io.cookie.getCookie;
dojo.io.cookie.deleteCookie=function(name){
dojo.io.cookie.setCookie(name,"-",0);
};
dojo.io.cookie.setObjectCookie=function(name,obj,days,path,_3aa,_3ab,_3ac){
if(arguments.length==5){
_3ac=_3aa;
_3aa=null;
_3ab=null;
}
var _3ad=[],_3ae,_3af="";
if(!_3ac){
_3ae=dojo.io.cookie.getObjectCookie(name);
}
if(days>=0){
if(!_3ae){
_3ae={};
}
for(var prop in obj){
if(obj[prop]==null){
delete _3ae[prop];
}else{
if((typeof obj[prop]=="string")||(typeof obj[prop]=="number")){
_3ae[prop]=obj[prop];
}
}
}
prop=null;
for(var prop in _3ae){
_3ad.push(escape(prop)+"="+escape(_3ae[prop]));
}
_3af=_3ad.join("&");
}
dojo.io.cookie.setCookie(name,_3af,days,path,_3aa,_3ab);
};
dojo.io.cookie.getObjectCookie=function(name){
var _3b2=null,_3b3=dojo.io.cookie.getCookie(name);
if(_3b3){
_3b2={};
var _3b4=_3b3.split("&");
for(var i=0;i<_3b4.length;i++){
var pair=_3b4[i].split("=");
var _3b7=pair[1];
if(isNaN(_3b7)){
_3b7=unescape(pair[1]);
}
_3b2[unescape(pair[0])]=_3b7;
}
}
return _3b2;
};
dojo.io.cookie.isSupported=function(){
if(typeof navigator.cookieEnabled!="boolean"){
dojo.io.cookie.setCookie("__TestingYourBrowserForCookieSupport__","CookiesAllowed",90,null);
var _3b8=dojo.io.cookie.getCookie("__TestingYourBrowserForCookieSupport__");
navigator.cookieEnabled=(_3b8=="CookiesAllowed");
if(navigator.cookieEnabled){
this.deleteCookie("__TestingYourBrowserForCookieSupport__");
}
}
return navigator.cookieEnabled;
};
if(!dojo.io.cookies){
dojo.io.cookies=dojo.io.cookie;
}
dojo.kwCompoundRequire({common:["dojo.io.common"],rhino:["dojo.io.RhinoIO"],browser:["dojo.io.BrowserIO","dojo.io.cookie"],dashboard:["dojo.io.BrowserIO","dojo.io.cookie"]});
dojo.provide("dojo.io.*");
dojo.kwCompoundRequire({common:[["dojo.uri.Uri",false,false]]});
dojo.provide("dojo.uri.*");
dojo.provide("dojo.io.IframeIO");
dojo.io.createIFrame=function(_3b9,_3ba,uri){
if(window[_3b9]){
return window[_3b9];
}
if(window.frames[_3b9]){
return window.frames[_3b9];
}
var r=dojo.render.html;
var _3bd=null;
var turi=uri;
if(!turi){
if(djConfig["useXDomain"]&&!djConfig["dojoIframeHistoryUrl"]){
dojo.debug("dojo.io.createIFrame: When using cross-domain Dojo builds,"+" please save iframe_history.html to your domain and set djConfig.dojoIframeHistoryUrl"+" to the path on your domain to iframe_history.html");
}
turi=(djConfig["dojoIframeHistoryUrl"]||dojo.uri.moduleUri("dojo","../iframe_history.html"))+"#noInit=true";
}
var _3bf=((r.ie)&&(dojo.render.os.win))?"<iframe name=\""+_3b9+"\" src=\""+turi+"\" onload=\""+_3ba+"\">":"iframe";
_3bd=document.createElement(_3bf);
with(_3bd){
name=_3b9;
setAttribute("name",_3b9);
id=_3b9;
}
dojo.body().appendChild(_3bd);
window[_3b9]=_3bd;
with(_3bd.style){
if(!r.safari){
position="absolute";
}
left=top="0px";
height=width="1px";
visibility="hidden";
}
if(!r.ie){
dojo.io.setIFrameSrc(_3bd,turi,true);
_3bd.onload=new Function(_3ba);
}
return _3bd;
};
dojo.io.IframeTransport=new function(){
var _3c0=this;
this.currentRequest=null;
this.requestQueue=[];
this.iframeName="dojoIoIframe";
this.fireNextRequest=function(){
try{
if((this.currentRequest)||(this.requestQueue.length==0)){
return;
}
var cr=this.currentRequest=this.requestQueue.shift();
cr._contentToClean=[];
var fn=cr["formNode"];
var _3c3=cr["content"]||{};
if(cr.sendTransport){
_3c3["dojo.transport"]="iframe";
}
if(fn){
if(_3c3){
for(var x in _3c3){
if(!fn[x]){
var tn;
if(dojo.render.html.ie){
tn=document.createElement("<input type='hidden' name='"+x+"' value='"+_3c3[x]+"'>");
fn.appendChild(tn);
}else{
tn=document.createElement("input");
fn.appendChild(tn);
tn.type="hidden";
tn.name=x;
tn.value=_3c3[x];
}
cr._contentToClean.push(x);
}else{
fn[x].value=_3c3[x];
}
}
}
if(cr["url"]){
cr._originalAction=fn.getAttribute("action");
fn.setAttribute("action",cr.url);
}
if(!fn.getAttribute("method")){
fn.setAttribute("method",(cr["method"])?cr["method"]:"post");
}
cr._originalTarget=fn.getAttribute("target");
fn.setAttribute("target",this.iframeName);
fn.target=this.iframeName;
fn.submit();
}else{
var _3c6=dojo.io.argsFromMap(this.currentRequest.content);
var _3c7=cr.url+(cr.url.indexOf("?")>-1?"&":"?")+_3c6;
dojo.io.setIFrameSrc(this.iframe,_3c7,true);
}
}
catch(e){
this.iframeOnload(e);
}
};
this.canHandle=function(_3c8){
return ((dojo.lang.inArray(["text/xml","text/plain","text/html","text/javascript","text/json","application/json"],_3c8["mimetype"]))&&(dojo.lang.inArray(["post","get"],_3c8["method"].toLowerCase()))&&(!((_3c8["sync"])&&(_3c8["sync"]==true))));
};
this.bind=function(_3c9){
if(!this["iframe"]){
this.setUpIframe();
}
this.requestQueue.push(_3c9);
this.fireNextRequest();
return;
};
this.setUpIframe=function(){
this.iframe=dojo.io.createIFrame(this.iframeName,"dojo.io.IframeTransport.iframeOnload();");
};
this.iframeOnload=function(_3ca){
if(!_3c0.currentRequest){
_3c0.fireNextRequest();
return;
}
var req=_3c0.currentRequest;
if(req.formNode){
var _3cc=req._contentToClean;
for(var i=0;i<_3cc.length;i++){
var key=_3cc[i];
if(dojo.render.html.safari){
var _3cf=req.formNode;
for(var j=0;j<_3cf.childNodes.length;j++){
var _3d1=_3cf.childNodes[j];
if(_3d1.name==key){
var _3d2=_3d1.parentNode;
_3d2.removeChild(_3d1);
break;
}
}
}else{
var _3d3=req.formNode[key];
req.formNode.removeChild(_3d3);
req.formNode[key]=null;
}
}
if(req["_originalAction"]){
req.formNode.setAttribute("action",req._originalAction);
}
if(req["_originalTarget"]){
req.formNode.setAttribute("target",req._originalTarget);
req.formNode.target=req._originalTarget;
}
}
var _3d4=function(_3d5){
var doc=_3d5.contentDocument||((_3d5.contentWindow)&&(_3d5.contentWindow.document))||((_3d5.name)&&(document.frames[_3d5.name])&&(document.frames[_3d5.name].document))||null;
return doc;
};
var _3d7;
var _3d8=false;
if(_3ca){
this._callError(req,"IframeTransport Request Error: "+_3ca);
}else{
var ifd=_3d4(_3c0.iframe);
try{
var cmt=req.mimetype;
if((cmt=="text/javascript")||(cmt=="text/json")||(cmt=="application/json")){
var js=ifd.getElementsByTagName("textarea")[0].value;
if(cmt=="text/json"||cmt=="application/json"){
js="("+js+")";
}
_3d7=dj_eval(js);
}else{
if(cmt=="text/html"){
_3d7=ifd;
}else{
_3d7=ifd.getElementsByTagName("textarea")[0].value;
}
}
_3d8=true;
}
catch(e){
this._callError(req,"IframeTransport Error: "+e);
}
}
try{
if(_3d8&&dojo.lang.isFunction(req["load"])){
req.load("load",_3d7,req);
}
}
catch(e){
throw e;
}
finally{
_3c0.currentRequest=null;
_3c0.fireNextRequest();
}
};
this._callError=function(req,_3dd){
var _3de=new dojo.io.Error(_3dd);
if(dojo.lang.isFunction(req["error"])){
req.error("error",_3de,req);
}
};
dojo.io.transports.addTransport("IframeTransport");
};
dojo.provide("dojo.collections.Collections");
dojo.collections.DictionaryEntry=function(k,v){
this.key=k;
this.value=v;
this.valueOf=function(){
return this.value;
};
this.toString=function(){
return String(this.value);
};
};
dojo.collections.Iterator=function(arr){
var a=arr;
var _3e3=0;
this.element=a[_3e3]||null;
this.atEnd=function(){
return (_3e3>=a.length);
};
this.get=function(){
if(this.atEnd()){
return null;
}
this.element=a[_3e3++];
return this.element;
};
this.map=function(fn,_3e5){
var s=_3e5||dj_global;
if(Array.map){
return Array.map(a,fn,s);
}else{
var arr=[];
for(var i=0;i<a.length;i++){
arr.push(fn.call(s,a[i]));
}
return arr;
}
};
this.reset=function(){
_3e3=0;
this.element=a[_3e3];
};
};
dojo.collections.DictionaryIterator=function(obj){
var a=[];
var _3eb={};
for(var p in obj){
if(!_3eb[p]){
a.push(obj[p]);
}
}
var _3ed=0;
this.element=a[_3ed]||null;
this.atEnd=function(){
return (_3ed>=a.length);
};
this.get=function(){
if(this.atEnd()){
return null;
}
this.element=a[_3ed++];
return this.element;
};
this.map=function(fn,_3ef){
var s=_3ef||dj_global;
if(Array.map){
return Array.map(a,fn,s);
}else{
var arr=[];
for(var i=0;i<a.length;i++){
arr.push(fn.call(s,a[i]));
}
return arr;
}
};
this.reset=function(){
_3ed=0;
this.element=a[_3ed];
};
};
dojo.provide("dojo.collections.Dictionary");
dojo.collections.Dictionary=function(_3f3){
var _3f4={};
this.count=0;
var _3f5={};
this.add=function(k,v){
var b=(k in _3f4);
_3f4[k]=new dojo.collections.DictionaryEntry(k,v);
if(!b){
this.count++;
}
};
this.clear=function(){
_3f4={};
this.count=0;
};
this.clone=function(){
return new dojo.collections.Dictionary(this);
};
this.contains=this.containsKey=function(k){
if(_3f5[k]){
return false;
}
return (_3f4[k]!=null);
};
this.containsValue=function(v){
var e=this.getIterator();
while(e.get()){
if(e.element.value==v){
return true;
}
}
return false;
};
this.entry=function(k){
return _3f4[k];
};
this.forEach=function(fn,_3fe){
var a=[];
for(var p in _3f4){
if(!_3f5[p]){
a.push(_3f4[p]);
}
}
var s=_3fe||dj_global;
if(Array.forEach){
Array.forEach(a,fn,s);
}else{
for(var i=0;i<a.length;i++){
fn.call(s,a[i],i,a);
}
}
};
this.getKeyList=function(){
return (this.getIterator()).map(function(_403){
return _403.key;
});
};
this.getValueList=function(){
return (this.getIterator()).map(function(_404){
return _404.value;
});
};
this.item=function(k){
if(k in _3f4){
return _3f4[k].valueOf();
}
return undefined;
};
this.getIterator=function(){
return new dojo.collections.DictionaryIterator(_3f4);
};
this.remove=function(k){
if(k in _3f4&&!_3f5[k]){
delete _3f4[k];
this.count--;
return true;
}
return false;
};
if(_3f3){
var e=_3f3.getIterator();
while(e.get()){
this.add(e.element.key,e.element.value);
}
}
};
dojo.provide("dojo.xml.Parse");
dojo.xml.Parse=function(){
var isIE=((dojo.render.html.capable)&&(dojo.render.html.ie));
function getTagName(node){
try{
return node.tagName.toLowerCase();
}
catch(e){
return "";
}
}
function getDojoTagName(node){
var _40b=getTagName(node);
if(!_40b){
return "";
}
if((dojo.widget)&&(dojo.widget.tags[_40b])){
return _40b;
}
var p=_40b.indexOf(":");
if(p>=0){
return _40b;
}
if(_40b.substr(0,5)=="dojo:"){
return _40b;
}
if(dojo.render.html.capable&&dojo.render.html.ie&&node.scopeName&&node.scopeName!="HTML"){
return node.scopeName.toLowerCase()+":"+_40b;
}
if(_40b.substr(0,4)=="dojo"){
return "dojo:"+_40b.substring(4);
}
var djt=node.getAttribute("dojoType")||node.getAttribute("dojotype");
if(djt){
if(djt.indexOf(":")<0){
djt="dojo:"+djt;
}
return djt.toLowerCase();
}
djt=node.getAttributeNS&&node.getAttributeNS(dojo.dom.dojoml,"type");
if(djt){
return "dojo:"+djt.toLowerCase();
}
try{
djt=node.getAttribute("dojo:type");
}
catch(e){
}
if(djt){
return "dojo:"+djt.toLowerCase();
}
if((dj_global["djConfig"])&&(!djConfig["ignoreClassNames"])){
var _40e=node.className||node.getAttribute("class");
if((_40e)&&(_40e.indexOf)&&(_40e.indexOf("dojo-")!=-1)){
var _40f=_40e.split(" ");
for(var x=0,c=_40f.length;x<c;x++){
if(_40f[x].slice(0,5)=="dojo-"){
return "dojo:"+_40f[x].substr(5).toLowerCase();
}
}
}
}
return "";
}
this.parseElement=function(node,_413,_414,_415){
var _416=getTagName(node);
if(isIE&&_416.indexOf("/")==0){
return null;
}
try{
var attr=node.getAttribute("parseWidgets");
if(attr&&attr.toLowerCase()=="false"){
return {};
}
}
catch(e){
}
var _418=true;
if(_414){
var _419=getDojoTagName(node);
_416=_419||_416;
_418=Boolean(_419);
}
var _41a={};
_41a[_416]=[];
var pos=_416.indexOf(":");
if(pos>0){
var ns=_416.substring(0,pos);
_41a["ns"]=ns;
if((dojo.ns)&&(!dojo.ns.allow(ns))){
_418=false;
}
}
if(_418){
var _41d=this.parseAttributes(node);
for(var attr in _41d){
if((!_41a[_416][attr])||(typeof _41a[_416][attr]!="array")){
_41a[_416][attr]=[];
}
_41a[_416][attr].push(_41d[attr]);
}
_41a[_416].nodeRef=node;
_41a.tagName=_416;
_41a.index=_415||0;
}
var _41e=0;
for(var i=0;i<node.childNodes.length;i++){
var tcn=node.childNodes.item(i);
switch(tcn.nodeType){
case dojo.dom.ELEMENT_NODE:
var ctn=getDojoTagName(tcn)||getTagName(tcn);
if(!_41a[ctn]){
_41a[ctn]=[];
}
_41a[ctn].push(this.parseElement(tcn,true,_414,_41e));
if((tcn.childNodes.length==1)&&(tcn.childNodes.item(0).nodeType==dojo.dom.TEXT_NODE)){
_41a[ctn][_41a[ctn].length-1].value=tcn.childNodes.item(0).nodeValue;
}
_41e++;
break;
case dojo.dom.TEXT_NODE:
if(node.childNodes.length==1){
_41a[_416].push({value:node.childNodes.item(0).nodeValue});
}
break;
default:
break;
}
}
return _41a;
};
this.parseAttributes=function(node){
var _423={};
var atts=node.attributes;
var _425,i=0;
while((_425=atts[i++])){
if(isIE){
if(!_425){
continue;
}
if((typeof _425=="object")&&(typeof _425.nodeValue=="undefined")||(_425.nodeValue==null)||(_425.nodeValue=="")){
continue;
}
}
var nn=_425.nodeName.split(":");
nn=(nn.length==2)?nn[1]:_425.nodeName;
_423[nn]={value:_425.nodeValue};
}
return _423;
};
};
dojo.provide("dojo.lang.declare");
dojo.lang.declare=function(_428,_429,init,_42b){
if((dojo.lang.isFunction(_42b))||((!_42b)&&(!dojo.lang.isFunction(init)))){
if(dojo.lang.isFunction(_42b)){
dojo.deprecated("dojo.lang.declare("+_428+"...):","use class, superclass, initializer, properties argument order","0.6");
}
var temp=_42b;
_42b=init;
init=temp;
}
if(_42b&&_42b.initializer){
dojo.deprecated("dojo.lang.declare("+_428+"...):","specify initializer as third argument, not as an element in properties","0.6");
}
var _42d=[];
if(dojo.lang.isArray(_429)){
_42d=_429;
_429=_42d.shift();
}
if(!init){
init=dojo.getObject(_428,false);
if((init)&&(!dojo.lang.isFunction(init))){
init=null;
}
}
var ctor=dojo.lang.declare._makeConstructor();
var scp=(_429?_429.prototype:null);
if(scp){
scp.prototyping=true;
ctor.prototype=new _429();
scp.prototyping=false;
}
ctor.superclass=scp;
ctor.mixins=_42d;
for(var i=0,l=_42d.length;i<l;i++){
dojo.lang.extend(ctor,_42d[i].prototype);
}
ctor.prototype.initializer=null;
ctor.prototype.declaredClass=_428;
if(dojo.lang.isArray(_42b)){
dojo.lang.extend.apply(dojo.lang,[ctor].concat(_42b));
}else{
dojo.lang.extend(ctor,(_42b)||{});
}
dojo.lang.extend(ctor,dojo.lang.declare._common);
ctor.prototype.constructor=ctor;
ctor.prototype.initializer=(ctor.prototype.initializer)||(init)||(function(){
});
var _432=dojo.getObject(_428,true,null,true);
_432.obj[_432.prop]=ctor;
return ctor;
};
dojo.lang.declare._makeConstructor=function(){
return function(){
var self=this._getPropContext();
var s=self.constructor.superclass;
if((s)&&(s.constructor)){
if(s.constructor==arguments.callee){
this._inherited("constructor",arguments);
}else{
this._contextMethod(s,"constructor",arguments);
}
}
var ms=(self.constructor.mixins)||([]);
for(var i=0,m;(m=ms[i]);i++){
(((m.prototype)&&(m.prototype.initializer))||(m)).apply(this,arguments);
}
if((!this.prototyping)&&(self.initializer)){
self.initializer.apply(this,arguments);
}
};
};
dojo.lang.declare._common={_getPropContext:function(){
return (this.___proto||this);
},_contextMethod:function(_438,_439,args){
var _43b,_43c=this.___proto;
this.___proto=_438;
try{
_43b=_438[_439].apply(this,(args||[]));
}
catch(e){
throw e;
}
finally{
this.___proto=_43c;
}
return _43b;
},_inherited:function(prop,args){
var p=this._getPropContext();
do{
if((!p.constructor)||(!p.constructor.superclass)){
return;
}
p=p.constructor.superclass;
}while(!(prop in p));
return (dojo.lang.isFunction(p[prop])?this._contextMethod(p,prop,args):p[prop]);
}};
dojo.declare=dojo.lang.declare;
dojo.provide("dojo.ns");
dojo.ns={namespaces:{},failed:{},loading:{},loaded:{},register:function(name,_441,_442,_443){
if(!_443||!this.namespaces[name]){
this.namespaces[name]=new dojo.ns.Ns(name,_441,_442);
}
},allow:function(name){
if(this.failed[name]){
return false;
}
if((djConfig.excludeNamespace)&&(dojo.lang.inArray(djConfig.excludeNamespace,name))){
return false;
}
return ((name==this.dojo)||(!djConfig.includeNamespace)||(dojo.lang.inArray(djConfig.includeNamespace,name)));
},get:function(name){
return this.namespaces[name];
},require:function(name){
var ns=this.namespaces[name];
if((ns)&&(this.loaded[name])){
return ns;
}
if(!this.allow(name)){
return false;
}
if(this.loading[name]){
dojo.debug("dojo.namespace.require: re-entrant request to load namespace \""+name+"\" must fail.");
return false;
}
var req=dojo.require;
this.loading[name]=true;
try{
if(name=="dojo"){
req("dojo.namespaces.dojo");
}else{
if(!dojo.hostenv.moduleHasPrefix(name)){
dojo.registerModulePath(name,"../"+name);
}
req([name,"manifest"].join("."),false,true);
}
if(!this.namespaces[name]){
this.failed[name]=true;
}
}
finally{
this.loading[name]=false;
}
return this.namespaces[name];
}};
dojo.ns.Ns=function(name,_44a,_44b){
this.name=name;
this.module=_44a;
this.resolver=_44b;
this._loaded=[];
this._failed=[];
};
dojo.ns.Ns.prototype.resolve=function(name,_44d,_44e){
if(!this.resolver||djConfig["skipAutoRequire"]){
return false;
}
var _44f=this.resolver(name,_44d);
if((_44f)&&(!this._loaded[_44f])&&(!this._failed[_44f])){
var req=dojo.require;
req(_44f,false,true);
if(dojo.hostenv.findModule(_44f,false)){
this._loaded[_44f]=true;
}else{
if(!_44e){
dojo.raise("dojo.ns.Ns.resolve: module '"+_44f+"' not found after loading via namespace '"+this.name+"'");
}
this._failed[_44f]=true;
}
}
return Boolean(this._loaded[_44f]);
};
dojo.registerNamespace=function(name,_452,_453){
dojo.ns.register.apply(dojo.ns,arguments);
};
dojo.registerNamespaceResolver=function(name,_455){
var n=dojo.ns.namespaces[name];
if(n){
n.resolver=_455;
}
};
dojo.registerNamespaceManifest=function(_457,path,name,_45a,_45b){
dojo.registerModulePath(_457,path);
dojo.registerNamespace(name,_45a,_45b);
};
dojo.registerNamespace("dojo","dojo.widget");
dojo.provide("dojo.event.common");
dojo.event=new function(){
this._canTimeout=dojo.lang.isFunction(dj_global["setTimeout"])||dojo.lang.isAlien(dj_global["setTimeout"]);
function interpolateArgs(args,_45d){
var dl=dojo.lang;
var ao={srcObj:dj_global,srcFunc:null,adviceObj:dj_global,adviceFunc:null,aroundObj:null,aroundFunc:null,adviceType:(args.length>2)?args[0]:"after",precedence:"last",once:false,delay:null,rate:0,adviceMsg:false,maxCalls:-1};
switch(args.length){
case 0:
return;
case 1:
return;
case 2:
ao.srcFunc=args[0];
ao.adviceFunc=args[1];
break;
case 3:
if((dl.isObject(args[0]))&&(dl.isString(args[1]))&&(dl.isString(args[2]))){
ao.adviceType="after";
ao.srcObj=args[0];
ao.srcFunc=args[1];
ao.adviceFunc=args[2];
}else{
if((dl.isString(args[1]))&&(dl.isString(args[2]))){
ao.srcFunc=args[1];
ao.adviceFunc=args[2];
}else{
if((dl.isObject(args[0]))&&(dl.isString(args[1]))&&(dl.isFunction(args[2]))){
ao.adviceType="after";
ao.srcObj=args[0];
ao.srcFunc=args[1];
var _460=dl.nameAnonFunc(args[2],ao.adviceObj,_45d);
ao.adviceFunc=_460;
}else{
if((dl.isFunction(args[0]))&&(dl.isObject(args[1]))&&(dl.isString(args[2]))){
ao.adviceType="after";
ao.srcObj=dj_global;
var _460=dl.nameAnonFunc(args[0],ao.srcObj,_45d);
ao.srcFunc=_460;
ao.adviceObj=args[1];
ao.adviceFunc=args[2];
}
}
}
}
break;
case 4:
if((dl.isObject(args[0]))&&(dl.isObject(args[2]))){
ao.adviceType="after";
ao.srcObj=args[0];
ao.srcFunc=args[1];
ao.adviceObj=args[2];
ao.adviceFunc=args[3];
}else{
if((dl.isString(args[0]))&&(dl.isString(args[1]))&&(dl.isObject(args[2]))){
ao.adviceType=args[0];
ao.srcObj=dj_global;
ao.srcFunc=args[1];
ao.adviceObj=args[2];
ao.adviceFunc=args[3];
}else{
if((dl.isString(args[0]))&&(dl.isFunction(args[1]))&&(dl.isObject(args[2]))){
ao.adviceType=args[0];
ao.srcObj=dj_global;
var _460=dl.nameAnonFunc(args[1],dj_global,_45d);
ao.srcFunc=_460;
ao.adviceObj=args[2];
ao.adviceFunc=args[3];
}else{
if((dl.isString(args[0]))&&(dl.isObject(args[1]))&&(dl.isString(args[2]))&&(dl.isFunction(args[3]))){
ao.srcObj=args[1];
ao.srcFunc=args[2];
var _460=dl.nameAnonFunc(args[3],dj_global,_45d);
ao.adviceObj=dj_global;
ao.adviceFunc=_460;
}else{
if(dl.isObject(args[1])){
ao.srcObj=args[1];
ao.srcFunc=args[2];
ao.adviceObj=dj_global;
ao.adviceFunc=args[3];
}else{
if(dl.isObject(args[2])){
ao.srcObj=dj_global;
ao.srcFunc=args[1];
ao.adviceObj=args[2];
ao.adviceFunc=args[3];
}else{
ao.srcObj=ao.adviceObj=ao.aroundObj=dj_global;
ao.srcFunc=args[1];
ao.adviceFunc=args[2];
ao.aroundFunc=args[3];
}
}
}
}
}
}
break;
case 6:
ao.srcObj=args[1];
ao.srcFunc=args[2];
ao.adviceObj=args[3];
ao.adviceFunc=args[4];
ao.aroundFunc=args[5];
ao.aroundObj=dj_global;
break;
default:
ao.srcObj=args[1];
ao.srcFunc=args[2];
ao.adviceObj=args[3];
ao.adviceFunc=args[4];
ao.aroundObj=args[5];
ao.aroundFunc=args[6];
ao.once=args[7];
ao.delay=args[8];
ao.rate=args[9];
ao.adviceMsg=args[10];
ao.maxCalls=(!isNaN(parseInt(args[11])))?args[11]:-1;
break;
}
if(dl.isFunction(ao.aroundFunc)){
var _460=dl.nameAnonFunc(ao.aroundFunc,ao.aroundObj,_45d);
ao.aroundFunc=_460;
}
if(dl.isFunction(ao.srcFunc)){
ao.srcFunc=dl.getNameInObj(ao.srcObj,ao.srcFunc);
}
if(dl.isFunction(ao.adviceFunc)){
ao.adviceFunc=dl.getNameInObj(ao.adviceObj,ao.adviceFunc);
}
if((ao.aroundObj)&&(dl.isFunction(ao.aroundFunc))){
ao.aroundFunc=dl.getNameInObj(ao.aroundObj,ao.aroundFunc);
}
if(!ao.srcObj){
dojo.raise("bad srcObj for srcFunc: "+ao.srcFunc);
}
if(!ao.adviceObj){
dojo.raise("bad adviceObj for adviceFunc: "+ao.adviceFunc);
}
if(!ao.adviceFunc){
dojo.debug("bad adviceFunc for srcFunc: "+ao.srcFunc);
dojo.debugShallow(ao);
}
return ao;
}
this.connect=function(){
if(arguments.length==1){
var ao=arguments[0];
}else{
var ao=interpolateArgs(arguments,true);
}
if(dojo.lang.isString(ao.srcFunc)&&(ao.srcFunc.toLowerCase()=="onkey")){
if(dojo.render.html.ie){
ao.srcFunc="onkeydown";
this.connect(ao);
}
ao.srcFunc="onkeypress";
}
if(dojo.lang.isArray(ao.srcObj)&&ao.srcObj!=""){
var _462={};
for(var x in ao){
_462[x]=ao[x];
}
var mjps=[];
dojo.lang.forEach(ao.srcObj,function(src){
if((dojo.render.html.capable)&&(dojo.lang.isString(src))){
src=dojo.byId(src);
}
_462.srcObj=src;
mjps.push(dojo.event.connect.call(dojo.event,_462));
});
return mjps;
}
var mjp=dojo.event.MethodJoinPoint.getForMethod(ao.srcObj,ao.srcFunc);
if(ao.adviceFunc){
var mjp2=dojo.event.MethodJoinPoint.getForMethod(ao.adviceObj,ao.adviceFunc);
}
mjp.kwAddAdvice(ao);
return mjp;
};
this.log=function(a1,a2){
var _46a;
if((arguments.length==1)&&(typeof a1=="object")){
_46a=a1;
}else{
_46a={srcObj:a1,srcFunc:a2};
}
_46a.adviceFunc=function(){
var _46b=[];
for(var x=0;x<arguments.length;x++){
_46b.push(arguments[x]);
}
dojo.debug("("+_46a.srcObj+")."+_46a.srcFunc,":",_46b.join(", "));
};
this.kwConnect(_46a);
};
this.connectBefore=function(){
var args=["before"];
for(var i=0;i<arguments.length;i++){
args.push(arguments[i]);
}
return this.connect.apply(this,args);
};
this.connectAround=function(){
var args=["around"];
for(var i=0;i<arguments.length;i++){
args.push(arguments[i]);
}
return this.connect.apply(this,args);
};
this.connectOnce=function(){
var ao=interpolateArgs(arguments,true);
ao.once=true;
return this.connect(ao);
};
this.connectRunOnce=function(){
var ao=interpolateArgs(arguments,true);
ao.maxCalls=1;
return this.connect(ao);
};
this._kwConnectImpl=function(_473,_474){
var fn=(_474)?"disconnect":"connect";
if(typeof _473["srcFunc"]=="function"){
_473.srcObj=_473["srcObj"]||dj_global;
var _476=dojo.lang.nameAnonFunc(_473.srcFunc,_473.srcObj,true);
_473.srcFunc=_476;
}
if(typeof _473["adviceFunc"]=="function"){
_473.adviceObj=_473["adviceObj"]||dj_global;
var _476=dojo.lang.nameAnonFunc(_473.adviceFunc,_473.adviceObj,true);
_473.adviceFunc=_476;
}
_473.srcObj=_473["srcObj"]||dj_global;
_473.adviceObj=_473["adviceObj"]||_473["targetObj"]||dj_global;
_473.adviceFunc=_473["adviceFunc"]||_473["targetFunc"];
return dojo.event[fn](_473);
};
this.kwConnect=function(_477){
return this._kwConnectImpl(_477,false);
};
this.disconnect=function(){
if(arguments.length==1){
var ao=arguments[0];
}else{
var ao=interpolateArgs(arguments,true);
}
if(!ao.adviceFunc){
return;
}
if(dojo.lang.isString(ao.srcFunc)&&(ao.srcFunc.toLowerCase()=="onkey")){
if(dojo.render.html.ie){
ao.srcFunc="onkeydown";
this.disconnect(ao);
}
ao.srcFunc="onkeypress";
}
if(!ao.srcObj[ao.srcFunc]){
return null;
}
var mjp=dojo.event.MethodJoinPoint.getForMethod(ao.srcObj,ao.srcFunc,true);
mjp.removeAdvice(ao.adviceObj,ao.adviceFunc,ao.adviceType,ao.once);
return mjp;
};
this.kwDisconnect=function(_47a){
return this._kwConnectImpl(_47a,true);
};
};
dojo.event.MethodInvocation=function(_47b,obj,args){
this.jp_=_47b;
this.object=obj;
this.args=[];
for(var x=0;x<args.length;x++){
this.args[x]=args[x];
}
this.around_index=-1;
};
dojo.event.MethodInvocation.prototype.proceed=function(){
this.around_index++;
if(this.around_index>=this.jp_.around.length){
return this.jp_.object[this.jp_.methodname].apply(this.jp_.object,this.args);
}else{
var ti=this.jp_.around[this.around_index];
var mobj=ti[0]||dj_global;
var meth=ti[1];
return mobj[meth].call(mobj,this);
}
};
dojo.event.MethodJoinPoint=function(obj,_483){
this.object=obj||dj_global;
this.methodname=_483;
this.methodfunc=this.object[_483];
};
dojo.event.MethodJoinPoint.getForMethod=function(obj,_485){
if(!obj){
obj=dj_global;
}
var ofn=obj[_485];
if(!ofn){
ofn=obj[_485]=function(){
};
if(!obj[_485]){
dojo.raise("Cannot set do-nothing method on that object "+_485);
}
}else{
if((typeof ofn!="function")&&(!dojo.lang.isFunction(ofn))&&(!dojo.lang.isAlien(ofn))){
return null;
}
}
var _487=_485+"$joinpoint";
var _488=_485+"$joinpoint$method";
var _489=obj[_487];
if(!_489){
var _48a=false;
if(dojo.event["browser"]){
if((obj["attachEvent"])||(obj["nodeType"])||(obj["addEventListener"])){
_48a=true;
dojo.event.browser.addClobberNodeAttrs(obj,[_487,_488,_485]);
}
}
var _48b=ofn.length;
obj[_488]=ofn;
_489=obj[_487]=new dojo.event.MethodJoinPoint(obj,_488);
if(!_48a){
obj[_485]=function(){
return _489.run.apply(_489,arguments);
};
}else{
obj[_485]=function(){
var args=[];
if(!arguments.length){
var evt=null;
try{
if(obj.ownerDocument){
evt=obj.ownerDocument.parentWindow.event;
}else{
if(obj.documentElement){
evt=obj.documentElement.ownerDocument.parentWindow.event;
}else{
if(obj.event){
evt=obj.event;
}else{
evt=window.event;
}
}
}
}
catch(e){
evt=window.event;
}
if(evt){
args.push(dojo.event.browser.fixEvent(evt,this));
}
}else{
for(var x=0;x<arguments.length;x++){
if((x==0)&&(dojo.event.browser.isEvent(arguments[x]))){
args.push(dojo.event.browser.fixEvent(arguments[x],this));
}else{
args.push(arguments[x]);
}
}
}
return _489.run.apply(_489,args);
};
}
obj[_485].__preJoinArity=_48b;
}
return _489;
};
dojo.lang.extend(dojo.event.MethodJoinPoint,{squelch:false,unintercept:function(){
this.object[this.methodname]=this.methodfunc;
this.before=[];
this.after=[];
this.around=[];
},disconnect:dojo.lang.forward("unintercept"),run:function(){
var obj=this.object||dj_global;
var args=arguments;
var _491=[];
for(var x=0;x<args.length;x++){
_491[x]=args[x];
}
var _493=function(marr){
if(!marr){
dojo.debug("Null argument to unrollAdvice()");
return;
}
var _495=marr[0]||dj_global;
var _496=marr[1];
if(!_495[_496]){
dojo.raise("function \""+_496+"\" does not exist on \""+_495+"\"");
}
var _497=marr[2]||dj_global;
var _498=marr[3];
var msg=marr[6];
var _49a=marr[7];
if(_49a>-1){
if(_49a==0){
return;
}
marr[7]--;
}
var _49b;
var to={args:[],jp_:this,object:obj,proceed:function(){
return _495[_496].apply(_495,to.args);
}};
to.args=_491;
var _49d=parseInt(marr[4]);
var _49e=((!isNaN(_49d))&&(marr[4]!==null)&&(typeof marr[4]!="undefined"));
if(marr[5]){
var rate=parseInt(marr[5]);
var cur=new Date();
var _4a1=false;
if((marr["last"])&&((cur-marr.last)<=rate)){
if(dojo.event._canTimeout){
if(marr["delayTimer"]){
clearTimeout(marr.delayTimer);
}
var tod=parseInt(rate*2);
var mcpy=dojo.lang.shallowCopy(marr);
marr.delayTimer=setTimeout(function(){
mcpy[5]=0;
_493(mcpy);
},tod);
}
return;
}else{
marr.last=cur;
}
}
if(_498){
_497[_498].call(_497,to);
}else{
if((_49e)&&((dojo.render.html)||(dojo.render.svg))){
dj_global["setTimeout"](function(){
if(msg){
_495[_496].call(_495,to);
}else{
_495[_496].apply(_495,args);
}
},_49d);
}else{
if(msg){
_495[_496].call(_495,to);
}else{
_495[_496].apply(_495,args);
}
}
}
};
var _4a4=function(){
if(this.squelch){
try{
return _493.apply(this,arguments);
}
catch(e){
dojo.debug(e);
}
}else{
return _493.apply(this,arguments);
}
};
if((this["before"])&&(this.before.length>0)){
dojo.lang.forEach(this.before.concat(new Array()),_4a4);
}
var _4a5;
try{
if((this["around"])&&(this.around.length>0)){
var mi=new dojo.event.MethodInvocation(this,obj,args);
_4a5=mi.proceed();
}else{
if(this.methodfunc){
_4a5=this.object[this.methodname].apply(this.object,args);
}
}
}
catch(e){
if(!this.squelch){
dojo.debug(e,"when calling",this.methodname,"on",this.object,"with arguments",args);
dojo.raise(e);
}
}
if((this["after"])&&(this.after.length>0)){
dojo.lang.forEach(this.after.concat(new Array()),_4a4);
}
return (this.methodfunc)?_4a5:null;
},getArr:function(kind){
var type="after";
if((typeof kind=="string")&&(kind.indexOf("before")!=-1)){
type="before";
}else{
if(kind=="around"){
type="around";
}
}
if(!this[type]){
this[type]=[];
}
return this[type];
},kwAddAdvice:function(args){
this.addAdvice(args["adviceObj"],args["adviceFunc"],args["aroundObj"],args["aroundFunc"],args["adviceType"],args["precedence"],args["once"],args["delay"],args["rate"],args["adviceMsg"],args["maxCalls"]);
},addAdvice:function(_4aa,_4ab,_4ac,_4ad,_4ae,_4af,once,_4b1,rate,_4b3,_4b4){
var arr=this.getArr(_4ae);
if(!arr){
dojo.raise("bad this: "+this);
}
var ao=[_4aa,_4ab,_4ac,_4ad,_4b1,rate,_4b3,_4b4];
if(once){
if(this.hasAdvice(_4aa,_4ab,_4ae,arr)>=0){
return;
}
}
if(_4af=="first"){
arr.unshift(ao);
}else{
arr.push(ao);
}
},hasAdvice:function(_4b7,_4b8,_4b9,arr){
if(!arr){
arr=this.getArr(_4b9);
}
var ind=-1;
for(var x=0;x<arr.length;x++){
var aao=(typeof _4b8=="object")?(new String(_4b8)).toString():_4b8;
var a1o=(typeof arr[x][1]=="object")?(new String(arr[x][1])).toString():arr[x][1];
if((arr[x][0]==_4b7)&&(a1o==aao)){
ind=x;
}
}
return ind;
},removeAdvice:function(_4bf,_4c0,_4c1,once){
var arr=this.getArr(_4c1);
var ind=this.hasAdvice(_4bf,_4c0,_4c1,arr);
if(ind==-1){
return false;
}
while(ind!=-1){
arr.splice(ind,1);
if(once){
break;
}
ind=this.hasAdvice(_4bf,_4c0,_4c1,arr);
}
return true;
}});
dojo.provide("dojo.event.topic");
dojo.event.topic=new function(){
this.topics={};
this.getTopic=function(_4c5){
if(!this.topics[_4c5]){
this.topics[_4c5]=new this.TopicImpl(_4c5);
}
return this.topics[_4c5];
};
this.registerPublisher=function(_4c6,obj,_4c8){
var _4c6=this.getTopic(_4c6);
_4c6.registerPublisher(obj,_4c8);
};
this.subscribe=function(_4c9,obj,_4cb){
var _4c9=this.getTopic(_4c9);
_4c9.subscribe(obj,_4cb);
};
this.unsubscribe=function(_4cc,obj,_4ce){
var _4cc=this.getTopic(_4cc);
_4cc.unsubscribe(obj,_4ce);
};
this.destroy=function(_4cf){
this.getTopic(_4cf).destroy();
delete this.topics[_4cf];
};
this.publishApply=function(_4d0,args){
var _4d0=this.getTopic(_4d0);
_4d0.sendMessage.apply(_4d0,args);
};
this.publish=function(_4d2,_4d3){
var _4d2=this.getTopic(_4d2);
var args=[];
for(var x=1;x<arguments.length;x++){
args.push(arguments[x]);
}
_4d2.sendMessage.apply(_4d2,args);
};
};
dojo.event.topic.TopicImpl=function(_4d6){
this.topicName=_4d6;
this.subscribe=function(_4d7,_4d8){
var tf=_4d8||_4d7;
var to=(!_4d8)?dj_global:_4d7;
return dojo.event.kwConnect({srcObj:this,srcFunc:"sendMessage",adviceObj:to,adviceFunc:tf});
};
this.unsubscribe=function(_4db,_4dc){
var tf=(!_4dc)?_4db:_4dc;
var to=(!_4dc)?null:_4db;
return dojo.event.kwDisconnect({srcObj:this,srcFunc:"sendMessage",adviceObj:to,adviceFunc:tf});
};
this._getJoinPoint=function(){
return dojo.event.MethodJoinPoint.getForMethod(this,"sendMessage");
};
this.setSquelch=function(_4df){
this._getJoinPoint().squelch=_4df;
};
this.destroy=function(){
this._getJoinPoint().disconnect();
};
this.registerPublisher=function(_4e0,_4e1){
dojo.event.connect(_4e0,_4e1,this,"sendMessage");
};
this.sendMessage=function(_4e2){
};
};
dojo.provide("dojo.event.browser");
dojo._ie_clobber=new function(){
this.clobberNodes=[];
function nukeProp(node,prop){
try{
node[prop]=null;
}
catch(e){
}
try{
delete node[prop];
}
catch(e){
}
try{
node.removeAttribute(prop);
}
catch(e){
}
}
this.clobber=function(_4e5){
var na;
var tna;
if(_4e5){
tna=_4e5.all||_4e5.getElementsByTagName("*");
na=[_4e5];
for(var x=0;x<tna.length;x++){
if(tna[x]["__doClobber__"]){
na.push(tna[x]);
}
}
}else{
try{
window.onload=null;
}
catch(e){
}
na=(this.clobberNodes.length)?this.clobberNodes:document.all;
}
tna=null;
var _4e9={};
for(var i=na.length-1;i>=0;i=i-1){
var el=na[i];
try{
if(el&&el["__clobberAttrs__"]){
for(var j=0;j<el.__clobberAttrs__.length;j++){
nukeProp(el,el.__clobberAttrs__[j]);
}
nukeProp(el,"__clobberAttrs__");
nukeProp(el,"__doClobber__");
}
}
catch(e){
}
}
na=null;
};
};
if(dojo.render.html.ie){
dojo.addOnUnload(function(){
dojo._ie_clobber.clobber();
try{
if((dojo["widget"])&&(dojo.widget["manager"])){
dojo.widget.manager.destroyAll();
}
}
catch(e){
}
if(dojo.widget){
for(var name in dojo.widget._templateCache){
if(dojo.widget._templateCache[name].node){
dojo.dom.destroyNode(dojo.widget._templateCache[name].node);
dojo.widget._templateCache[name].node=null;
delete dojo.widget._templateCache[name].node;
}
}
}
try{
window.onload=null;
}
catch(e){
}
try{
window.onunload=null;
}
catch(e){
}
dojo._ie_clobber.clobberNodes=[];
});
}
dojo.event.browser=new function(){
var _4ee=0;
this.normalizedEventName=function(_4ef){
switch(_4ef){
case "CheckboxStateChange":
case "DOMAttrModified":
case "DOMMenuItemActive":
case "DOMMenuItemInactive":
case "DOMMouseScroll":
case "DOMNodeInserted":
case "DOMNodeRemoved":
case "RadioStateChange":
return _4ef;
break;
default:
var lcn=_4ef.toLowerCase();
return (lcn.indexOf("on")==0)?lcn.substr(2):lcn;
break;
}
};
this.clean=function(node){
if(dojo.render.html.ie){
dojo._ie_clobber.clobber(node);
}
};
this.addClobberNode=function(node){
if(!dojo.render.html.ie){
return;
}
if(!node["__doClobber__"]){
node.__doClobber__=true;
dojo._ie_clobber.clobberNodes.push(node);
node.__clobberAttrs__=[];
}
};
this.addClobberNodeAttrs=function(node,_4f4){
if(!dojo.render.html.ie){
return;
}
this.addClobberNode(node);
for(var x=0;x<_4f4.length;x++){
node.__clobberAttrs__.push(_4f4[x]);
}
};
this.removeListener=function(node,_4f7,fp,_4f9){
if(!_4f9){
var _4f9=false;
}
_4f7=dojo.event.browser.normalizedEventName(_4f7);
if(_4f7=="key"){
if(dojo.render.html.ie){
this.removeListener(node,"onkeydown",fp,_4f9);
}
_4f7="keypress";
}
if(node.removeEventListener){
node.removeEventListener(_4f7,fp,_4f9);
}
};
this.addListener=function(node,_4fb,fp,_4fd,_4fe){
if(!node){
return;
}
if(!_4fd){
var _4fd=false;
}
_4fb=dojo.event.browser.normalizedEventName(_4fb);
if(_4fb=="key"){
if(dojo.render.html.ie){
this.addListener(node,"onkeydown",fp,_4fd,_4fe);
}
_4fb="keypress";
}
if(!_4fe){
var _4ff=function(evt){
if(!evt){
evt=window.event;
}
var ret=fp(dojo.event.browser.fixEvent(evt,this));
if(_4fd){
dojo.event.browser.stopEvent(evt);
}
return ret;
};
}else{
_4ff=fp;
}
if(node.addEventListener){
node.addEventListener(_4fb,_4ff,_4fd);
return _4ff;
}else{
_4fb="on"+_4fb;
if(typeof node[_4fb]=="function"){
var _502=node[_4fb];
node[_4fb]=function(e){
_502(e);
return _4ff(e);
};
}else{
node[_4fb]=_4ff;
}
if(dojo.render.html.ie){
this.addClobberNodeAttrs(node,[_4fb]);
}
return _4ff;
}
};
this.isEvent=function(obj){
return (typeof obj!="undefined")&&(obj)&&(typeof Event!="undefined")&&(obj.eventPhase);
};
this.currentEvent=null;
this.callListener=function(_505,_506){
if(typeof _505!="function"){
dojo.raise("listener not a function: "+_505);
}
dojo.event.browser.currentEvent.currentTarget=_506;
return _505.call(_506,dojo.event.browser.currentEvent);
};
this._stopPropagation=function(){
dojo.event.browser.currentEvent.cancelBubble=true;
};
this._preventDefault=function(){
dojo.event.browser.currentEvent.returnValue=false;
};
this.keys={KEY_BACKSPACE:8,KEY_TAB:9,KEY_CLEAR:12,KEY_ENTER:13,KEY_SHIFT:16,KEY_CTRL:17,KEY_ALT:18,KEY_PAUSE:19,KEY_CAPS_LOCK:20,KEY_ESCAPE:27,KEY_SPACE:32,KEY_PAGE_UP:33,KEY_PAGE_DOWN:34,KEY_END:35,KEY_HOME:36,KEY_LEFT_ARROW:37,KEY_UP_ARROW:38,KEY_RIGHT_ARROW:39,KEY_DOWN_ARROW:40,KEY_INSERT:45,KEY_DELETE:46,KEY_HELP:47,KEY_LEFT_WINDOW:91,KEY_RIGHT_WINDOW:92,KEY_SELECT:93,KEY_NUMPAD_0:96,KEY_NUMPAD_1:97,KEY_NUMPAD_2:98,KEY_NUMPAD_3:99,KEY_NUMPAD_4:100,KEY_NUMPAD_5:101,KEY_NUMPAD_6:102,KEY_NUMPAD_7:103,KEY_NUMPAD_8:104,KEY_NUMPAD_9:105,KEY_NUMPAD_MULTIPLY:106,KEY_NUMPAD_PLUS:107,KEY_NUMPAD_ENTER:108,KEY_NUMPAD_MINUS:109,KEY_NUMPAD_PERIOD:110,KEY_NUMPAD_DIVIDE:111,KEY_F1:112,KEY_F2:113,KEY_F3:114,KEY_F4:115,KEY_F5:116,KEY_F6:117,KEY_F7:118,KEY_F8:119,KEY_F9:120,KEY_F10:121,KEY_F11:122,KEY_F12:123,KEY_F13:124,KEY_F14:125,KEY_F15:126,KEY_NUM_LOCK:144,KEY_SCROLL_LOCK:145};
this.revKeys=[];
for(var key in this.keys){
this.revKeys[this.keys[key]]=key;
}
this.fixEvent=function(evt,_509){
if(!evt){
if(window["event"]){
evt=window.event;
}
}
if((evt["type"])&&(evt["type"].indexOf("key")==0)){
evt.keys=this.revKeys;
for(var key in this.keys){
evt[key]=this.keys[key];
}
if(evt["type"]=="keydown"&&dojo.render.html.ie){
switch(evt.keyCode){
case evt.KEY_SHIFT:
case evt.KEY_CTRL:
case evt.KEY_ALT:
case evt.KEY_CAPS_LOCK:
case evt.KEY_LEFT_WINDOW:
case evt.KEY_RIGHT_WINDOW:
case evt.KEY_SELECT:
case evt.KEY_NUM_LOCK:
case evt.KEY_SCROLL_LOCK:
case evt.KEY_NUMPAD_0:
case evt.KEY_NUMPAD_1:
case evt.KEY_NUMPAD_2:
case evt.KEY_NUMPAD_3:
case evt.KEY_NUMPAD_4:
case evt.KEY_NUMPAD_5:
case evt.KEY_NUMPAD_6:
case evt.KEY_NUMPAD_7:
case evt.KEY_NUMPAD_8:
case evt.KEY_NUMPAD_9:
case evt.KEY_NUMPAD_PERIOD:
break;
case evt.KEY_NUMPAD_MULTIPLY:
case evt.KEY_NUMPAD_PLUS:
case evt.KEY_NUMPAD_ENTER:
case evt.KEY_NUMPAD_MINUS:
case evt.KEY_NUMPAD_DIVIDE:
break;
case evt.KEY_PAUSE:
case evt.KEY_TAB:
case evt.KEY_BACKSPACE:
case evt.KEY_ENTER:
case evt.KEY_ESCAPE:
case evt.KEY_PAGE_UP:
case evt.KEY_PAGE_DOWN:
case evt.KEY_END:
case evt.KEY_HOME:
case evt.KEY_LEFT_ARROW:
case evt.KEY_UP_ARROW:
case evt.KEY_RIGHT_ARROW:
case evt.KEY_DOWN_ARROW:
case evt.KEY_INSERT:
case evt.KEY_DELETE:
case evt.KEY_F1:
case evt.KEY_F2:
case evt.KEY_F3:
case evt.KEY_F4:
case evt.KEY_F5:
case evt.KEY_F6:
case evt.KEY_F7:
case evt.KEY_F8:
case evt.KEY_F9:
case evt.KEY_F10:
case evt.KEY_F11:
case evt.KEY_F12:
case evt.KEY_F12:
case evt.KEY_F13:
case evt.KEY_F14:
case evt.KEY_F15:
case evt.KEY_CLEAR:
case evt.KEY_HELP:
evt.key=evt.keyCode;
break;
default:
if(evt.ctrlKey||evt.altKey){
var _50b=evt.keyCode;
if(_50b>=65&&_50b<=90&&evt.shiftKey==false){
_50b+=32;
}
if(_50b>=1&&_50b<=26&&evt.ctrlKey){
_50b+=96;
}
evt.key=String.fromCharCode(_50b);
}
}
}else{
if(evt["type"]=="keypress"){
if(dojo.render.html.opera){
if(evt.which==0){
evt.key=evt.keyCode;
}else{
if(evt.which>0){
switch(evt.which){
case evt.KEY_SHIFT:
case evt.KEY_CTRL:
case evt.KEY_ALT:
case evt.KEY_CAPS_LOCK:
case evt.KEY_NUM_LOCK:
case evt.KEY_SCROLL_LOCK:
break;
case evt.KEY_PAUSE:
case evt.KEY_TAB:
case evt.KEY_BACKSPACE:
case evt.KEY_ENTER:
case evt.KEY_ESCAPE:
evt.key=evt.which;
break;
default:
var _50b=evt.which;
if((evt.ctrlKey||evt.altKey||evt.metaKey)&&(evt.which>=65&&evt.which<=90&&evt.shiftKey==false)){
_50b+=32;
}
evt.key=String.fromCharCode(_50b);
}
}
}
}else{
if(dojo.render.html.ie){
if(!evt.ctrlKey&&!evt.altKey&&evt.keyCode>=evt.KEY_SPACE){
evt.key=String.fromCharCode(evt.keyCode);
}
}else{
if(dojo.render.html.safari){
switch(evt.keyCode){
case 25:
evt.key=evt.KEY_TAB;
evt.shift=true;
break;
case 63232:
evt.key=evt.KEY_UP_ARROW;
break;
case 63233:
evt.key=evt.KEY_DOWN_ARROW;
break;
case 63234:
evt.key=evt.KEY_LEFT_ARROW;
break;
case 63235:
evt.key=evt.KEY_RIGHT_ARROW;
break;
case 63236:
evt.key=evt.KEY_F1;
break;
case 63237:
evt.key=evt.KEY_F2;
break;
case 63238:
evt.key=evt.KEY_F3;
break;
case 63239:
evt.key=evt.KEY_F4;
break;
case 63240:
evt.key=evt.KEY_F5;
break;
case 63241:
evt.key=evt.KEY_F6;
break;
case 63242:
evt.key=evt.KEY_F7;
break;
case 63243:
evt.key=evt.KEY_F8;
break;
case 63244:
evt.key=evt.KEY_F9;
break;
case 63245:
evt.key=evt.KEY_F10;
break;
case 63246:
evt.key=evt.KEY_F11;
break;
case 63247:
evt.key=evt.KEY_F12;
break;
case 63250:
evt.key=evt.KEY_PAUSE;
break;
case 63272:
evt.key=evt.KEY_DELETE;
break;
case 63273:
evt.key=evt.KEY_HOME;
break;
case 63275:
evt.key=evt.KEY_END;
break;
case 63276:
evt.key=evt.KEY_PAGE_UP;
break;
case 63277:
evt.key=evt.KEY_PAGE_DOWN;
break;
case 63302:
evt.key=evt.KEY_INSERT;
break;
case 63248:
case 63249:
case 63289:
break;
default:
evt.key=evt.charCode>=evt.KEY_SPACE?String.fromCharCode(evt.charCode):evt.keyCode;
}
}else{
evt.key=evt.charCode>0?String.fromCharCode(evt.charCode):evt.keyCode;
}
}
}
}
}
}
if(dojo.render.html.ie){
if(!evt.target){
evt.target=evt.srcElement;
}
if(!evt.currentTarget){
evt.currentTarget=(_509?_509:evt.srcElement);
}
if(!evt.layerX){
evt.layerX=evt.offsetX;
}
if(!evt.layerY){
evt.layerY=evt.offsetY;
}
var doc=(evt.srcElement&&evt.srcElement.ownerDocument)?evt.srcElement.ownerDocument:document;
var _50d=((dojo.render.html.ie55)||(doc["compatMode"]=="BackCompat"))?doc.body:doc.documentElement;
if(!evt.pageX){
evt.pageX=evt.clientX+(_50d.scrollLeft||0);
}
if(!evt.pageY){
evt.pageY=evt.clientY+(_50d.scrollTop||0);
}
if(evt.type=="mouseover"){
evt.relatedTarget=evt.fromElement;
}
if(evt.type=="mouseout"){
evt.relatedTarget=evt.toElement;
}
this.currentEvent=evt;
evt.callListener=this.callListener;
evt.stopPropagation=this._stopPropagation;
evt.preventDefault=this._preventDefault;
}
return evt;
};
this.stopEvent=function(evt){
if(window.event){
evt.cancelBubble=true;
evt.returnValue=false;
}else{
evt.preventDefault();
evt.stopPropagation();
}
};
};
dojo.kwCompoundRequire({common:["dojo.event.common","dojo.event.topic"],browser:["dojo.event.browser"],dashboard:["dojo.event.browser"]});
dojo.provide("dojo.event.*");
dojo.provide("dojo.widget.Manager");
dojo.widget.manager=new function(){
this.widgets=[];
this.widgetIds=[];
this.topWidgets={};
var _50f={};
var _510=[];
this.getUniqueId=function(_511){
var _512;
do{
_512=_511+"_"+(_50f[_511]!=undefined?++_50f[_511]:_50f[_511]=0);
}while(this.getWidgetById(_512));
return _512;
};
this.add=function(_513){
this.widgets.push(_513);
if(!_513.extraArgs["id"]){
_513.extraArgs["id"]=_513.extraArgs["ID"];
}
if(_513.widgetId==""){
if(_513["id"]){
_513.widgetId=_513["id"];
}else{
if(_513.extraArgs["id"]){
_513.widgetId=_513.extraArgs["id"];
}else{
_513.widgetId=this.getUniqueId(_513.ns+"_"+_513.widgetType);
}
}
}
if(this.widgetIds[_513.widgetId]){
dojo.debug("widget ID collision on ID: "+_513.widgetId);
}
this.widgetIds[_513.widgetId]=_513;
};
this.destroyAll=function(){
for(var x=this.widgets.length-1;x>=0;x--){
try{
this.widgets[x].destroy(true);
delete this.widgets[x];
}
catch(e){
}
}
};
this.remove=function(_515){
if(dojo.lang.isNumber(_515)){
var tw=this.widgets[_515].widgetId;
delete this.topWidgets[tw];
delete this.widgetIds[tw];
this.widgets.splice(_515,1);
}else{
this.removeById(_515);
}
};
this.removeById=function(id){
if(!dojo.lang.isString(id)){
id=id["widgetId"];
if(!id){
dojo.debug("invalid widget or id passed to removeById");
return;
}
}
for(var i=0;i<this.widgets.length;i++){
if(this.widgets[i].widgetId==id){
this.remove(i);
break;
}
}
};
this.getWidgetById=function(id){
if(dojo.lang.isString(id)){
return this.widgetIds[id];
}
return id;
};
this.getWidgetsByType=function(type){
var lt=type.toLowerCase();
var _51c=(type.indexOf(":")<0?function(x){
return x.widgetType.toLowerCase();
}:function(x){
return x.getNamespacedType();
});
var ret=[];
dojo.lang.forEach(this.widgets,function(x){
if(_51c(x)==lt){
ret.push(x);
}
});
return ret;
};
this.getWidgetsByFilter=function(_521,_522){
var ret=[];
dojo.lang.every(this.widgets,function(x){
if(_521(x)){
ret.push(x);
if(_522){
return false;
}
}
return true;
});
return (_522?ret[0]:ret);
};
this.getAllWidgets=function(){
return this.widgets.concat();
};
this.getWidgetByNode=function(node){
var w=this.getAllWidgets();
node=dojo.byId(node);
for(var i=0;i<w.length;i++){
if(w[i].domNode==node){
return w[i];
}
}
return null;
};
this.byId=this.getWidgetById;
this.byType=this.getWidgetsByType;
this.byFilter=this.getWidgetsByFilter;
this.byNode=this.getWidgetByNode;
var _528={};
var _529=["dojo.widget"];
for(var i=0;i<_529.length;i++){
_529[_529[i]]=true;
}
this.registerWidgetPackage=function(_52b){
if(!_529[_52b]){
_529[_52b]=true;
_529.push(_52b);
}
};
this.getWidgetPackageList=function(){
return dojo.lang.map(_529,function(elt){
return (elt!==true?elt:undefined);
});
};
this.getImplementation=function(_52d,_52e,_52f,ns){
var impl=this.getImplementationName(_52d,ns);
if(impl){
var ret=_52e?new impl(_52e):new impl();
return ret;
}
};
function buildPrefixCache(){
for(var _533 in dojo.render){
if(dojo.render[_533]["capable"]===true){
var _534=dojo.render[_533].prefixes;
for(var i=0;i<_534.length;i++){
_510.push(_534[i].toLowerCase());
}
}
}
}
var _536=function(_537,_538){
if(!_538){
return null;
}
for(var i=0,l=_510.length,_53b;i<=l;i++){
_53b=(i<l?_538[_510[i]]:_538);
if(!_53b){
continue;
}
for(var name in _53b){
if(name.toLowerCase()==_537){
return _53b[name];
}
}
}
return null;
};
var _53d=function(_53e,_53f){
var _540=dojo.getObject(_53f,false);
return (_540?_536(_53e,_540):null);
};
this.getImplementationName=function(_541,ns){
var _543=_541.toLowerCase();
ns=ns||"dojo";
var imps=_528[ns]||(_528[ns]={});
var impl=imps[_543];
if(impl){
return impl;
}
if(!_510.length){
buildPrefixCache();
}
var _546=dojo.ns.get(ns);
if(!_546){
dojo.ns.register(ns,ns+".widget");
_546=dojo.ns.get(ns);
}
if(_546){
_546.resolve(_541);
}
impl=_53d(_543,_546.module);
if(impl){
return (imps[_543]=impl);
}
_546=dojo.ns.require(ns);
if((_546)&&(_546.resolver)){
_546.resolve(_541);
impl=_53d(_543,_546.module);
if(impl){
return (imps[_543]=impl);
}
}
throw new Error("Could not locate widget implementation for \""+_541+"\" in \""+_546.module+"\" registered to namespace \""+_546.name+"\"");
};
this.resizing=false;
this.onWindowResized=function(){
if(this.resizing){
return;
}
try{
this.resizing=true;
for(var id in this.topWidgets){
var _548=this.topWidgets[id];
if(_548.checkSize){
_548.checkSize();
}
}
}
catch(e){
}
finally{
this.resizing=false;
}
};
if(typeof window!="undefined"){
dojo.addOnLoad(this,"onWindowResized");
dojo.event.connect(window,"onresize",this,"onWindowResized");
}
};
(function(){
var dw=dojo.widget;
var dwm=dw.manager;
var h=dojo.lang.curry(dojo.lang,"hitch",dwm);
var g=function(_54d,_54e){
dw[(_54e||_54d)]=h(_54d);
};
g("add","addWidget");
g("destroyAll","destroyAllWidgets");
g("remove","removeWidget");
g("removeById","removeWidgetById");
g("getWidgetById");
g("getWidgetById","byId");
g("getWidgetsByType");
g("getWidgetsByFilter");
g("getWidgetsByType","byType");
g("getWidgetsByFilter","byFilter");
g("getWidgetByNode","byNode");
dw.all=function(n){
var _550=dwm.getAllWidgets.apply(dwm,arguments);
if(arguments.length>0){
return _550[n];
}
return _550;
};
g("registerWidgetPackage");
g("getImplementation","getWidgetImplementation");
g("getImplementationName","getWidgetImplementationName");
dw.widgets=dwm.widgets;
dw.widgetIds=dwm.widgetIds;
dw.root=dwm.root;
})();
dojo.provide("dojo.a11y");
dojo.a11y={imgPath:dojo.uri.moduleUri("dojo.widget","templates/images"),doAccessibleCheck:true,accessible:null,checkAccessible:function(){
if(this.accessible===null){
this.accessible=false;
if(this.doAccessibleCheck==true){
this.accessible=this.testAccessible();
}
}
return this.accessible;
},testAccessible:function(){
this.accessible=false;
if(dojo.render.html.ie||dojo.render.html.mozilla){
var div=document.createElement("div");
div.style.backgroundImage="url(\""+this.imgPath+"/tab_close.gif\")";
dojo.body().appendChild(div);
var _552=null;
if(window.getComputedStyle){
var _553=getComputedStyle(div,"");
_552=_553.getPropertyValue("background-image");
}else{
_552=div.currentStyle.backgroundImage;
}
var _554=false;
if(_552!=null&&(_552=="none"||_552=="url(invalid-url:)")){
this.accessible=true;
}
dojo.body().removeChild(div);
}
return this.accessible;
},setAccessible:function(_555){
this.accessible=_555;
},setCheckAccessible:function(_556){
this.doAccessibleCheck=_556;
},setAccessibleMode:function(){
if(this.accessible===null){
if(this.checkAccessible()){
dojo.render.html.prefixes.unshift("a11y");
}
}
return this.accessible;
}};
dojo.provide("dojo.widget.Widget");
dojo.declare("dojo.widget.Widget",null,function(){
this.children=[];
this.extraArgs={};
},{parent:null,isTopLevel:false,disabled:false,isContainer:false,widgetId:"",widgetType:"Widget",ns:"dojo",getNamespacedType:function(){
return (this.ns?this.ns+":"+this.widgetType:this.widgetType).toLowerCase();
},toString:function(){
return "[Widget "+this.getNamespacedType()+", "+(this.widgetId||"NO ID")+"]";
},repr:function(){
return this.toString();
},enable:function(){
this.disabled=false;
},disable:function(){
this.disabled=true;
},onResized:function(){
this.notifyChildrenOfResize();
},notifyChildrenOfResize:function(){
for(var i=0;i<this.children.length;i++){
var _558=this.children[i];
if(_558.onResized){
_558.onResized();
}
}
},create:function(args,_55a,_55b,ns){
if(ns){
this.ns=ns;
}
this.satisfyPropertySets(args,_55a,_55b);
this.mixInProperties(args,_55a,_55b);
this.postMixInProperties(args,_55a,_55b);
dojo.widget.manager.add(this);
this.buildRendering(args,_55a,_55b);
this.initialize(args,_55a,_55b);
this.postInitialize(args,_55a,_55b);
this.postCreate(args,_55a,_55b);
return this;
},destroy:function(_55d){
if(this.parent){
this.parent.removeChild(this);
}
this.destroyChildren();
this.uninitialize();
this.destroyRendering(_55d);
dojo.widget.manager.removeById(this.widgetId);
},destroyChildren:function(){
var _55e;
var i=0;
while(this.children.length>i){
_55e=this.children[i];
if(_55e instanceof dojo.widget.Widget){
this.removeChild(_55e);
_55e.destroy();
continue;
}
i++;
}
},getChildrenOfType:function(type,_561){
var ret=[];
var _563=dojo.lang.isFunction(type);
if(!_563){
type=type.toLowerCase();
}
for(var x=0;x<this.children.length;x++){
if(_563){
if(this.children[x] instanceof type){
ret.push(this.children[x]);
}
}else{
if(this.children[x].widgetType.toLowerCase()==type){
ret.push(this.children[x]);
}
}
if(_561){
ret=ret.concat(this.children[x].getChildrenOfType(type,_561));
}
}
return ret;
},getDescendants:function(){
var _565=[];
var _566=[this];
var elem;
while((elem=_566.pop())){
_565.push(elem);
if(elem.children){
dojo.lang.forEach(elem.children,function(elem){
_566.push(elem);
});
}
}
return _565;
},isFirstChild:function(){
return this===this.parent.children[0];
},isLastChild:function(){
return this===this.parent.children[this.parent.children.length-1];
},satisfyPropertySets:function(args){
return args;
},mixInProperties:function(args,frag){
if((args["fastMixIn"])||(frag["fastMixIn"])){
for(var x in args){
this[x]=args[x];
}
return;
}
var _56d;
var _56e=dojo.widget.lcArgsCache[this.widgetType];
if(_56e==null){
_56e={};
for(var y in this){
_56e[((new String(y)).toLowerCase())]=y;
}
dojo.widget.lcArgsCache[this.widgetType]=_56e;
}
var _570={};
for(var x in args){
if(!this[x]){
var y=_56e[(new String(x)).toLowerCase()];
if(y){
args[y]=args[x];
x=y;
}
}
if(_570[x]){
continue;
}
_570[x]=true;
if((typeof this[x])!=(typeof _56d)){
if(typeof args[x]!="string"){
this[x]=args[x];
}else{
if(dojo.lang.isString(this[x])){
this[x]=args[x];
}else{
if(dojo.lang.isNumber(this[x])){
this[x]=new Number(args[x]);
}else{
if(dojo.lang.isBoolean(this[x])){
this[x]=(args[x].toLowerCase()=="false")?false:true;
}else{
if(dojo.lang.isFunction(this[x])){
if(args[x].search(/[^\w\.]+/i)==-1){
this[x]=dojo.getObject(args[x],false);
}else{
var tn=dojo.lang.nameAnonFunc(new Function(args[x]),this);
dojo.event.kwConnect({srcObj:this,srcFunc:x,adviceObj:this,adviceFunc:tn});
}
}else{
if(dojo.lang.isArray(this[x])){
this[x]=args[x].split(";");
}else{
if(this[x] instanceof Date){
this[x]=new Date(Number(args[x]));
}else{
if(typeof this[x]=="object"){
if(this[x] instanceof dojo.uri.Uri){
this[x]=dojo.uri.dojoUri(args[x]);
}else{
var _572=args[x].split(";");
for(var y=0;y<_572.length;y++){
var si=_572[y].indexOf(":");
if((si!=-1)&&(_572[y].length>si)){
this[x][_572[y].substr(0,si).replace(/^\s+|\s+$/g,"")]=_572[y].substr(si+1);
}
}
}
}else{
this[x]=args[x];
}
}
}
}
}
}
}
}
}else{
this.extraArgs[x.toLowerCase()]=args[x];
}
}
},postMixInProperties:function(args,frag,_576){
},initialize:function(args,frag,_579){
return false;
},postInitialize:function(args,frag,_57c){
return false;
},postCreate:function(args,frag,_57f){
return false;
},uninitialize:function(){
return false;
},buildRendering:function(args,frag,_582){
dojo.unimplemented("dojo.widget.Widget.buildRendering, on "+this.toString()+", ");
return false;
},destroyRendering:function(){
dojo.unimplemented("dojo.widget.Widget.destroyRendering");
return false;
},addedTo:function(_583){
},addChild:function(_584){
dojo.unimplemented("dojo.widget.Widget.addChild");
return false;
},removeChild:function(_585){
for(var x=0;x<this.children.length;x++){
if(this.children[x]===_585){
this.children.splice(x,1);
_585.parent=null;
break;
}
}
return _585;
},getPreviousSibling:function(){
var idx=this.getParentIndex();
if(idx<=0){
return null;
}
return this.parent.children[idx-1];
},getSiblings:function(){
return this.parent.children;
},getParentIndex:function(){
return dojo.lang.indexOf(this.parent.children,this,true);
},getNextSibling:function(){
var idx=this.getParentIndex();
if(idx==this.parent.children.length-1){
return null;
}
if(idx<0){
return null;
}
return this.parent.children[idx+1];
}});
dojo.widget.lcArgsCache={};
dojo.widget.tags={};
dojo.widget.tags["dojo:propertyset"]=function(_589,_58a,_58b){
var _58c=_58a.parseProperties(_589["dojo:propertyset"]);
};
dojo.widget.tags["dojo:connect"]=function(_58d,_58e,_58f){
var _590=_58e.parseProperties(_58d["dojo:connect"]);
};
dojo.widget.buildWidgetFromParseTree=function(type,frag,_593,_594,_595,_596){
dojo.a11y.setAccessibleMode();
var _597=type.split(":");
_597=(_597.length==2)?_597[1]:type;
var _598=_596||_593.parseProperties(frag[frag["ns"]+":"+_597]);
var _599=dojo.widget.manager.getImplementation(_597,null,null,frag["ns"]);
if(!_599){
throw new Error("cannot find \""+type+"\" widget");
}else{
if(!_599.create){
throw new Error("\""+type+"\" widget object has no \"create\" method and does not appear to implement *Widget");
}
}
_598["dojoinsertionindex"]=_595;
var ret=_599.create(_598,frag,_594,frag["ns"]);
return ret;
};
dojo.widget.defineWidget=function(_59b,_59c,_59d,init,_59f){
if(dojo.lang.isString(arguments[3])){
dojo.widget._defineWidget(arguments[0],arguments[3],arguments[1],arguments[4],arguments[2]);
}else{
var args=[arguments[0]],p=3;
if(dojo.lang.isString(arguments[1])){
args.push(arguments[1],arguments[2]);
}else{
args.push("",arguments[1]);
p=2;
}
if(dojo.lang.isFunction(arguments[p])){
args.push(arguments[p],arguments[p+1]);
}else{
args.push(null,arguments[p]);
}
dojo.widget._defineWidget.apply(this,args);
}
};
dojo.widget.defineWidget.renderers="html|svg|vml";
dojo.widget._defineWidget=function(_5a2,_5a3,_5a4,init,_5a6){
var _5a7=_5a2.split(".");
var type=_5a7.pop();
var regx="\\.("+(_5a3?_5a3+"|":"")+dojo.widget.defineWidget.renderers+")\\.";
var r=_5a2.search(new RegExp(regx));
_5a7=(r<0?_5a7.join("."):_5a2.substr(0,r));
dojo.widget.manager.registerWidgetPackage(_5a7);
var pos=_5a7.indexOf(".");
var _5ac=(pos>-1)?_5a7.substring(0,pos):_5a7;
_5a6=(_5a6)||{};
_5a6.widgetType=type;
if((!init)&&(_5a6["classConstructor"])){
init=_5a6.classConstructor;
delete _5a6.classConstructor;
}
dojo.declare(_5a2,_5a4,init,_5a6);
};
dojo.provide("dojo.widget.Parse");
dojo.widget.Parse=function(_5ad){
this.propertySetsList=[];
this.fragment=_5ad;
this.createComponents=function(frag,_5af){
var _5b0=[];
var _5b1=false;
try{
if(frag&&frag.tagName&&(frag!=frag.nodeRef)){
var _5b2=dojo.widget.tags;
var tna=String(frag.tagName).split(";");
for(var x=0;x<tna.length;x++){
var ltn=tna[x].replace(/^\s+|\s+$/g,"").toLowerCase();
frag.tagName=ltn;
var ret;
if(_5b2[ltn]){
_5b1=true;
ret=_5b2[ltn](frag,this,_5af,frag.index);
_5b0.push(ret);
}else{
if(ltn.indexOf(":")==-1){
ltn="dojo:"+ltn;
}
ret=dojo.widget.buildWidgetFromParseTree(ltn,frag,this,_5af,frag.index);
if(ret){
_5b1=true;
_5b0.push(ret);
}
}
}
}
}
catch(e){
dojo.debug("dojo.widget.Parse: error:",e);
}
if(!_5b1){
_5b0=_5b0.concat(this.createSubComponents(frag,_5af));
}
return _5b0;
};
this.createSubComponents=function(_5b7,_5b8){
var frag,_5ba=[];
for(var item in _5b7){
frag=_5b7[item];
if(frag&&typeof frag=="object"&&(frag!=_5b7.nodeRef)&&(frag!=_5b7.tagName)&&(item.indexOf("$")==-1)){
_5ba=_5ba.concat(this.createComponents(frag,_5b8));
}
}
return _5ba;
};
this.parsePropertySets=function(_5bc){
return [];
};
this.parseProperties=function(_5bd){
var _5be={};
for(var item in _5bd){
if((_5bd[item]==_5bd.tagName)||(_5bd[item]==_5bd.nodeRef)){
}else{
var frag=_5bd[item];
if(frag.tagName&&dojo.widget.tags[frag.tagName.toLowerCase()]){
}else{
if(frag[0]&&frag[0].value!=""&&frag[0].value!=null){
try{
if(item.toLowerCase()=="dataprovider"){
var _5c1=this;
this.getDataProvider(_5c1,frag[0].value);
_5be.dataProvider=this.dataProvider;
}
_5be[item]=frag[0].value;
var _5c2=this.parseProperties(frag);
for(var _5c3 in _5c2){
_5be[_5c3]=_5c2[_5c3];
}
}
catch(e){
dojo.debug(e);
}
}
}
switch(item.toLowerCase()){
case "checked":
case "disabled":
if(typeof _5be[item]!="boolean"){
_5be[item]=true;
}
break;
}
}
}
return _5be;
};
this.getDataProvider=function(_5c4,_5c5){
dojo.io.bind({url:_5c5,load:function(type,_5c7){
if(type=="load"){
_5c4.dataProvider=_5c7;
}
},mimetype:"text/javascript",sync:true});
};
this.getPropertySetById=function(_5c8){
for(var x=0;x<this.propertySetsList.length;x++){
if(_5c8==this.propertySetsList[x]["id"][0].value){
return this.propertySetsList[x];
}
}
return "";
};
this.getPropertySetsByType=function(_5ca){
var _5cb=[];
for(var x=0;x<this.propertySetsList.length;x++){
var cpl=this.propertySetsList[x];
var cpcc=cpl.componentClass||cpl.componentType||null;
var _5cf=this.propertySetsList[x]["id"][0].value;
if(cpcc&&(_5cf==cpcc[0].value)){
_5cb.push(cpl);
}
}
return _5cb;
};
this.getPropertySets=function(_5d0){
var ppl="dojo:propertyproviderlist";
var _5d2=[];
var _5d3=_5d0.tagName;
if(_5d0[ppl]){
var _5d4=_5d0[ppl].value.split(" ");
for(var _5d5 in _5d4){
if((_5d5.indexOf("..")==-1)&&(_5d5.indexOf("://")==-1)){
var _5d6=this.getPropertySetById(_5d5);
if(_5d6!=""){
_5d2.push(_5d6);
}
}else{
}
}
}
return this.getPropertySetsByType(_5d3).concat(_5d2);
};
this.createComponentFromScript=function(_5d7,_5d8,_5d9,ns){
_5d9.fastMixIn=true;
var ltn=(ns||"dojo")+":"+_5d8.toLowerCase();
if(dojo.widget.tags[ltn]){
return [dojo.widget.tags[ltn](_5d9,this,null,null,_5d9)];
}
return [dojo.widget.buildWidgetFromParseTree(ltn,_5d9,this,null,null,_5d9)];
};
};
dojo.widget._parser_collection={"dojo":new dojo.widget.Parse()};
dojo.widget.getParser=function(name){
if(!name){
name="dojo";
}
if(!this._parser_collection[name]){
this._parser_collection[name]=new dojo.widget.Parse();
}
return this._parser_collection[name];
};
dojo.widget.createWidget=function(name,_5de,_5df,_5e0){
var _5e1=false;
var _5e2=(typeof name=="string");
if(_5e2){
var pos=name.indexOf(":");
var ns=(pos>-1)?name.substring(0,pos):"dojo";
if(pos>-1){
name=name.substring(pos+1);
}
var _5e5=name.toLowerCase();
var _5e6=ns+":"+_5e5;
_5e1=(dojo.byId(name)&&!dojo.widget.tags[_5e6]);
}
if((arguments.length==1)&&(_5e1||!_5e2)){
var xp=new dojo.xml.Parse();
var tn=_5e1?dojo.byId(name):name;
return dojo.widget.getParser().createComponents(xp.parseElement(tn,null,true))[0];
}
function fromScript(_5e9,name,_5eb,ns){
_5eb[_5e6]={dojotype:[{value:_5e5}],nodeRef:_5e9,fastMixIn:true};
_5eb.ns=ns;
return dojo.widget.getParser().createComponentFromScript(_5e9,name,_5eb,ns);
}
_5de=_5de||{};
var _5ed=false;
var tn=null;
var h=dojo.render.html.capable;
if(h){
tn=document.createElement("span");
}
if(!_5df){
_5ed=true;
_5df=tn;
if(h){
dojo.body().appendChild(_5df);
}
}else{
if(_5e0){
dojo.dom.insertAtPosition(tn,_5df,_5e0);
}else{
tn=_5df;
}
}
var _5ef=fromScript(tn,name.toLowerCase(),_5de,ns);
if((!_5ef)||(!_5ef[0])||(typeof _5ef[0].widgetType=="undefined")){
throw new Error("createWidget: Creation of \""+name+"\" widget failed.");
}
try{
if(_5ed&&_5ef[0].domNode.parentNode){
_5ef[0].domNode.parentNode.removeChild(_5ef[0].domNode);
}
}
catch(e){
dojo.debug(e);
}
return _5ef[0];
};
dojo.provide("dojo.widget.DomWidget");
dojo.widget._cssFiles={};
dojo.widget._cssStrings={};
dojo.widget._templateCache={};
dojo.widget.defaultStrings={dojoRoot:dojo.hostenv.getBaseScriptUri(),dojoWidgetModuleUri:dojo.uri.moduleUri("dojo.widget"),baseScriptUri:dojo.hostenv.getBaseScriptUri()};
dojo.widget.fillFromTemplateCache=function(obj,_5f1,_5f2,_5f3){
var _5f4=_5f1||obj.templatePath;
var _5f5=dojo.widget._templateCache;
if(!_5f4&&!obj["widgetType"]){
do{
var _5f6="__dummyTemplate__"+dojo.widget._templateCache.dummyCount++;
}while(_5f5[_5f6]);
obj.widgetType=_5f6;
}
var wt=_5f4?_5f4.toString():obj.widgetType;
var ts=_5f5[wt];
if(!ts){
_5f5[wt]={"string":null,"node":null};
if(_5f3){
ts={};
}else{
ts=_5f5[wt];
}
}
if((!obj.templateString)&&(!_5f3)){
obj.templateString=_5f2||ts["string"];
}
if(obj.templateString){
obj.templateString=this._sanitizeTemplateString(obj.templateString);
}
if((!obj.templateNode)&&(!_5f3)){
obj.templateNode=ts["node"];
}
if((!obj.templateNode)&&(!obj.templateString)&&(_5f4)){
var _5f9=this._sanitizeTemplateString(dojo.hostenv.getText(_5f4));
obj.templateString=_5f9;
if(!_5f3){
_5f5[wt]["string"]=_5f9;
}
}
if((!ts["string"])&&(!_5f3)){
ts.string=obj.templateString;
}
};
dojo.widget._sanitizeTemplateString=function(_5fa){
if(_5fa){
_5fa=_5fa.replace(/^\s*<\?xml(\s)+version=[\'\"](\d)*.(\d)*[\'\"](\s)*\?>/im,"");
var _5fb=_5fa.match(/<body[^>]*>\s*([\s\S]+)\s*<\/body>/im);
if(_5fb){
_5fa=_5fb[1];
}
}else{
_5fa="";
}
return _5fa;
};
dojo.widget._templateCache.dummyCount=0;
dojo.widget.attachProperties=["dojoAttachPoint","id"];
dojo.widget.eventAttachProperty="dojoAttachEvent";
dojo.widget.onBuildProperty="dojoOnBuild";
dojo.widget.waiNames=["waiRole","waiState"];
dojo.widget.wai={waiRole:{name:"waiRole","namespace":"http://www.w3.org/TR/xhtml2",alias:"x2",prefix:"wairole:"},waiState:{name:"waiState","namespace":"http://www.w3.org/2005/07/aaa",alias:"aaa",prefix:""},setAttr:function(node,ns,attr,_5ff){
if(dojo.render.html.ie){
node.setAttribute(this[ns].alias+":"+attr,this[ns].prefix+_5ff);
}else{
node.setAttributeNS(this[ns]["namespace"],attr,this[ns].prefix+_5ff);
}
},getAttr:function(node,ns,attr){
if(dojo.render.html.ie){
return node.getAttribute(this[ns].alias+":"+attr);
}else{
return node.getAttributeNS(this[ns]["namespace"],attr);
}
},removeAttr:function(node,ns,attr){
var _606=true;
if(dojo.render.html.ie){
_606=node.removeAttribute(this[ns].alias+":"+attr);
}else{
node.removeAttributeNS(this[ns]["namespace"],attr);
}
return _606;
}};
dojo.widget.attachTemplateNodes=function(_607,_608,_609){
var _60a=dojo.dom.ELEMENT_NODE;
function trim(str){
return str.replace(/^\s+|\s+$/g,"");
}
if(!_607){
_607=_608.domNode;
}
if(_607.nodeType!=_60a){
return;
}
var _60c=_607.all||_607.getElementsByTagName("*");
var _60d=_608;
for(var x=-1;x<_60c.length;x++){
var _60f=(x==-1)?_607:_60c[x];
var _610=[];
if(!_608.widgetsInTemplate||!_60f.getAttribute("dojoType")){
for(var y=0;y<this.attachProperties.length;y++){
var _612=_60f.getAttribute(this.attachProperties[y]);
if(_612){
_610=_612.split(";");
for(var z=0;z<_610.length;z++){
if(dojo.lang.isArray(_608[_610[z]])){
_608[_610[z]].push(_60f);
}else{
_608[_610[z]]=_60f;
}
}
break;
}
}
var _614=_60f.getAttribute(this.eventAttachProperty);
if(_614){
var evts=_614.split(";");
for(var y=0;y<evts.length;y++){
if((!evts[y])||(!evts[y].length)){
continue;
}
var _616=null;
var tevt=trim(evts[y]);
if(evts[y].indexOf(":")>=0){
var _618=tevt.split(":");
tevt=trim(_618[0]);
_616=trim(_618[1]);
}
if(!_616){
_616=tevt;
}
var tf=function(){
var ntf=new String(_616);
return function(evt){
if(_60d[ntf]){
_60d[ntf](dojo.event.browser.fixEvent(evt,this));
}
};
}();
dojo.event.browser.addListener(_60f,tevt,tf,false,true);
}
}
for(var y=0;y<_609.length;y++){
var _61c=_60f.getAttribute(_609[y]);
if((_61c)&&(_61c.length)){
var _616=null;
var _61d=_609[y].substr(4);
_616=trim(_61c);
var _61e=[_616];
if(_616.indexOf(";")>=0){
_61e=dojo.lang.map(_616.split(";"),trim);
}
for(var z=0;z<_61e.length;z++){
if(!_61e[z].length){
continue;
}
var tf=function(){
var ntf=new String(_61e[z]);
return function(evt){
if(_60d[ntf]){
_60d[ntf](dojo.event.browser.fixEvent(evt,this));
}
};
}();
dojo.event.browser.addListener(_60f,_61d,tf,false,true);
}
}
}
}
var _621=_60f.getAttribute(this.templateProperty);
if(_621){
_608[_621]=_60f;
}
dojo.lang.forEach(dojo.widget.waiNames,function(name){
var wai=dojo.widget.wai[name];
var val=_60f.getAttribute(wai.name);
if(val){
if(val.indexOf("-")==-1){
dojo.widget.wai.setAttr(_60f,wai.name,"role",val);
}else{
var _625=val.split("-");
dojo.widget.wai.setAttr(_60f,wai.name,_625[0],_625[1]);
}
}
},this);
var _626=_60f.getAttribute(this.onBuildProperty);
if(_626){
eval("var node = baseNode; var widget = targetObj; "+_626);
}
}
};
dojo.widget.getDojoEventsFromStr=function(str){
var re=/(dojoOn([a-z]+)(\s?))=/gi;
var evts=str?str.match(re)||[]:[];
var ret=[];
var lem={};
for(var x=0;x<evts.length;x++){
if(evts[x].length<1){
continue;
}
var cm=evts[x].replace(/\s/,"");
cm=(cm.slice(0,cm.length-1));
if(!lem[cm]){
lem[cm]=true;
ret.push(cm);
}
}
return ret;
};
dojo.declare("dojo.widget.DomWidget",dojo.widget.Widget,function(){
if((arguments.length>0)&&(typeof arguments[0]=="object")){
this.create(arguments[0]);
}
},{templateNode:null,templateString:null,templateCssString:null,preventClobber:false,domNode:null,containerNode:null,widgetsInTemplate:false,addChild:function(_62e,_62f,pos,ref,_632){
if(typeof _632=="undefined"){
_632=this.children.length;
}
this.addWidgetAsDirectChild(_62e,_62f,pos,ref,_632);
this.registerChild(_62e,_632);
return _62e;
},addWidgetAsDirectChild:function(_633,_634,pos,ref,_637){
if((!this.containerNode)&&(!_634)){
this.containerNode=this.domNode;
}
var cn=(_634)?_634:this.containerNode;
if(!pos){
pos="after";
}
if(!ref){
if(!cn){
cn=dojo.body();
}
ref=cn.lastChild;
}
if(!_637){
_637=0;
}
_633.domNode.setAttribute("dojoinsertionindex",_637);
if(!ref){
cn.appendChild(_633.domNode);
}else{
if(pos=="insertAtIndex"){
dojo.dom.insertAtIndex(_633.domNode,ref.parentNode,_637);
}else{
if((pos=="after")&&(ref===cn.lastChild)){
cn.appendChild(_633.domNode);
}else{
dojo.dom.insertAtPosition(_633.domNode,ref,pos);
}
}
}
},registerChild:function(_639,_63a){
_639.dojoInsertionIndex=_63a;
var idx=-1;
for(var i=0;i<this.children.length;i++){
if(this.children[i].dojoInsertionIndex<=_63a){
idx=i;
}
}
this.children.splice(idx+1,0,_639);
_639.parent=this;
_639.addedTo(this,idx+1);
delete dojo.widget.manager.topWidgets[_639.widgetId];
},removeChild:function(_63d){
dojo.dom.removeNode(_63d.domNode);
return dojo.widget.DomWidget.superclass.removeChild.call(this,_63d);
},getFragNodeRef:function(frag){
if(!frag){
return null;
}
if(!frag[this.getNamespacedType()]){
dojo.raise("Error: no frag for widget type "+this.getNamespacedType()+", id "+this.widgetId+" (maybe a widget has set it's type incorrectly)");
}
return frag[this.getNamespacedType()]["nodeRef"];
},postInitialize:function(args,frag,_641){
var _642=this.getFragNodeRef(frag);
if(_641&&(_641.snarfChildDomOutput||!_642)){
_641.addWidgetAsDirectChild(this,"","insertAtIndex","",args["dojoinsertionindex"],_642);
}else{
if(_642){
if(this.domNode&&(this.domNode!==_642)){
this._sourceNodeRef=dojo.dom.replaceNode(_642,this.domNode);
}
}
}
if(_641){
_641.registerChild(this,args.dojoinsertionindex);
}else{
dojo.widget.manager.topWidgets[this.widgetId]=this;
}
if(this.widgetsInTemplate){
var _643=new dojo.xml.Parse();
var _644;
var _645=this.domNode.getElementsByTagName("*");
for(var i=0;i<_645.length;i++){
if(_645[i].getAttribute("dojoAttachPoint")=="subContainerWidget"){
_644=_645[i];
}
if(_645[i].getAttribute("dojoType")){
_645[i].setAttribute("isSubWidget",true);
}
}
if(this.isContainer&&!this.containerNode){
if(_644){
var src=this.getFragNodeRef(frag);
if(src){
dojo.dom.moveChildren(src,_644);
frag["dojoDontFollow"]=true;
}
}else{
dojo.debug("No subContainerWidget node can be found in template file for widget "+this);
}
}
var _648=_643.parseElement(this.domNode,null,true);
dojo.widget.getParser().createSubComponents(_648,this);
var _649=[];
var _64a=[this];
var w;
while((w=_64a.pop())){
for(var i=0;i<w.children.length;i++){
var _64c=w.children[i];
if(_64c._processedSubWidgets||!_64c.extraArgs["issubwidget"]){
continue;
}
_649.push(_64c);
if(_64c.isContainer){
_64a.push(_64c);
}
}
}
for(var i=0;i<_649.length;i++){
var _64d=_649[i];
if(_64d._processedSubWidgets){
dojo.debug("This should not happen: widget._processedSubWidgets is already true!");
return;
}
_64d._processedSubWidgets=true;
if(_64d.extraArgs["dojoattachevent"]){
var evts=_64d.extraArgs["dojoattachevent"].split(";");
for(var j=0;j<evts.length;j++){
var _650=null;
var tevt=dojo.string.trim(evts[j]);
if(tevt.indexOf(":")>=0){
var _652=tevt.split(":");
tevt=dojo.string.trim(_652[0]);
_650=dojo.string.trim(_652[1]);
}
if(!_650){
_650=tevt;
}
if(dojo.lang.isFunction(_64d[tevt])){
dojo.event.kwConnect({srcObj:_64d,srcFunc:tevt,targetObj:this,targetFunc:_650});
}else{
alert(tevt+" is not a function in widget "+_64d);
}
}
}
if(_64d.extraArgs["dojoattachpoint"]){
this[_64d.extraArgs["dojoattachpoint"]]=_64d;
}
}
}
if(this.isContainer&&!frag["dojoDontFollow"]){
dojo.widget.getParser().createSubComponents(frag,this);
}
},buildRendering:function(args,frag){
var ts=dojo.widget._templateCache[this.widgetType];
if(args["templatecsspath"]){
args["templateCssPath"]=args["templatecsspath"];
}
var _656=args["templateCssPath"]||this.templateCssPath;
if(_656&&!dojo.widget._cssFiles[_656.toString()]){
if((!this.templateCssString)&&(_656)){
this.templateCssString=dojo.hostenv.getText(_656);
this.templateCssPath=null;
}
dojo.widget._cssFiles[_656.toString()]=true;
}
if((this["templateCssString"])&&(!dojo.widget._cssStrings[this.templateCssString])){
dojo.html.insertCssText(this.templateCssString,null,_656);
dojo.widget._cssStrings[this.templateCssString]=true;
}
if((!this.preventClobber)&&((this.templatePath)||(this.templateNode)||((this["templateString"])&&(this.templateString.length))||((typeof ts!="undefined")&&((ts["string"])||(ts["node"]))))){
this.buildFromTemplate(args,frag);
}else{
this.domNode=this.getFragNodeRef(frag);
}
this.fillInTemplate(args,frag);
},buildFromTemplate:function(args,frag){
var _659=false;
if(args["templatepath"]){
args["templatePath"]=args["templatepath"];
}
dojo.widget.fillFromTemplateCache(this,args["templatePath"],null,_659);
var ts=dojo.widget._templateCache[this.templatePath?this.templatePath.toString():this.widgetType];
if((ts)&&(!_659)){
if(!this.templateString.length){
this.templateString=ts["string"];
}
if(!this.templateNode){
this.templateNode=ts["node"];
}
}
var _65b=false;
var node=null;
var tstr=this.templateString;
if((!this.templateNode)&&(this.templateString)){
_65b=this.templateString.match(/\$\{([^\}]+)\}/g);
if(_65b){
var hash=this.strings||{};
for(var key in dojo.widget.defaultStrings){
if(dojo.lang.isUndefined(hash[key])){
hash[key]=dojo.widget.defaultStrings[key];
}
}
for(var i=0;i<_65b.length;i++){
var key=_65b[i];
key=key.substring(2,key.length-1);
var kval=(key.substring(0,5)=="this.")?dojo.getObject(key.substring(5),false,this):hash[key];
var _662;
if((kval)||(dojo.lang.isString(kval))){
_662=new String((dojo.lang.isFunction(kval))?kval.call(this,key,this.templateString):kval);
while(_662.indexOf("\"")>-1){
_662=_662.replace("\"","&quot;");
}
tstr=tstr.replace(_65b[i],_662);
}
}
}else{
this.templateNode=this.createNodesFromText(this.templateString,true)[0];
if(!_659){
ts.node=this.templateNode;
}
}
}
if((!this.templateNode)&&(!_65b)){
dojo.debug("DomWidget.buildFromTemplate: could not create template");
return false;
}else{
if(!_65b){
node=this.templateNode.cloneNode(true);
if(!node){
return false;
}
}else{
node=this.createNodesFromText(tstr,true)[0];
}
}
this.domNode=node;
this.attachTemplateNodes();
if(this.isContainer&&this.containerNode){
var src=this.getFragNodeRef(frag);
if(src){
dojo.dom.moveChildren(src,this.containerNode);
}
}
},attachTemplateNodes:function(_664,_665){
if(!_664){
_664=this.domNode;
}
if(!_665){
_665=this;
}
return dojo.widget.attachTemplateNodes(_664,_665,dojo.widget.getDojoEventsFromStr(this.templateString));
},fillInTemplate:function(){
},destroyRendering:function(){
try{
dojo.dom.destroyNode(this.domNode);
delete this.domNode;
}
catch(e){
}
if(this._sourceNodeRef){
try{
dojo.dom.destroyNode(this._sourceNodeRef);
}
catch(e){
}
}
},createNodesFromText:function(){
dojo.unimplemented("dojo.widget.DomWidget.createNodesFromText");
}});
dojo.provide("dojo.html.display");
dojo.html._toggle=function(node,_667,_668){
node=dojo.byId(node);
_668(node,!_667(node));
return _667(node);
};
dojo.html.show=function(node){
node=dojo.byId(node);
if(dojo.html.getStyleProperty(node,"display")=="none"){
var _66a=dojo.html.getAttribute("djDisplayCache");
dojo.html.setStyle(node,"display",(_66a||""));
node.removeAttribute("djDisplayCache");
}
};
dojo.html.hide=function(node){
node=dojo.byId(node);
var _66c=dojo.html.getAttribute("djDisplayCache");
if(_66c==null){
var d=dojo.html.getStyleProperty(node,"display");
if(d!="none"){
node.setAttribute("djDisplayCache",d);
}
}
dojo.html.setStyle(node,"display","none");
};
dojo.html.setShowing=function(node,_66f){
dojo.html[(_66f?"show":"hide")](node);
};
dojo.html.isShowing=function(node){
return (dojo.html.getStyleProperty(node,"display")!="none");
};
dojo.html.toggleShowing=function(node){
return dojo.html._toggle(node,dojo.html.isShowing,dojo.html.setShowing);
};
dojo.html.displayMap={tr:"",td:"",th:"",img:"inline",span:"inline",input:"inline",button:"inline"};
dojo.html.suggestDisplayByTagName=function(node){
node=dojo.byId(node);
if(node&&node.tagName){
var tag=node.tagName.toLowerCase();
return (tag in dojo.html.displayMap?dojo.html.displayMap[tag]:"block");
}
};
dojo.html.setDisplay=function(node,_675){
dojo.html.setStyle(node,"display",((_675 instanceof String||typeof _675=="string")?_675:(_675?dojo.html.suggestDisplayByTagName(node):"none")));
};
dojo.html.isDisplayed=function(node){
return (dojo.html.getComputedStyle(node,"display")!="none");
};
dojo.html.toggleDisplay=function(node){
return dojo.html._toggle(node,dojo.html.isDisplayed,dojo.html.setDisplay);
};
dojo.html.setVisibility=function(node,_679){
dojo.html.setStyle(node,"visibility",((_679 instanceof String||typeof _679=="string")?_679:(_679?"visible":"hidden")));
};
dojo.html.isVisible=function(node){
return (dojo.html.getComputedStyle(node,"visibility")!="hidden");
};
dojo.html.toggleVisibility=function(node){
return dojo.html._toggle(node,dojo.html.isVisible,dojo.html.setVisibility);
};
dojo.html.setOpacity=function(node,_67d,_67e){
node=dojo.byId(node);
var h=dojo.render.html;
if(!_67e){
if(_67d>=1){
if(h.ie){
dojo.html.clearOpacity(node);
return;
}else{
_67d=0.999999;
}
}else{
if(_67d<0){
_67d=0;
}
}
}
if(h.ie){
if(node.nodeName.toLowerCase()=="tr"){
var tds=node.getElementsByTagName("td");
for(var x=0;x<tds.length;x++){
tds[x].style.filter="Alpha(Opacity="+_67d*100+")";
}
}
node.style.filter="Alpha(Opacity="+_67d*100+")";
}else{
if(h.moz){
node.style.opacity=_67d;
node.style.MozOpacity=_67d;
}else{
if(h.safari){
node.style.opacity=_67d;
node.style.KhtmlOpacity=_67d;
}else{
node.style.opacity=_67d;
}
}
}
};
dojo.html.clearOpacity=function(node){
node=dojo.byId(node);
var ns=node.style;
var h=dojo.render.html;
if(h.ie){
try{
if(node.filters&&node.filters.alpha){
ns.filter="";
}
}
catch(e){
}
}else{
if(h.moz){
ns.opacity=1;
ns.MozOpacity=1;
}else{
if(h.safari){
ns.opacity=1;
ns.KhtmlOpacity=1;
}else{
ns.opacity=1;
}
}
}
};
dojo.html.getOpacity=function(node){
node=dojo.byId(node);
var h=dojo.render.html;
if(h.ie){
var opac=(node.filters&&node.filters.alpha&&typeof node.filters.alpha.opacity=="number"?node.filters.alpha.opacity:100)/100;
}else{
var opac=node.style.opacity||node.style.MozOpacity||node.style.KhtmlOpacity||1;
}
return opac>=0.999999?1:Number(opac);
};
dojo.provide("dojo.html.layout");
dojo.html.sumAncestorProperties=function(node,prop){
node=dojo.byId(node);
if(!node){
return 0;
}
var _68a=0;
while(node){
if(dojo.html.getComputedStyle(node,"position")=="fixed"){
return 0;
}
var val=node[prop];
if(val){
_68a+=val-0;
if(node==dojo.body()){
break;
}
}
node=node.parentNode;
}
return _68a;
};
dojo.html.setStyleAttributes=function(node,_68d){
node=dojo.byId(node);
var _68e=_68d.replace(/(;)?\s*$/,"").split(";");
for(var i=0;i<_68e.length;i++){
var _690=_68e[i].split(":");
var name=_690[0].replace(/\s*$/,"").replace(/^\s*/,"").toLowerCase();
var _692=_690[1].replace(/\s*$/,"").replace(/^\s*/,"");
switch(name){
case "opacity":
dojo.html.setOpacity(node,_692);
break;
case "content-height":
dojo.html.setContentBox(node,{height:_692});
break;
case "content-width":
dojo.html.setContentBox(node,{width:_692});
break;
case "outer-height":
dojo.html.setMarginBox(node,{height:_692});
break;
case "outer-width":
dojo.html.setMarginBox(node,{width:_692});
break;
default:
node.style[dojo.html.toCamelCase(name)]=_692;
}
}
};
dojo.html.boxSizing={MARGIN_BOX:"margin-box",BORDER_BOX:"border-box",PADDING_BOX:"padding-box",CONTENT_BOX:"content-box"};
dojo.html.getAbsolutePosition=dojo.html.abs=function(node,_694,_695){
node=dojo.byId(node);
var _696=dojo.doc();
var ret={x:0,y:0};
var bs=dojo.html.boxSizing;
if(!_695){
_695=bs.CONTENT_BOX;
}
var _699=2;
var _69a;
switch(_695){
case bs.MARGIN_BOX:
_69a=3;
break;
case bs.BORDER_BOX:
_69a=2;
break;
case bs.PADDING_BOX:
default:
_69a=1;
break;
case bs.CONTENT_BOX:
_69a=0;
break;
}
var h=dojo.render.html;
var db=_696["body"]||_696["documentElement"];
if(h.ie){
with(node.getBoundingClientRect()){
ret.x=left-2;
ret.y=top-2;
}
}else{
if(_696["getBoxObjectFor"]){
_699=1;
try{
var bo=_696.getBoxObjectFor(node);
ret.x=bo.x-dojo.html.sumAncestorProperties(node,"scrollLeft");
ret.y=bo.y-dojo.html.sumAncestorProperties(node,"scrollTop");
}
catch(e){
}
}else{
if(node["offsetParent"]){
var _69e;
if((h.safari)&&(node.style.getPropertyValue("position")=="absolute")&&(node.parentNode==db)){
_69e=db;
}else{
_69e=db.parentNode;
}
if(node.parentNode!=db){
var nd=node;
if(dojo.render.html.opera){
nd=db;
}
ret.x-=dojo.html.sumAncestorProperties(nd,"scrollLeft");
ret.y-=dojo.html.sumAncestorProperties(nd,"scrollTop");
}
var _6a0=node;
do{
var n=_6a0["offsetLeft"];
if(!h.opera||n>0){
ret.x+=isNaN(n)?0:n;
}
var m=_6a0["offsetTop"];
ret.y+=isNaN(m)?0:m;
_6a0=_6a0.offsetParent;
}while((_6a0!=_69e)&&(_6a0!=null));
}else{
if(node["x"]&&node["y"]){
ret.x+=isNaN(node.x)?0:node.x;
ret.y+=isNaN(node.y)?0:node.y;
}
}
}
}
if(_694){
var _6a3=dojo.html.getScroll();
ret.y+=_6a3.top;
ret.x+=_6a3.left;
}
var _6a4=[dojo.html.getPaddingExtent,dojo.html.getBorderExtent,dojo.html.getMarginExtent];
if(_699>_69a){
for(var i=_69a;i<_699;++i){
ret.y+=_6a4[i](node,"top");
ret.x+=_6a4[i](node,"left");
}
}else{
if(_699<_69a){
for(var i=_69a;i>_699;--i){
ret.y-=_6a4[i-1](node,"top");
ret.x-=_6a4[i-1](node,"left");
}
}
}
ret.top=ret.y;
ret.left=ret.x;
return ret;
};
dojo.html.isPositionAbsolute=function(node){
return (dojo.html.getComputedStyle(node,"position")=="absolute");
};
dojo.html._getComponentPixelValues=function(node,_6a8,_6a9,_6aa){
var _6ab=["top","bottom","left","right"];
var obj={};
for(var i in _6ab){
side=_6ab[i];
obj[side]=_6a9(node,_6a8+side,_6aa);
}
obj.width=obj.left+obj.right;
obj.height=obj.top+obj.bottom;
return obj;
};
dojo.html.getMargin=function(node){
return dojo.html._getComponentPixelValues(node,"margin-",dojo.html.getPixelValue,dojo.html.isPositionAbsolute(node));
};
dojo.html.getBorder=function(node){
return dojo.html._getComponentPixelValues(node,"",dojo.html.getBorderExtent);
};
dojo.html.getBorderExtent=function(node,side){
return (dojo.html.getStyle(node,"border-"+side+"-style")=="none"?0:dojo.html.getPixelValue(node,"border-"+side+"-width"));
};
dojo.html.getMarginExtent=function(node,side){
return dojo.html.getPixelValue(node,"margin-"+side,dojo.html.isPositionAbsolute(node));
};
dojo.html.getPaddingExtent=function(node,side){
return dojo.html.getPixelValue(node,"padding-"+side,true);
};
dojo.html.getPadding=function(node){
return dojo.html._getComponentPixelValues(node,"padding-",dojo.html.getPixelValue,true);
};
dojo.html.getPadBorder=function(node){
var pad=dojo.html.getPadding(node);
var _6b9=dojo.html.getBorder(node);
return {width:pad.width+_6b9.width,height:pad.height+_6b9.height};
};
dojo.html.getBoxSizing=function(node){
var h=dojo.render.html;
var bs=dojo.html.boxSizing;
if(((h.ie)||(h.opera))&&node.nodeName.toLowerCase()!="img"){
var cm=document["compatMode"];
if((cm=="BackCompat")||(cm=="QuirksMode")){
return bs.BORDER_BOX;
}else{
return bs.CONTENT_BOX;
}
}else{
if(arguments.length==0){
node=document.documentElement;
}
var _6be;
if(!h.ie){
_6be=dojo.html.getStyle(node,"-moz-box-sizing");
if(!_6be){
_6be=dojo.html.getStyle(node,"box-sizing");
}
}
return (_6be?_6be:bs.CONTENT_BOX);
}
};
dojo.html.isBorderBox=function(node){
return (dojo.html.getBoxSizing(node)==dojo.html.boxSizing.BORDER_BOX);
};
dojo.html.getBorderBox=function(node){
node=dojo.byId(node);
return {width:node.offsetWidth,height:node.offsetHeight};
};
dojo.html.getPaddingBox=function(node){
var box=dojo.html.getBorderBox(node);
var _6c3=dojo.html.getBorder(node);
return {width:box.width-_6c3.width,height:box.height-_6c3.height};
};
dojo.html.getContentBox=function(node){
node=dojo.byId(node);
var _6c5=dojo.html.getPadBorder(node);
return {width:node.offsetWidth-_6c5.width,height:node.offsetHeight-_6c5.height};
};
dojo.html.setContentBox=function(node,args){
node=dojo.byId(node);
var _6c8=0;
var _6c9=0;
var isbb=dojo.html.isBorderBox(node);
var _6cb=(isbb?dojo.html.getPadBorder(node):{width:0,height:0});
var ret={};
if(typeof args.width!="undefined"){
_6c8=args.width+_6cb.width;
ret.width=dojo.html.setPositivePixelValue(node,"width",_6c8);
}
if(typeof args.height!="undefined"){
_6c9=args.height+_6cb.height;
ret.height=dojo.html.setPositivePixelValue(node,"height",_6c9);
}
return ret;
};
dojo.html.getMarginBox=function(node){
var _6ce=dojo.html.getBorderBox(node);
var _6cf=dojo.html.getMargin(node);
return {width:_6ce.width+_6cf.width,height:_6ce.height+_6cf.height};
};
dojo.html.setMarginBox=function(node,args){
node=dojo.byId(node);
var _6d2=0;
var _6d3=0;
var isbb=dojo.html.isBorderBox(node);
var _6d5=(!isbb?dojo.html.getPadBorder(node):{width:0,height:0});
var _6d6=dojo.html.getMargin(node);
var ret={};
if(typeof args.width!="undefined"){
_6d2=args.width-_6d5.width;
_6d2-=_6d6.width;
ret.width=dojo.html.setPositivePixelValue(node,"width",_6d2);
}
if(typeof args.height!="undefined"){
_6d3=args.height-_6d5.height;
_6d3-=_6d6.height;
ret.height=dojo.html.setPositivePixelValue(node,"height",_6d3);
}
return ret;
};
dojo.html.getElementBox=function(node,type){
var bs=dojo.html.boxSizing;
switch(type){
case bs.MARGIN_BOX:
return dojo.html.getMarginBox(node);
case bs.BORDER_BOX:
return dojo.html.getBorderBox(node);
case bs.PADDING_BOX:
return dojo.html.getPaddingBox(node);
case bs.CONTENT_BOX:
default:
return dojo.html.getContentBox(node);
}
};
dojo.html.toCoordinateObject=dojo.html.toCoordinateArray=function(_6db,_6dc,_6dd){
if(!_6db.nodeType&&!(_6db instanceof String||typeof _6db=="string")&&("width" in _6db||"height" in _6db||"left" in _6db||"x" in _6db||"top" in _6db||"y" in _6db)){
var ret={left:_6db.left||_6db.x||0,top:_6db.top||_6db.y||0,width:_6db.width||0,height:_6db.height||0};
}else{
var node=dojo.byId(_6db);
var pos=dojo.html.abs(node,_6dc,_6dd);
var _6e1=dojo.html.getMarginBox(node);
var ret={left:pos.left,top:pos.top,width:_6e1.width,height:_6e1.height};
}
ret.x=ret.left;
ret.y=ret.top;
return ret;
};
dojo.html.setMarginBoxWidth=dojo.html.setOuterWidth=function(node,_6e3){
return dojo.html._callDeprecated("setMarginBoxWidth","setMarginBox",arguments,"width");
};
dojo.html.setMarginBoxHeight=dojo.html.setOuterHeight=function(){
return dojo.html._callDeprecated("setMarginBoxHeight","setMarginBox",arguments,"height");
};
dojo.html.getMarginBoxWidth=dojo.html.getOuterWidth=function(){
return dojo.html._callDeprecated("getMarginBoxWidth","getMarginBox",arguments,null,"width");
};
dojo.html.getMarginBoxHeight=dojo.html.getOuterHeight=function(){
return dojo.html._callDeprecated("getMarginBoxHeight","getMarginBox",arguments,null,"height");
};
dojo.html.getTotalOffset=function(node,type,_6e6){
return dojo.html._callDeprecated("getTotalOffset","getAbsolutePosition",arguments,null,type);
};
dojo.html.getAbsoluteX=function(node,_6e8){
return dojo.html._callDeprecated("getAbsoluteX","getAbsolutePosition",arguments,null,"x");
};
dojo.html.getAbsoluteY=function(node,_6ea){
return dojo.html._callDeprecated("getAbsoluteY","getAbsolutePosition",arguments,null,"y");
};
dojo.html.totalOffsetLeft=function(node,_6ec){
return dojo.html._callDeprecated("totalOffsetLeft","getAbsolutePosition",arguments,null,"left");
};
dojo.html.totalOffsetTop=function(node,_6ee){
return dojo.html._callDeprecated("totalOffsetTop","getAbsolutePosition",arguments,null,"top");
};
dojo.html.getMarginWidth=function(node){
return dojo.html._callDeprecated("getMarginWidth","getMargin",arguments,null,"width");
};
dojo.html.getMarginHeight=function(node){
return dojo.html._callDeprecated("getMarginHeight","getMargin",arguments,null,"height");
};
dojo.html.getBorderWidth=function(node){
return dojo.html._callDeprecated("getBorderWidth","getBorder",arguments,null,"width");
};
dojo.html.getBorderHeight=function(node){
return dojo.html._callDeprecated("getBorderHeight","getBorder",arguments,null,"height");
};
dojo.html.getPaddingWidth=function(node){
return dojo.html._callDeprecated("getPaddingWidth","getPadding",arguments,null,"width");
};
dojo.html.getPaddingHeight=function(node){
return dojo.html._callDeprecated("getPaddingHeight","getPadding",arguments,null,"height");
};
dojo.html.getPadBorderWidth=function(node){
return dojo.html._callDeprecated("getPadBorderWidth","getPadBorder",arguments,null,"width");
};
dojo.html.getPadBorderHeight=function(node){
return dojo.html._callDeprecated("getPadBorderHeight","getPadBorder",arguments,null,"height");
};
dojo.html.getBorderBoxWidth=dojo.html.getInnerWidth=function(){
return dojo.html._callDeprecated("getBorderBoxWidth","getBorderBox",arguments,null,"width");
};
dojo.html.getBorderBoxHeight=dojo.html.getInnerHeight=function(){
return dojo.html._callDeprecated("getBorderBoxHeight","getBorderBox",arguments,null,"height");
};
dojo.html.getContentBoxWidth=dojo.html.getContentWidth=function(){
return dojo.html._callDeprecated("getContentBoxWidth","getContentBox",arguments,null,"width");
};
dojo.html.getContentBoxHeight=dojo.html.getContentHeight=function(){
return dojo.html._callDeprecated("getContentBoxHeight","getContentBox",arguments,null,"height");
};
dojo.html.setContentBoxWidth=dojo.html.setContentWidth=function(node,_6f8){
return dojo.html._callDeprecated("setContentBoxWidth","setContentBox",arguments,"width");
};
dojo.html.setContentBoxHeight=dojo.html.setContentHeight=function(node,_6fa){
return dojo.html._callDeprecated("setContentBoxHeight","setContentBox",arguments,"height");
};
dojo.provide("dojo.html.util");
dojo.html.getElementWindow=function(_6fb){
return dojo.html.getDocumentWindow(_6fb.ownerDocument);
};
dojo.html.getDocumentWindow=function(doc){
if(dojo.render.html.safari&&!doc._parentWindow){
var fix=function(win){
win.document._parentWindow=win;
for(var i=0;i<win.frames.length;i++){
fix(win.frames[i]);
}
};
fix(window.top);
}
if(dojo.render.html.ie&&window!==document.parentWindow&&!doc._parentWindow){
doc.parentWindow.execScript("document._parentWindow = window;","Javascript");
var win=doc._parentWindow;
doc._parentWindow=null;
return win;
}
return doc._parentWindow||doc.parentWindow||doc.defaultView;
};
dojo.html.getAbsolutePositionExt=function(node,_702,_703,_704){
var _705=dojo.html.getElementWindow(node);
var ret=dojo.withGlobal(_705,"getAbsolutePosition",dojo.html,arguments);
var win=dojo.html.getElementWindow(node);
if(_704!=win&&win.frameElement){
var ext=dojo.html.getAbsolutePositionExt(win.frameElement,_702,_703,_704);
ret.x+=ext.x;
ret.y+=ext.y;
}
ret.top=ret.y;
ret.left=ret.x;
return ret;
};
dojo.html.gravity=function(node,e){
node=dojo.byId(node);
var _70b=dojo.html.getCursorPosition(e);
with(dojo.html){
var _70c=getAbsolutePosition(node,true);
var bb=getBorderBox(node);
var _70e=_70c.x+(bb.width/2);
var _70f=_70c.y+(bb.height/2);
}
with(dojo.html.gravity){
return ((_70b.x<_70e?WEST:EAST)|(_70b.y<_70f?NORTH:SOUTH));
}
};
dojo.html.gravity.NORTH=1;
dojo.html.gravity.SOUTH=1<<1;
dojo.html.gravity.EAST=1<<2;
dojo.html.gravity.WEST=1<<3;
dojo.html.overElement=function(_710,e){
_710=dojo.byId(_710);
var _712=dojo.html.getCursorPosition(e);
var bb=dojo.html.getBorderBox(_710);
var _714=dojo.html.getAbsolutePosition(_710,true,dojo.html.boxSizing.BORDER_BOX);
var top=_714.y;
var _716=top+bb.height;
var left=_714.x;
var _718=left+bb.width;
return (_712.x>=left&&_712.x<=_718&&_712.y>=top&&_712.y<=_716);
};
dojo.html.renderedTextContent=function(node){
node=dojo.byId(node);
var _71a="";
if(node==null){
return _71a;
}
for(var i=0;i<node.childNodes.length;i++){
switch(node.childNodes[i].nodeType){
case 1:
case 5:
var _71c="unknown";
try{
_71c=dojo.html.getStyle(node.childNodes[i],"display");
}
catch(E){
}
switch(_71c){
case "block":
case "list-item":
case "run-in":
case "table":
case "table-row-group":
case "table-header-group":
case "table-footer-group":
case "table-row":
case "table-column-group":
case "table-column":
case "table-cell":
case "table-caption":
_71a+="\n";
_71a+=dojo.html.renderedTextContent(node.childNodes[i]);
_71a+="\n";
break;
case "none":
break;
default:
if(node.childNodes[i].tagName&&node.childNodes[i].tagName.toLowerCase()=="br"){
_71a+="\n";
}else{
_71a+=dojo.html.renderedTextContent(node.childNodes[i]);
}
break;
}
break;
case 3:
case 2:
case 4:
var text=node.childNodes[i].nodeValue;
var _71e="unknown";
try{
_71e=dojo.html.getStyle(node,"text-transform");
}
catch(E){
}
switch(_71e){
case "capitalize":
var _71f=text.split(" ");
for(var i=0;i<_71f.length;i++){
_71f[i]=_71f[i].charAt(0).toUpperCase()+_71f[i].substring(1);
}
text=_71f.join(" ");
break;
case "uppercase":
text=text.toUpperCase();
break;
case "lowercase":
text=text.toLowerCase();
break;
default:
break;
}
switch(_71e){
case "nowrap":
break;
case "pre-wrap":
break;
case "pre-line":
break;
case "pre":
break;
default:
text=text.replace(/\s+/," ");
if(/\s$/.test(_71a)){
text.replace(/^\s/,"");
}
break;
}
_71a+=text;
break;
default:
break;
}
}
return _71a;
};
dojo.html.createNodesFromText=function(txt,trim){
if(trim){
txt=txt.replace(/^\s+|\s+$/g,"");
}
var tn=dojo.doc().createElement("div");
tn.style.visibility="hidden";
dojo.body().appendChild(tn);
var _723="none";
if((/^<t[dh][\s\r\n>]/i).test(txt.replace(/^\s+/))){
txt="<table><tbody><tr>"+txt+"</tr></tbody></table>";
_723="cell";
}else{
if((/^<tr[\s\r\n>]/i).test(txt.replace(/^\s+/))){
txt="<table><tbody>"+txt+"</tbody></table>";
_723="row";
}else{
if((/^<(thead|tbody|tfoot)[\s\r\n>]/i).test(txt.replace(/^\s+/))){
txt="<table>"+txt+"</table>";
_723="section";
}
}
}
tn.innerHTML=txt;
if(tn["normalize"]){
tn.normalize();
}
var _724=null;
switch(_723){
case "cell":
_724=tn.getElementsByTagName("tr")[0];
break;
case "row":
_724=tn.getElementsByTagName("tbody")[0];
break;
case "section":
_724=tn.getElementsByTagName("table")[0];
break;
default:
_724=tn;
break;
}
var _725=[];
for(var x=0;x<_724.childNodes.length;x++){
_725.push(_724.childNodes[x].cloneNode(true));
}
tn.style.display="none";
dojo.html.destroyNode(tn);
return _725;
};
dojo.html.placeOnScreen=function(node,_728,_729,_72a,_72b,_72c,_72d){
if(_728 instanceof Array||typeof _728=="array"){
_72d=_72c;
_72c=_72b;
_72b=_72a;
_72a=_729;
_729=_728[1];
_728=_728[0];
}
if(_72c instanceof String||typeof _72c=="string"){
_72c=_72c.split(",");
}
if(!isNaN(_72a)){
_72a=[Number(_72a),Number(_72a)];
}else{
if(!(_72a instanceof Array||typeof _72a=="array")){
_72a=[0,0];
}
}
var _72e=dojo.html.getScroll().offset;
var view=dojo.html.getViewport();
node=dojo.byId(node);
var _730=node.style.display;
node.style.display="";
var bb=dojo.html.getBorderBox(node);
var w=bb.width;
var h=bb.height;
node.style.display=_730;
if(!(_72c instanceof Array||typeof _72c=="array")){
_72c=["TL"];
}
var _734,_735,_736=Infinity,_737;
for(var _738=0;_738<_72c.length;++_738){
var _739=_72c[_738];
var _73a=true;
var tryX=_728-(_739.charAt(1)=="L"?0:w)+_72a[0]*(_739.charAt(1)=="L"?1:-1);
var tryY=_729-(_739.charAt(0)=="T"?0:h)+_72a[1]*(_739.charAt(0)=="T"?1:-1);
if(_72b){
tryX-=_72e.x;
tryY-=_72e.y;
}
if(tryX<0){
tryX=0;
_73a=false;
}
if(tryY<0){
tryY=0;
_73a=false;
}
var x=tryX+w;
if(x>view.width){
x=view.width-w;
_73a=false;
}else{
x=tryX;
}
x=Math.max(_72a[0],x)+_72e.x;
var y=tryY+h;
if(y>view.height){
y=view.height-h;
_73a=false;
}else{
y=tryY;
}
y=Math.max(_72a[1],y)+_72e.y;
if(_73a){
_734=x;
_735=y;
_736=0;
_737=_739;
break;
}else{
var dist=Math.pow(x-tryX-_72e.x,2)+Math.pow(y-tryY-_72e.y,2);
if(_736>dist){
_736=dist;
_734=x;
_735=y;
_737=_739;
}
}
}
if(!_72d){
node.style.left=_734+"px";
node.style.top=_735+"px";
}
return {left:_734,top:_735,x:_734,y:_735,dist:_736,corner:_737};
};
dojo.html.placeOnScreenAroundElement=function(node,_741,_742,_743,_744,_745){
var best,_747=Infinity;
_741=dojo.byId(_741);
var _748=_741.style.display;
_741.style.display="";
var mb=dojo.html.getElementBox(_741,_743);
var _74a=mb.width;
var _74b=mb.height;
var _74c=dojo.html.getAbsolutePosition(_741,true,_743);
_741.style.display=_748;
for(var _74d in _744){
var pos,_74f,_750;
var _751=_744[_74d];
_74f=_74c.x+(_74d.charAt(1)=="L"?0:_74a);
_750=_74c.y+(_74d.charAt(0)=="T"?0:_74b);
pos=dojo.html.placeOnScreen(node,_74f,_750,_742,true,_751,true);
if(pos.dist==0){
best=pos;
break;
}else{
if(_747>pos.dist){
_747=pos.dist;
best=pos;
}
}
}
if(!_745){
node.style.left=best.left+"px";
node.style.top=best.top+"px";
}
return best;
};
dojo.html.scrollIntoView=function(node){
if(!node){
return;
}
if(dojo.render.html.ie){
if(dojo.html.getBorderBox(node.parentNode).height<=node.parentNode.scrollHeight){
node.scrollIntoView(false);
}
}else{
if(dojo.render.html.mozilla){
node.scrollIntoView(false);
}else{
var _753=node.parentNode;
var _754=_753.scrollTop+dojo.html.getBorderBox(_753).height;
var _755=node.offsetTop+dojo.html.getMarginBox(node).height;
if(_754<_755){
_753.scrollTop+=(_755-_754);
}else{
if(_753.scrollTop>node.offsetTop){
_753.scrollTop-=(_753.scrollTop-node.offsetTop);
}
}
}
}
};
dojo.html.isLeftToRight=function(node){
for(;node;node=node.parentNode){
if(node.dir){
return node.dir=="ltr";
}
}
return true;
};
dojo.provide("dojo.gfx.color");
dojo.gfx.color.Color=function(r,g,b,a){
if(dojo.lang.isArray(r)){
this.r=r[0];
this.g=r[1];
this.b=r[2];
this.a=r[3]||1;
}else{
if(dojo.lang.isString(r)){
var rgb=dojo.gfx.color.extractRGB(r);
this.r=rgb[0];
this.g=rgb[1];
this.b=rgb[2];
this.a=g||1;
}else{
if(r instanceof dojo.gfx.color.Color){
this.r=r.r;
this.b=r.b;
this.g=r.g;
this.a=r.a;
}else{
this.r=r;
this.g=g;
this.b=b;
this.a=a;
}
}
}
};
dojo.gfx.color.Color.fromArray=function(arr){
return new dojo.gfx.color.Color(arr[0],arr[1],arr[2],arr[3]);
};
dojo.extend(dojo.gfx.color.Color,{toRgb:function(_75d){
if(_75d){
return this.toRgba();
}else{
return [this.r,this.g,this.b];
}
},toRgba:function(){
return [this.r,this.g,this.b,this.a];
},toHex:function(){
return dojo.gfx.color.rgb2hex(this.toRgb());
},toCss:function(){
return "rgb("+this.toRgb().join()+")";
},toString:function(){
return this.toHex();
},blend:function(_75e,_75f){
var rgb=null;
if(dojo.lang.isArray(_75e)){
rgb=_75e;
}else{
if(_75e instanceof dojo.gfx.color.Color){
rgb=_75e.toRgb();
}else{
rgb=new dojo.gfx.color.Color(_75e).toRgb();
}
}
return dojo.gfx.color.blend(this.toRgb(),rgb,_75f);
}});
dojo.gfx.color.named={white:[255,255,255],black:[0,0,0],red:[255,0,0],green:[0,255,0],lime:[0,255,0],blue:[0,0,255],navy:[0,0,128],gray:[128,128,128],silver:[192,192,192]};
dojo.gfx.color.blend=function(a,b,_763){
if(typeof a=="string"){
return dojo.gfx.color.blendHex(a,b,_763);
}
if(!_763){
_763=0;
}
_763=Math.min(Math.max(-1,_763),1);
_763=((_763+1)/2);
var c=[];
for(var x=0;x<3;x++){
c[x]=parseInt(b[x]+((a[x]-b[x])*_763));
}
return c;
};
dojo.gfx.color.blendHex=function(a,b,_768){
return dojo.gfx.color.rgb2hex(dojo.gfx.color.blend(dojo.gfx.color.hex2rgb(a),dojo.gfx.color.hex2rgb(b),_768));
};
dojo.gfx.color.extractRGB=function(_769){
_769=_769.toLowerCase();
if(_769.indexOf("rgb")==0){
var _76a=_769.match(/rgba*\((\d+), *(\d+), *(\d+)/i);
var ret=_76a.splice(1,3);
return ret;
}else{
var _76c=dojo.gfx.color.hex2rgb(_769);
if(_76c){
return _76c;
}else{
return dojo.gfx.color.named[_769]||[255,255,255];
}
}
};
dojo.gfx.color.hex2rgb=function(hex){
var _76e="0123456789ABCDEF";
var rgb=new Array(3);
if(hex.indexOf("#")==0){
hex=hex.substring(1);
}
hex=hex.toUpperCase();
if(hex.replace(new RegExp("["+_76e+"]","g"),"")!=""){
return null;
}
if(hex.length==3){
rgb[0]=hex.charAt(0)+hex.charAt(0);
rgb[1]=hex.charAt(1)+hex.charAt(1);
rgb[2]=hex.charAt(2)+hex.charAt(2);
}else{
rgb[0]=hex.substring(0,2);
rgb[1]=hex.substring(2,4);
rgb[2]=hex.substring(4);
}
for(var i=0;i<rgb.length;i++){
rgb[i]=_76e.indexOf(rgb[i].charAt(0))*16+_76e.indexOf(rgb[i].charAt(1));
}
return rgb;
};
dojo.gfx.color.rgb2hex=function(r,g,b){
if(dojo.lang.isArray(r)){
g=r[1]||0;
b=r[2]||0;
r=r[0]||0;
}
var ret=dojo.lang.map([r,g,b],function(x){
x=new Number(x);
var s=x.toString(16);
while(s.length<2){
s="0"+s;
}
return s;
});
ret.unshift("#");
return ret.join("");
};
dojo.provide("dojo.lfx.Animation");
dojo.lfx.Line=function(_777,end){
this.start=_777;
this.end=end;
if(dojo.lang.isArray(_777)){
var diff=[];
dojo.lang.forEach(this.start,function(s,i){
diff[i]=this.end[i]-s;
},this);
this.getValue=function(n){
var res=[];
dojo.lang.forEach(this.start,function(s,i){
res[i]=(diff[i]*n)+s;
},this);
return res;
};
}else{
var diff=end-_777;
this.getValue=function(n){
return (diff*n)+this.start;
};
}
};
if((dojo.render.html.khtml)&&(!dojo.render.html.safari)){
dojo.lfx.easeDefault=function(n){
return (parseFloat("0.5")+((Math.sin((n+parseFloat("1.5"))*Math.PI))/2));
};
}else{
dojo.lfx.easeDefault=function(n){
return (0.5+((Math.sin((n+1.5)*Math.PI))/2));
};
}
dojo.lfx.easeIn=function(n){
return Math.pow(n,3);
};
dojo.lfx.easeOut=function(n){
return (1-Math.pow(1-n,3));
};
dojo.lfx.easeInOut=function(n){
return ((3*Math.pow(n,2))-(2*Math.pow(n,3)));
};
dojo.lfx.IAnimation=function(){
};
dojo.lang.extend(dojo.lfx.IAnimation,{curve:null,duration:1000,easing:null,repeatCount:0,rate:10,handler:null,beforeBegin:null,onBegin:null,onAnimate:null,onEnd:null,onPlay:null,onPause:null,onStop:null,play:null,pause:null,stop:null,connect:function(evt,_787,_788){
if(!_788){
_788=_787;
_787=this;
}
_788=dojo.lang.hitch(_787,_788);
var _789=this[evt]||function(){
};
this[evt]=function(){
var ret=_789.apply(this,arguments);
_788.apply(this,arguments);
return ret;
};
return this;
},fire:function(evt,args){
if(this[evt]){
this[evt].apply(this,(args||[]));
}
return this;
},repeat:function(_78d){
this.repeatCount=_78d;
return this;
},_active:false,_paused:false});
dojo.lfx.Animation=function(_78e,_78f,_790,_791,_792,rate){
dojo.lfx.IAnimation.call(this);
if(dojo.lang.isNumber(_78e)||(!_78e&&_78f.getValue)){
rate=_792;
_792=_791;
_791=_790;
_790=_78f;
_78f=_78e;
_78e=null;
}else{
if(_78e.getValue||dojo.lang.isArray(_78e)){
rate=_791;
_792=_790;
_791=_78f;
_790=_78e;
_78f=null;
_78e=null;
}
}
if(dojo.lang.isArray(_790)){
this.curve=new dojo.lfx.Line(_790[0],_790[1]);
}else{
this.curve=_790;
}
if(_78f!=null&&_78f>0){
this.duration=_78f;
}
if(_792){
this.repeatCount=_792;
}
if(rate){
this.rate=rate;
}
if(_78e){
dojo.lang.forEach(["handler","beforeBegin","onBegin","onEnd","onPlay","onStop","onAnimate"],function(item){
if(_78e[item]){
this.connect(item,_78e[item]);
}
},this);
}
if(_791&&dojo.lang.isFunction(_791)){
this.easing=_791;
}
};
dojo.inherits(dojo.lfx.Animation,dojo.lfx.IAnimation);
dojo.lang.extend(dojo.lfx.Animation,{_startTime:null,_endTime:null,_timer:null,_percent:0,_startRepeatCount:0,play:function(_795,_796){
if(_796){
clearTimeout(this._timer);
this._active=false;
this._paused=false;
this._percent=0;
}else{
if(this._active&&!this._paused){
return this;
}
}
this.fire("handler",["beforeBegin"]);
this.fire("beforeBegin");
if(_795>0){
setTimeout(dojo.lang.hitch(this,function(){
this.play(null,_796);
}),_795);
return this;
}
this._startTime=new Date().valueOf();
if(this._paused){
this._startTime-=(this.duration*this._percent/100);
}
this._endTime=this._startTime+this.duration;
this._active=true;
this._paused=false;
var step=this._percent/100;
var _798=this.curve.getValue(step);
if(this._percent==0){
if(!this._startRepeatCount){
this._startRepeatCount=this.repeatCount;
}
this.fire("handler",["begin",_798]);
this.fire("onBegin",[_798]);
}
this.fire("handler",["play",_798]);
this.fire("onPlay",[_798]);
this._cycle();
return this;
},pause:function(){
clearTimeout(this._timer);
if(!this._active){
return this;
}
this._paused=true;
var _799=this.curve.getValue(this._percent/100);
this.fire("handler",["pause",_799]);
this.fire("onPause",[_799]);
return this;
},gotoPercent:function(pct,_79b){
clearTimeout(this._timer);
this._active=true;
this._paused=true;
this._percent=pct;
if(_79b){
this.play();
}
return this;
},stop:function(_79c){
clearTimeout(this._timer);
var step=this._percent/100;
if(_79c){
step=1;
}
var _79e=this.curve.getValue(step);
this.fire("handler",["stop",_79e]);
this.fire("onStop",[_79e]);
this._active=false;
this._paused=false;
return this;
},status:function(){
if(this._active){
return this._paused?"paused":"playing";
}else{
return "stopped";
}
return this;
},_cycle:function(){
clearTimeout(this._timer);
if(this._active){
var curr=new Date().valueOf();
var step=(curr-this._startTime)/(this._endTime-this._startTime);
if(step>=1){
step=1;
this._percent=100;
}else{
this._percent=step*100;
}
if((this.easing)&&(dojo.lang.isFunction(this.easing))){
step=this.easing(step);
}
var _7a1=this.curve.getValue(step);
this.fire("handler",["animate",_7a1]);
this.fire("onAnimate",[_7a1]);
if(step<1){
this._timer=setTimeout(dojo.lang.hitch(this,"_cycle"),this.rate);
}else{
this._active=false;
this.fire("handler",["end"]);
this.fire("onEnd");
if(this.repeatCount>0){
this.repeatCount--;
this.play(null,true);
}else{
if(this.repeatCount==-1){
this.play(null,true);
}else{
if(this._startRepeatCount){
this.repeatCount=this._startRepeatCount;
this._startRepeatCount=0;
}
}
}
}
}
return this;
}});
dojo.lfx.Combine=function(_7a2){
dojo.lfx.IAnimation.call(this);
this._anims=[];
this._animsEnded=0;
var _7a3=arguments;
if(_7a3.length==1&&(dojo.lang.isArray(_7a3[0])||dojo.lang.isArrayLike(_7a3[0]))){
_7a3=_7a3[0];
}
dojo.lang.forEach(_7a3,function(anim){
this._anims.push(anim);
anim.connect("onEnd",dojo.lang.hitch(this,"_onAnimsEnded"));
},this);
};
dojo.inherits(dojo.lfx.Combine,dojo.lfx.IAnimation);
dojo.lang.extend(dojo.lfx.Combine,{_animsEnded:0,play:function(_7a5,_7a6){
if(!this._anims.length){
return this;
}
this.fire("beforeBegin");
if(_7a5>0){
setTimeout(dojo.lang.hitch(this,function(){
this.play(null,_7a6);
}),_7a5);
return this;
}
if(_7a6||this._anims[0].percent==0){
this.fire("onBegin");
}
this.fire("onPlay");
this._animsCall("play",null,_7a6);
return this;
},pause:function(){
this.fire("onPause");
this._animsCall("pause");
return this;
},stop:function(_7a7){
this.fire("onStop");
this._animsCall("stop",_7a7);
return this;
},_onAnimsEnded:function(){
this._animsEnded++;
if(this._animsEnded>=this._anims.length){
this.fire("onEnd");
}
return this;
},_animsCall:function(_7a8){
var args=[];
if(arguments.length>1){
for(var i=1;i<arguments.length;i++){
args.push(arguments[i]);
}
}
var _7ab=this;
dojo.lang.forEach(this._anims,function(anim){
anim[_7a8](args);
},_7ab);
return this;
}});
dojo.lfx.Chain=function(_7ad){
dojo.lfx.IAnimation.call(this);
this._anims=[];
this._currAnim=-1;
var _7ae=arguments;
if(_7ae.length==1&&(dojo.lang.isArray(_7ae[0])||dojo.lang.isArrayLike(_7ae[0]))){
_7ae=_7ae[0];
}
var _7af=this;
dojo.lang.forEach(_7ae,function(anim,i,_7b2){
this._anims.push(anim);
if(i<_7b2.length-1){
anim.connect("onEnd",dojo.lang.hitch(this,"_playNext"));
}else{
anim.connect("onEnd",dojo.lang.hitch(this,function(){
this.fire("onEnd");
}));
}
},this);
};
dojo.inherits(dojo.lfx.Chain,dojo.lfx.IAnimation);
dojo.lang.extend(dojo.lfx.Chain,{_currAnim:-1,play:function(_7b3,_7b4){
if(!this._anims.length){
return this;
}
if(_7b4||!this._anims[this._currAnim]){
this._currAnim=0;
}
var _7b5=this._anims[this._currAnim];
this.fire("beforeBegin");
if(_7b3>0){
setTimeout(dojo.lang.hitch(this,function(){
this.play(null,_7b4);
}),_7b3);
return this;
}
if(_7b5){
if(this._currAnim==0){
this.fire("handler",["begin",this._currAnim]);
this.fire("onBegin",[this._currAnim]);
}
this.fire("onPlay",[this._currAnim]);
_7b5.play(null,_7b4);
}
return this;
},pause:function(){
if(this._anims[this._currAnim]){
this._anims[this._currAnim].pause();
this.fire("onPause",[this._currAnim]);
}
return this;
},playPause:function(){
if(this._anims.length==0){
return this;
}
if(this._currAnim==-1){
this._currAnim=0;
}
var _7b6=this._anims[this._currAnim];
if(_7b6){
if(!_7b6._active||_7b6._paused){
this.play();
}else{
this.pause();
}
}
return this;
},stop:function(){
var _7b7=this._anims[this._currAnim];
if(_7b7){
_7b7.stop();
this.fire("onStop",[this._currAnim]);
}
return _7b7;
},_playNext:function(){
if(this._currAnim==-1||this._anims.length==0){
return this;
}
this._currAnim++;
if(this._anims[this._currAnim]){
this._anims[this._currAnim].play(null,true);
}
return this;
}});
dojo.lfx.combine=function(_7b8){
var _7b9=arguments;
if(dojo.lang.isArray(arguments[0])){
_7b9=arguments[0];
}
if(_7b9.length==1){
return _7b9[0];
}
return new dojo.lfx.Combine(_7b9);
};
dojo.lfx.chain=function(_7ba){
var _7bb=arguments;
if(dojo.lang.isArray(arguments[0])){
_7bb=arguments[0];
}
if(_7bb.length==1){
return _7bb[0];
}
return new dojo.lfx.Chain(_7bb);
};
dojo.provide("dojo.html.color");
dojo.html.getBackgroundColor=function(node){
node=dojo.byId(node);
var _7bd;
do{
_7bd=dojo.html.getStyle(node,"background-color");
if(_7bd.toLowerCase()=="rgba(0, 0, 0, 0)"){
_7bd="transparent";
}
if(node==document.getElementsByTagName("body")[0]){
node=null;
break;
}
node=node.parentNode;
}while(node&&dojo.lang.inArray(["transparent",""],_7bd));
if(_7bd=="transparent"){
_7bd=[255,255,255,0];
}else{
_7bd=dojo.gfx.color.extractRGB(_7bd);
}
return _7bd;
};
dojo.provide("dojo.lfx.html");
dojo.lfx.html._byId=function(_7be){
if(!_7be){
return [];
}
if(dojo.lang.isArrayLike(_7be)){
if(!_7be.alreadyChecked){
var n=[];
dojo.lang.forEach(_7be,function(node){
n.push(dojo.byId(node));
});
n.alreadyChecked=true;
return n;
}else{
return _7be;
}
}else{
var n=[];
n.push(dojo.byId(_7be));
n.alreadyChecked=true;
return n;
}
};
dojo.lfx.html.propertyAnimation=function(_7c1,_7c2,_7c3,_7c4,_7c5){
_7c1=dojo.lfx.html._byId(_7c1);
var _7c6={"propertyMap":_7c2,"nodes":_7c1,"duration":_7c3,"easing":_7c4||dojo.lfx.easeDefault};
var _7c7=function(args){
if(args.nodes.length==1){
var pm=args.propertyMap;
if(!dojo.lang.isArray(args.propertyMap)){
var parr=[];
for(var _7cb in pm){
pm[_7cb].property=_7cb;
parr.push(pm[_7cb]);
}
pm=args.propertyMap=parr;
}
dojo.lang.forEach(pm,function(prop){
if(dj_undef("start",prop)){
if(prop.property!="opacity"){
prop.start=parseInt(dojo.html.getComputedStyle(args.nodes[0],prop.property));
}else{
prop.start=dojo.html.getOpacity(args.nodes[0]);
}
}
});
}
};
var _7cd=function(_7ce){
var _7cf=[];
dojo.lang.forEach(_7ce,function(c){
_7cf.push(Math.round(c));
});
return _7cf;
};
var _7d1=function(n,_7d3){
n=dojo.byId(n);
if(!n||!n.style){
return;
}
for(var s in _7d3){
try{
if(s=="opacity"){
dojo.html.setOpacity(n,_7d3[s]);
}else{
n.style[s]=_7d3[s];
}
}
catch(e){
dojo.debug(e);
}
}
};
var _7d5=function(_7d6){
this._properties=_7d6;
this.diffs=new Array(_7d6.length);
dojo.lang.forEach(_7d6,function(prop,i){
if(dojo.lang.isFunction(prop.start)){
prop.start=prop.start(prop,i);
}
if(dojo.lang.isFunction(prop.end)){
prop.end=prop.end(prop,i);
}
if(dojo.lang.isArray(prop.start)){
this.diffs[i]=null;
}else{
if(prop.start instanceof dojo.gfx.color.Color){
prop.startRgb=prop.start.toRgb();
prop.endRgb=prop.end.toRgb();
}else{
this.diffs[i]=prop.end-prop.start;
}
}
},this);
this.getValue=function(n){
var ret={};
dojo.lang.forEach(this._properties,function(prop,i){
var _7dd=null;
if(dojo.lang.isArray(prop.start)){
}else{
if(prop.start instanceof dojo.gfx.color.Color){
_7dd=(prop.units||"rgb")+"(";
for(var j=0;j<prop.startRgb.length;j++){
_7dd+=Math.round(((prop.endRgb[j]-prop.startRgb[j])*n)+prop.startRgb[j])+(j<prop.startRgb.length-1?",":"");
}
_7dd+=")";
}else{
_7dd=((this.diffs[i])*n)+prop.start+(prop.property!="opacity"?prop.units||"px":"");
}
}
ret[dojo.html.toCamelCase(prop.property)]=_7dd;
},this);
return ret;
};
};
var anim=new dojo.lfx.Animation({beforeBegin:function(){
_7c7(_7c6);
anim.curve=new _7d5(_7c6.propertyMap);
},onAnimate:function(_7e0){
dojo.lang.forEach(_7c6.nodes,function(node){
_7d1(node,_7e0);
});
}},_7c6.duration,null,_7c6.easing);
if(_7c5){
for(var x in _7c5){
if(dojo.lang.isFunction(_7c5[x])){
anim.connect(x,anim,_7c5[x]);
}
}
}
return anim;
};
dojo.lfx.html._makeFadeable=function(_7e3){
var _7e4=function(node){
if(dojo.render.html.ie){
if((node.style.zoom.length==0)&&(dojo.html.getStyle(node,"zoom")=="normal")){
node.style.zoom="1";
}
if((node.style.width.length==0)&&(dojo.html.getStyle(node,"width")=="auto")){
node.style.width="auto";
}
}
};
if(dojo.lang.isArrayLike(_7e3)){
dojo.lang.forEach(_7e3,_7e4);
}else{
_7e4(_7e3);
}
};
dojo.lfx.html.fade=function(_7e6,_7e7,_7e8,_7e9,_7ea){
_7e6=dojo.lfx.html._byId(_7e6);
var _7eb={property:"opacity"};
if(!dj_undef("start",_7e7)){
_7eb.start=_7e7.start;
}else{
_7eb.start=function(){
return dojo.html.getOpacity(_7e6[0]);
};
}
if(!dj_undef("end",_7e7)){
_7eb.end=_7e7.end;
}else{
dojo.raise("dojo.lfx.html.fade needs an end value");
}
var anim=dojo.lfx.propertyAnimation(_7e6,[_7eb],_7e8,_7e9);
anim.connect("beforeBegin",function(){
dojo.lfx.html._makeFadeable(_7e6);
});
if(_7ea){
anim.connect("onEnd",function(){
_7ea(_7e6,anim);
});
}
return anim;
};
dojo.lfx.html.fadeIn=function(_7ed,_7ee,_7ef,_7f0){
return dojo.lfx.html.fade(_7ed,{end:1},_7ee,_7ef,_7f0);
};
dojo.lfx.html.fadeOut=function(_7f1,_7f2,_7f3,_7f4){
return dojo.lfx.html.fade(_7f1,{end:0},_7f2,_7f3,_7f4);
};
dojo.lfx.html.fadeShow=function(_7f5,_7f6,_7f7,_7f8){
_7f5=dojo.lfx.html._byId(_7f5);
dojo.lang.forEach(_7f5,function(node){
dojo.html.setOpacity(node,0);
});
var anim=dojo.lfx.html.fadeIn(_7f5,_7f6,_7f7,_7f8);
anim.connect("beforeBegin",function(){
if(dojo.lang.isArrayLike(_7f5)){
dojo.lang.forEach(_7f5,dojo.html.show);
}else{
dojo.html.show(_7f5);
}
});
return anim;
};
dojo.lfx.html.fadeHide=function(_7fb,_7fc,_7fd,_7fe){
var anim=dojo.lfx.html.fadeOut(_7fb,_7fc,_7fd,function(){
if(dojo.lang.isArrayLike(_7fb)){
dojo.lang.forEach(_7fb,dojo.html.hide);
}else{
dojo.html.hide(_7fb);
}
if(_7fe){
_7fe(_7fb,anim);
}
});
return anim;
};
dojo.lfx.html.wipeIn=function(_800,_801,_802,_803){
_800=dojo.lfx.html._byId(_800);
var _804=[];
dojo.lang.forEach(_800,function(node){
var _806={};
with(node.style){
visibility="hidden";
display="";
}
var _807=dojo.html.getBorderBox(node).height;
with(node.style){
visibility="";
display="none";
}
var anim=dojo.lfx.propertyAnimation(node,{"height":{start:1,end:function(){
return _807;
}}},_801,_802);
anim.connect("beforeBegin",function(){
_806.overflow=node.style.overflow;
_806.height=node.style.height;
with(node.style){
overflow="hidden";
height="1px";
}
dojo.html.show(node);
});
anim.connect("onEnd",function(){
with(node.style){
overflow=_806.overflow;
height=_806.height;
}
if(_803){
_803(node,anim);
}
});
_804.push(anim);
});
return dojo.lfx.combine(_804);
};
dojo.lfx.html.wipeOut=function(_809,_80a,_80b,_80c){
_809=dojo.lfx.html._byId(_809);
var _80d=[];
dojo.lang.forEach(_809,function(node){
var _80f={};
var anim=dojo.lfx.propertyAnimation(node,{"height":{start:function(){
return dojo.html.getContentBox(node).height;
},end:1}},_80a,_80b,{"beforeBegin":function(){
_80f.overflow=node.style.overflow;
_80f.height=node.style.height;
with(node.style){
overflow="hidden";
}
dojo.html.show(node);
},"onEnd":function(){
dojo.html.hide(node);
with(node.style){
overflow=_80f.overflow;
height=_80f.height;
}
if(_80c){
_80c(node,anim);
}
}});
_80d.push(anim);
});
return dojo.lfx.combine(_80d);
};
dojo.lfx.html.slideTo=function(_811,_812,_813,_814,_815){
_811=dojo.lfx.html._byId(_811);
var _816=[];
var _817=dojo.html.getComputedStyle;
dojo.lang.forEach(_811,function(node){
var top=null;
var left=null;
var init=(function(){
var _81c=node;
return function(){
var pos=_817(_81c,"position");
top=(pos=="absolute"?node.offsetTop:parseInt(_817(node,"top"))||0);
left=(pos=="absolute"?node.offsetLeft:parseInt(_817(node,"left"))||0);
if(!dojo.lang.inArray(["absolute","relative"],pos)){
var ret=dojo.html.abs(_81c,true);
dojo.html.setStyleAttributes(_81c,"position:absolute;top:"+ret.y+"px;left:"+ret.x+"px;");
top=ret.y;
left=ret.x;
}
};
})();
init();
var anim=dojo.lfx.propertyAnimation(node,{"top":{start:top,end:(_812.top||0)},"left":{start:left,end:(_812.left||0)}},_813,_814,{"beforeBegin":init});
if(_815){
anim.connect("onEnd",function(){
_815(_811,anim);
});
}
_816.push(anim);
});
return dojo.lfx.combine(_816);
};
dojo.lfx.html.slideBy=function(_820,_821,_822,_823,_824){
_820=dojo.lfx.html._byId(_820);
var _825=[];
var _826=dojo.html.getComputedStyle;
dojo.lang.forEach(_820,function(node){
var top=null;
var left=null;
var init=(function(){
var _82b=node;
return function(){
var pos=_826(_82b,"position");
top=(pos=="absolute"?node.offsetTop:parseInt(_826(node,"top"))||0);
left=(pos=="absolute"?node.offsetLeft:parseInt(_826(node,"left"))||0);
if(!dojo.lang.inArray(["absolute","relative"],pos)){
var ret=dojo.html.abs(_82b,true);
dojo.html.setStyleAttributes(_82b,"position:absolute;top:"+ret.y+"px;left:"+ret.x+"px;");
top=ret.y;
left=ret.x;
}
};
})();
init();
var anim=dojo.lfx.propertyAnimation(node,{"top":{start:top,end:top+(_821.top||0)},"left":{start:left,end:left+(_821.left||0)}},_822,_823).connect("beforeBegin",init);
if(_824){
anim.connect("onEnd",function(){
_824(_820,anim);
});
}
_825.push(anim);
});
return dojo.lfx.combine(_825);
};
dojo.lfx.html.explode=function(_82f,_830,_831,_832,_833){
var h=dojo.html;
_82f=dojo.byId(_82f);
_830=dojo.byId(_830);
var _835=h.toCoordinateObject(_82f,true);
var _836=document.createElement("div");
h.copyStyle(_836,_830);
if(_830.explodeClassName){
_836.className=_830.explodeClassName;
}
with(_836.style){
position="absolute";
display="none";
var _837=h.getStyle(_82f,"background-color");
backgroundColor=_837?_837.toLowerCase():"transparent";
backgroundColor=(backgroundColor=="transparent")?"rgb(221, 221, 221)":backgroundColor;
}
dojo.body().appendChild(_836);
with(_830.style){
visibility="hidden";
display="block";
}
var _838=h.toCoordinateObject(_830,true);
with(_830.style){
display="none";
visibility="visible";
}
var _839={opacity:{start:0.5,end:1}};
dojo.lang.forEach(["height","width","top","left"],function(type){
_839[type]={start:_835[type],end:_838[type]};
});
var anim=new dojo.lfx.propertyAnimation(_836,_839,_831,_832,{"beforeBegin":function(){
h.setDisplay(_836,"block");
},"onEnd":function(){
h.setDisplay(_830,"block");
_836.parentNode.removeChild(_836);
}});
if(_833){
anim.connect("onEnd",function(){
_833(_830,anim);
});
}
return anim;
};
dojo.lfx.html.implode=function(_83c,end,_83e,_83f,_840){
var h=dojo.html;
_83c=dojo.byId(_83c);
end=dojo.byId(end);
var _842=dojo.html.toCoordinateObject(_83c,true);
var _843=dojo.html.toCoordinateObject(end,true);
var _844=document.createElement("div");
dojo.html.copyStyle(_844,_83c);
if(_83c.explodeClassName){
_844.className=_83c.explodeClassName;
}
dojo.html.setOpacity(_844,0.3);
with(_844.style){
position="absolute";
display="none";
backgroundColor=h.getStyle(_83c,"background-color").toLowerCase();
}
dojo.body().appendChild(_844);
var _845={opacity:{start:1,end:0.5}};
dojo.lang.forEach(["height","width","top","left"],function(type){
_845[type]={start:_842[type],end:_843[type]};
});
var anim=new dojo.lfx.propertyAnimation(_844,_845,_83e,_83f,{"beforeBegin":function(){
dojo.html.hide(_83c);
dojo.html.show(_844);
},"onEnd":function(){
_844.parentNode.removeChild(_844);
}});
if(_840){
anim.connect("onEnd",function(){
_840(_83c,anim);
});
}
return anim;
};
dojo.lfx.html.highlight=function(_848,_849,_84a,_84b,_84c){
_848=dojo.lfx.html._byId(_848);
var _84d=[];
dojo.lang.forEach(_848,function(node){
var _84f=dojo.html.getBackgroundColor(node);
var bg=dojo.html.getStyle(node,"background-color").toLowerCase();
var _851=dojo.html.getStyle(node,"background-image");
var _852=(bg=="transparent"||bg=="rgba(0, 0, 0, 0)");
while(_84f.length>3){
_84f.pop();
}
var rgb=new dojo.gfx.color.Color(_849);
var _854=new dojo.gfx.color.Color(_84f);
var anim=dojo.lfx.propertyAnimation(node,{"background-color":{start:rgb,end:_854}},_84a,_84b,{"beforeBegin":function(){
if(_851){
node.style.backgroundImage="none";
}
node.style.backgroundColor="rgb("+rgb.toRgb().join(",")+")";
},"onEnd":function(){
if(_851){
node.style.backgroundImage=_851;
}
if(_852){
node.style.backgroundColor="transparent";
}
if(_84c){
_84c(node,anim);
}
}});
_84d.push(anim);
});
return dojo.lfx.combine(_84d);
};
dojo.lfx.html.unhighlight=function(_856,_857,_858,_859,_85a){
_856=dojo.lfx.html._byId(_856);
var _85b=[];
dojo.lang.forEach(_856,function(node){
var _85d=new dojo.gfx.color.Color(dojo.html.getBackgroundColor(node));
var rgb=new dojo.gfx.color.Color(_857);
var _85f=dojo.html.getStyle(node,"background-image");
var anim=dojo.lfx.propertyAnimation(node,{"background-color":{start:_85d,end:rgb}},_858,_859,{"beforeBegin":function(){
if(_85f){
node.style.backgroundImage="none";
}
node.style.backgroundColor="rgb("+_85d.toRgb().join(",")+")";
},"onEnd":function(){
if(_85a){
_85a(node,anim);
}
}});
_85b.push(anim);
});
return dojo.lfx.combine(_85b);
};
dojo.lang.mixin(dojo.lfx,dojo.lfx.html);
dojo.kwCompoundRequire({browser:["dojo.lfx.html"],dashboard:["dojo.lfx.html"]});
dojo.provide("dojo.lfx.*");
dojo.provide("dojo.lfx.toggler");
dojo.lfx.toggler.plain=function(){
this.stop=function(){
};
this.show=function(node,_862,_863,_864){
dojo.html.show(node);
if(dojo.lang.isFunction(_864)){
_864();
}
};
this.hide=function(node,_866,_867,_868){
dojo.html.hide(node);
if(dojo.lang.isFunction(_868)){
_868();
}
};
};
dojo.lfx.toggler.common={stop:function(){
if(this.anim&&this.anim.status()!="stopped"){
this.anim.stop();
}
},_act:function(_869,node,_86b,_86c,_86d,_86e){
this.stop();
this.anim=dojo.lfx[_869](node,_86b,_86c,_86d).play();
},show:function(node,_870,_871,_872,_873){
this._act(this.show_action,node,_870,_871,_872,_873);
},hide:function(node,_875,_876,_877,_878){
this._act(this.hide_action,node,_875,_876,_877,_878);
}};
dojo.lfx.toggler.fade=function(){
this.anim=null;
this.show_action="fadeShow";
this.hide_action="fadeHide";
};
dojo.extend(dojo.lfx.toggler.fade,dojo.lfx.toggler.common);
dojo.lfx.toggler.wipe=function(){
this.anim=null;
this.show_action="wipeIn";
this.hide_action="wipeOut";
};
dojo.extend(dojo.lfx.toggler.wipe,dojo.lfx.toggler.common);
dojo.lfx.toggler.explode=function(){
this.anim=null;
this.show_action="explode";
this.hide_action="implode";
this.show=function(node,_87a,_87b,_87c,_87d){
this.stop();
this.anim=dojo.lfx.explode(_87d||{x:0,y:0,width:0,height:0},node,_87a,_87b,_87c).play();
};
this.hide=function(node,_87f,_880,_881,_882){
this.stop();
this.anim=dojo.lfx.implode(node,_882||{x:0,y:0,width:0,height:0},_87f,_880,_881).play();
};
};
dojo.extend(dojo.lfx.toggler.explode,dojo.lfx.toggler.common);
dojo.provide("dojo.widget.HtmlWidget");
dojo.declare("dojo.widget.HtmlWidget",dojo.widget.DomWidget,{templateCssPath:null,templatePath:null,lang:"",toggle:"plain",toggleDuration:150,initialize:function(args,frag){
},postMixInProperties:function(args,frag){
if(this.lang===""){
this.lang=null;
}
this.toggleObj=new (dojo.lfx.toggler[this.toggle.toLowerCase()]||dojo.lfx.toggler.plain);
},createNodesFromText:function(txt,wrap){
return dojo.html.createNodesFromText(txt,wrap);
},destroyRendering:function(_889){
try{
if(this.bgIframe){
this.bgIframe.remove();
delete this.bgIframe;
}
if(!_889&&this.domNode){
dojo.event.browser.clean(this.domNode);
}
dojo.widget.HtmlWidget.superclass.destroyRendering.call(this);
}
catch(e){
}
},isShowing:function(){
return dojo.html.isShowing(this.domNode);
},toggleShowing:function(){
if(this.isShowing()){
this.hide();
}else{
this.show();
}
},show:function(){
if(this.isShowing()){
return;
}
this.animationInProgress=true;
this.toggleObj.show(this.domNode,this.toggleDuration,null,dojo.lang.hitch(this,this.onShow),this.explodeSrc);
},onShow:function(){
this.animationInProgress=false;
this.checkSize();
},hide:function(){
if(!this.isShowing()){
return;
}
this.animationInProgress=true;
this.toggleObj.hide(this.domNode,this.toggleDuration,null,dojo.lang.hitch(this,this.onHide),this.explodeSrc);
},onHide:function(){
this.animationInProgress=false;
},_isResized:function(w,h){
if(!this.isShowing()){
return false;
}
var wh=dojo.html.getMarginBox(this.domNode);
var _88d=w||wh.width;
var _88e=h||wh.height;
if(this.width==_88d&&this.height==_88e){
return false;
}
this.width=_88d;
this.height=_88e;
return true;
},checkSize:function(){
if(!this._isResized()){
return;
}
this.onResized();
},resizeTo:function(w,h){
dojo.html.setMarginBox(this.domNode,{width:w,height:h});
if(this.isShowing()){
this.onResized();
}
},resizeSoon:function(){
if(this.isShowing()){
dojo.lang.setTimeout(this,this.onResized,0);
}
},onResized:function(){
dojo.lang.forEach(this.children,function(_891){
if(_891.checkSize){
_891.checkSize();
}
});
}});
dojo.kwCompoundRequire({common:["dojo.xml.Parse","dojo.widget.Widget","dojo.widget.Parse","dojo.widget.Manager"],browser:["dojo.widget.DomWidget","dojo.widget.HtmlWidget"],dashboard:["dojo.widget.DomWidget","dojo.widget.HtmlWidget"],svg:["dojo.widget.SvgWidget"],rhino:["dojo.widget.SwtWidget"]});
dojo.provide("dojo.widget.*");
dojo.provide("dojo.html.iframe");
dojo.html.iframeContentWindow=function(_892){
var win=dojo.html.getDocumentWindow(dojo.html.iframeContentDocument(_892))||dojo.html.iframeContentDocument(_892)["__parent__"]||(_892.name&&document.frames[_892.name])||null;
return win;
};
dojo.html.iframeContentDocument=function(_894){
var doc=_894.contentDocument||((_894.contentWindow)&&(_894.contentWindow.document))||((_894.name)&&(document.frames[_894.name])&&(document.frames[_894.name].document))||null;
return doc;
};
dojo.html.BackgroundIframe=function(node){
if(dojo.render.html.ie55||dojo.render.html.ie60){
var html="<iframe src='javascript:false'"+" style='position: absolute; left: 0px; top: 0px; width: 100%; height: 100%;"+"z-index: -1; filter:Alpha(Opacity=\"0\");' "+">";
this.iframe=dojo.doc().createElement(html);
this.iframe.tabIndex=-1;
if(node){
node.appendChild(this.iframe);
this.domNode=node;
}else{
dojo.body().appendChild(this.iframe);
this.iframe.style.display="none";
}
}
};
dojo.lang.extend(dojo.html.BackgroundIframe,{iframe:null,onResized:function(){
if(this.iframe&&this.domNode&&this.domNode.parentNode){
var _898=dojo.html.getMarginBox(this.domNode);
if(_898.width==0||_898.height==0){
dojo.lang.setTimeout(this,this.onResized,100);
return;
}
this.iframe.style.width=_898.width+"px";
this.iframe.style.height=_898.height+"px";
}
},size:function(node){
if(!this.iframe){
return;
}
var _89a=dojo.html.toCoordinateObject(node,true,dojo.html.boxSizing.BORDER_BOX);
with(this.iframe.style){
width=_89a.width+"px";
height=_89a.height+"px";
left=_89a.left+"px";
top=_89a.top+"px";
}
},setZIndex:function(node){
if(!this.iframe){
return;
}
if(dojo.dom.isNode(node)){
this.iframe.style.zIndex=dojo.html.getStyle(node,"z-index")-1;
}else{
if(!isNaN(node)){
this.iframe.style.zIndex=node;
}
}
},show:function(){
if(this.iframe){
this.iframe.style.display="block";
}
},hide:function(){
if(this.iframe){
this.iframe.style.display="none";
}
},remove:function(){
if(this.iframe){
dojo.html.removeNode(this.iframe,true);
delete this.iframe;
this.iframe=null;
}
}});
dojo.provide("dojo.lfx.scroll");
dojo.lfx.smoothScroll=function(node,win,_89e,_89f,_8a0,_8a1){
var _8a2={"window":win,"offset":_89e||{x:0,y:0},"target":dojo.html.getAbsolutePositionExt(node,true,dojo.html.boxSizing.BORDER_BOX,win),"duration":_89f,"easing":_8a0||dojo.lfx.easeOut};
var anim=new dojo.lfx.Animation({beforeBegin:function(){
var _8a4=dojo.withGlobal(_8a2.window,dojo.html.getScroll).offset;
delete this.curve;
anim.curve=new dojo.lfx.Line([_8a4.x,_8a4.y],[_8a2.target.x+_8a2.offset.x,_8a2.target.y+_8a2.offset.y]);
},onAnimate:function(_8a5){
_8a2.window.scrollTo(_8a5[0],_8a5[1]);
}},_89f,null,_8a0||dojo.lfx.easeOut);
if(_8a1){
for(var x in _8a1){
if(dojo.lang.isFunction(_8a1[x])){
anim.connect(x,anim,_8a1[x]);
}
}
}
return anim;
};
dojo.provide("dojo.logging.Logger");
dojo.provide("dojo.logging.LogFilter");
dojo.provide("dojo.logging.Record");
dojo.provide("dojo.log");
dojo.logging.Record=function(_8a7,_8a8){
this.level=_8a7;
this.message="";
this.msgArgs=[];
this.time=new Date();
if(dojo.lang.isArray(_8a8)){
if(_8a8.length>0&&dojo.lang.isString(_8a8[0])){
this.message=_8a8.shift();
}
this.msgArgs=_8a8;
}else{
this.message=_8a8;
}
};
dojo.logging.LogFilter=function(_8a9){
this.passChain=_8a9||"";
this.filter=function(_8aa){
return true;
};
};
dojo.logging.Logger=function(){
this.cutOffLevel=0;
this.propagate=true;
this.parent=null;
this.data=[];
this.filters=[];
this.handlers=[];
};
dojo.extend(dojo.logging.Logger,{_argsToArr:function(args){
var ret=[];
for(var x=0;x<args.length;x++){
ret.push(args[x]);
}
return ret;
},setLevel:function(lvl){
this.cutOffLevel=parseInt(lvl);
},isEnabledFor:function(lvl){
return parseInt(lvl)>=this.cutOffLevel;
},getEffectiveLevel:function(){
if((this.cutOffLevel==0)&&(this.parent)){
return this.parent.getEffectiveLevel();
}
return this.cutOffLevel;
},addFilter:function(flt){
this.filters.push(flt);
return this.filters.length-1;
},removeFilterByIndex:function(_8b1){
if(this.filters[_8b1]){
delete this.filters[_8b1];
return true;
}
return false;
},removeFilter:function(_8b2){
for(var x=0;x<this.filters.length;x++){
if(this.filters[x]===_8b2){
delete this.filters[x];
return true;
}
}
return false;
},removeAllFilters:function(){
this.filters=[];
},filter:function(rec){
for(var x=0;x<this.filters.length;x++){
if((this.filters[x]["filter"])&&(!this.filters[x].filter(rec))||(rec.level<this.cutOffLevel)){
return false;
}
}
return true;
},addHandler:function(hdlr){
this.handlers.push(hdlr);
return this.handlers.length-1;
},handle:function(rec){
if((!this.filter(rec))||(rec.level<this.cutOffLevel)){
return false;
}
for(var x=0;x<this.handlers.length;x++){
if(this.handlers[x]["handle"]){
this.handlers[x].handle(rec);
}
}
return true;
},log:function(lvl,msg){
if((this.propagate)&&(this.parent)&&(this.parent.rec.level>=this.cutOffLevel)){
this.parent.log(lvl,msg);
return false;
}
this.handle(new dojo.logging.Record(lvl,msg));
return true;
},debug:function(msg){
return this.logType("DEBUG",this._argsToArr(arguments));
},info:function(msg){
return this.logType("INFO",this._argsToArr(arguments));
},warning:function(msg){
return this.logType("WARNING",this._argsToArr(arguments));
},error:function(msg){
return this.logType("ERROR",this._argsToArr(arguments));
},critical:function(msg){
return this.logType("CRITICAL",this._argsToArr(arguments));
},exception:function(msg,e,_8c2){
if(e){
var _8c3=[e.name,(e.description||e.message)];
if(e.fileName){
_8c3.push(e.fileName);
_8c3.push("line "+e.lineNumber);
}
msg+=" "+_8c3.join(" : ");
}
this.logType("ERROR",msg);
if(!_8c2){
throw e;
}
},logType:function(type,args){
return this.log.apply(this,[dojo.logging.log.getLevel(type),args]);
},warn:function(){
this.warning.apply(this,arguments);
},err:function(){
this.error.apply(this,arguments);
},crit:function(){
this.critical.apply(this,arguments);
}});
dojo.logging.LogHandler=function(_8c6){
this.cutOffLevel=(_8c6)?_8c6:0;
this.formatter=null;
this.data=[];
this.filters=[];
};
dojo.lang.extend(dojo.logging.LogHandler,{setFormatter:function(_8c7){
dojo.unimplemented("setFormatter");
},flush:function(){
},close:function(){
},handleError:function(){
dojo.deprecated("dojo.logging.LogHandler.handleError","use handle()","0.6");
},handle:function(_8c8){
if((this.filter(_8c8))&&(_8c8.level>=this.cutOffLevel)){
this.emit(_8c8);
}
},emit:function(_8c9){
dojo.unimplemented("emit");
}});
void (function(){
var _8ca=["setLevel","addFilter","removeFilterByIndex","removeFilter","removeAllFilters","filter"];
var tgt=dojo.logging.LogHandler.prototype;
var src=dojo.logging.Logger.prototype;
for(var x=0;x<_8ca.length;x++){
tgt[_8ca[x]]=src[_8ca[x]];
}
})();
dojo.logging.log=new dojo.logging.Logger();
dojo.logging.log.levels=[{"name":"DEBUG","level":1},{"name":"INFO","level":2},{"name":"WARNING","level":3},{"name":"ERROR","level":4},{"name":"CRITICAL","level":5}];
dojo.logging.log.loggers={};
dojo.logging.log.getLogger=function(name){
if(!this.loggers[name]){
this.loggers[name]=new dojo.logging.Logger();
this.loggers[name].parent=this;
}
return this.loggers[name];
};
dojo.logging.log.getLevelName=function(lvl){
for(var x=0;x<this.levels.length;x++){
if(this.levels[x].level==lvl){
return this.levels[x].name;
}
}
return null;
};
dojo.logging.log.getLevel=function(name){
for(var x=0;x<this.levels.length;x++){
if(this.levels[x].name.toUpperCase()==name.toUpperCase()){
return this.levels[x].level;
}
}
return null;
};
dojo.declare("dojo.logging.MemoryLogHandler",dojo.logging.LogHandler,function(_8d3,_8d4,_8d5,_8d6){
dojo.logging.LogHandler.call(this,_8d3);
this.numRecords=(typeof djConfig["loggingNumRecords"]!="undefined")?djConfig["loggingNumRecords"]:((_8d4)?_8d4:-1);
this.postType=(typeof djConfig["loggingPostType"]!="undefined")?djConfig["loggingPostType"]:(_8d5||-1);
this.postInterval=(typeof djConfig["loggingPostInterval"]!="undefined")?djConfig["loggingPostInterval"]:(_8d5||-1);
},{emit:function(_8d7){
if(!djConfig.isDebug){
return;
}
var _8d8=String(dojo.log.getLevelName(_8d7.level)+": "+_8d7.time.toLocaleTimeString())+": "+_8d7.message;
if(!dj_undef("println",dojo.hostenv)){
dojo.hostenv.println(_8d8,_8d7.msgArgs);
}
this.data.push(_8d7);
if(this.numRecords!=-1){
while(this.data.length>this.numRecords){
this.data.shift();
}
}
}});
dojo.logging.logQueueHandler=new dojo.logging.MemoryLogHandler(0,50,0,10000);
dojo.logging.log.addHandler(dojo.logging.logQueueHandler);
dojo.log=dojo.logging.log;
dojo.kwCompoundRequire({common:[["dojo.logging.Logger",false,false]],rhino:["dojo.logging.RhinoLogger"]});
dojo.provide("dojo.logging.*");

