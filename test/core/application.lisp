(in-package :ucw-test)

(defsuite* (test/application :in test) ()
  (with-fixture ensure-test-server
    (run-child-tests)))

(defun run-application (application &rest initargs)
  (rebind (*test-server* *test-application*)
    (unwind-protect
         (progn
           (startup-test-server (make-test-server))
           (unless (typep application 'application)
             (setf application (apply #'make-instance application initargs)))
           (register-test-application application)
           (break "Your application is running, point your browser to ~A"
                  (print-uri-to-string (uri))))
      (shutdown-test-server))))

(defclass test-application (basic-application)
  ())

(defparameter +application-test-answer+     "Test Application? Brilliant!")
(defparameter +test-application-url-prefix+ "/test-app/")
(defparameter +application-test-path+       (strcat +test-application-url-prefix+ "brilliant.ucw"))

(defmethod service ((app test-application) context)
  "Creates a special case for the remote test 
This also tests FUNCALL-WITH-REQUEST-CONTEXT"
  (if (string= (query-path (context.request context)) +application-test-path+)
      (funcall-with-request-context
       context
       (lambda ()
         (out +application-test-answer+)))
      (call-next-method)))

(defun make-test-application (&key (application 'test-application))
  (make-instance application
                 :url-prefix +test-application-url-prefix+
                 ;; dispatchers must be nil
                 ;; or are created by default
                 :dispatchers nil))

(defun register-test-application (&optional (app *test-application*) (server *test-server*))
  (finishes
    (let ((app-count (length (server.applications server))))
      (register-application server app)
      (setf *test-application* app)
      (is (eq (application.server app) server))
      (is (= (1+ app-count) (length (server.applications server)))))))

(defun unregister-test-application (&optional (app *test-application*) (server *test-server*))
  (finishes
    (let ((app-count (length (server.applications server))))
      (unregister-application server app)
      (is (eq (application.server app) server))
      (is (= (1- app-count) (length (server.applications server)))))))

(defixture ensure-test-application
  (:setup
   (ensure-test-server :setup)
   (register-test-application (make-test-application)))
  (:teardown
   (unregister-test-application)
   (ensure-test-server :teardown)))

(deftest test/application/answers ()
  (with-fixture ensure-test-server
    (with-fixture ensure-test-application
      (is (string= +application-test-answer+ (web +application-test-path+))))
    (is (not (string= +application-test-answer+
                      (web +application-test-path+ :expected-status 404))))))

(defmacro defapptest (name args &body body)
  `(deftest ,name ,args
    (with-fixture ensure-test-application
      ,@body)))
