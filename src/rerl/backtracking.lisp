;; -*- lisp -*-

(in-package :it.bese.ucw)

;;;; ** Backtracking 

;;;; Places

(defmacro make-place (form &optional (copyer '#'identity))
  "Create a new place around FORM."
  (with-unique-names (v)
    `(make-instance 'place
                    :getter (lambda () ,form)
                    :setter (lambda (,v) (setf ,form ,v))
                    :copyer ,copyer
                    :form ',form)))

(defgeneric place (place)
  (:method ((place place))
           "Returns the current value of PLACE."
           (etypecase (place.getter place)
             (function (funcall (place.getter place)))
             (arnesi::closure/cc
              (with-call/cc
                (funcall (place.getter place)))))))

(defgeneric (setf place) (value place)
  (:method (value (place place))
           "Set the value of PLACE to VALUE."
           (etypecase (place.setter place)
             (function (funcall (place.setter place) value))
             (arnesi::closure/cc
              (with-call/cc
                (funcall (place.setter place) value))))))

(defgeneric clone-place-value (place &optional value)
  (:method ((place place) &optional (value (place place)))
           "Return a copy of the current value in PLACE."
           (funcall (place.copyer place) value)))

;;;; Backtracking

(defun iterate-effective-backtracks (frame visitor)
  (declare (type function visitor)
           (optimize (speed 3)))
  (iter (with previous-cell = nil)
        (for cell :first (effective-backtracks-of frame) :then (cdr cell))
        (while cell)
        (for place-value-cons = (car cell))
        (for place = (trivial-garbage:weak-pointer-value (car place-value-cons)))
        (for value = (cdr place-value-cons))
        (if place
            (progn
              (funcall visitor place value place-value-cons)
              (setf previous-cell cell))
            (if previous-cell
                (progn
                  (setf (cdr previous-cell) (cdr cell))
                  (setf previous-cell cell))
                (setf (effective-backtracks-of frame) (cdr cell))))))

(defun clone-effective-backtracks-of (frame)
  "We create a new list of backtracks with the same getter, setter and value but a new copy of value."
  (declare (optimize (speed 3)))
  (let ((result '()))
    (iterate-effective-backtracks
     frame
     (lambda (place value place-value-cons)
       (declare (ignore place-value-cons))
       (push (cons (trivial-garbage:make-weak-pointer place) value) result)))
    result))

(defun clear-effective-backtracks-of (frame)
  (setf (effective-backtracks-of frame) '()))

(defgeneric save-backtracked (frame)
  (:method ((frame standard-session-frame))
           "For every place in FRAME we want to back track save (in frame's
  BACKTRACKS) a copy of the current value of the backtrack
  places. This involes iterating over FRAME's backtracks and
  calling the copyer on the result of copying the getter."
           (declare (optimize (speed 3)))
           (iterate-effective-backtracks
            frame
            (lambda (place value place-value-cons)
              (declare (ignore value))
              (setf (cdr place-value-cons) (place place))))))

(defgeneric reinstate-backtracked (frame)
  (:method ((frame standard-session-frame))
           "Reset all the values of the backtracked places in FRAME to the
  values they had the last FRAME was handled."
           (declare (optimize (speed 3)))
           (iterate-effective-backtracks
            frame
            (lambda (place value place-value-cons)
              (declare (ignore place-value-cons))
              (setf (place place) (clone-place-value place value))))))

(defgeneric backtrack (frame place &optional value)
  (:method ((frame session-frame) place &optional (value (place place)))
           (vector-push-extend place (allocated-backtracks-of frame))
           (push (cons (trivial-garbage:make-weak-pointer place) value)
                 (effective-backtracks-of frame))
           t))

(defconstant +unbound-value+ '|#<unbound-value>|)

(defgeneric backtrack-slot (frame object slot-name &optional copyer)
  (:method ((frame standard-session-frame) object slot-name &optional (copyer #'identity))
           (backtrack frame
                      (make-instance 'place
                                     :getter (lambda ()
                                               (if (slot-boundp object slot-name)
                                                   (slot-value object slot-name)
                                                   +unbound-value+))
                                     :setter (lambda (v)
                                               (if (eql +unbound-value+ v)
                                                   (slot-makunbound object slot-name)
                                                   (setf (slot-value object slot-name) v)))
                                     :copyer copyer
                                     :form `(slot-value ,object ,slot-name)))))

;; Copyright (c) 2003-2005 Edward Marco Baringer
;; All rights reserved. 
;; 
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;; 
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 
;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;    of its contributors may be used to endorse or promote products
;;    derived from this software without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
