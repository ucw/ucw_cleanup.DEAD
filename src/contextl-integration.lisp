(in-package :ucw)

(eval-always
  (use-package :contextl :ucw))

(export '(contextl-context-capturing-ajax-component-mixin
          contextl-context-capturing-action-mixin
          contextl-aware-widget-component
          layered-component-mixin
          contextl-aware-ajax-action)
        :ucw)

(defcomponent contextl-context-capturing-ajax-component-mixin (ajax-component-mixin)
  ((render-time-layer-context
    :initform nil
    :accessor render-time-layer-context-of))
  (:documentation "A component mixin that restores the contextl layers when it's being ajax rendered."))

(defmethod render :wrap-around ((self contextl-context-capturing-ajax-component-mixin))
  (setf (render-time-layer-context-of self)
        (current-layer-context))
  (call-next-method))

(defmethod ajax-render :around ((self contextl-context-capturing-ajax-component-mixin))
  (let ((previous-contextl-context (render-time-layer-context-of self)))
    (assert previous-contextl-context () "This should not happen: we are ajax-rendering a contextl-context-capturing-ajax-component-mixin without a valid render-time-layer-context slot.")
    (ucw.component.layers.debug "Reinstating layer context in ajax-render of ~A" self)
    (funcall-with-layer-context previous-contextl-context #'call-next-method)
    (ucw.component.layers.debug "Leaving the reinstated layer context of ajax-render of ~A" self)))

(defcomponent* contextl-aware-widget-component (contextl-context-capturing-ajax-component-mixin
                                                widget-component)
  ())

(defcomponent layered-component-mixin ()
  ((layer
    :initarg :layer
    :accessor layer-of)))

(defmethod render :wrap-around ((self layered-component-mixin))
  (ucw.component.layers.debug "Entering layer ~A which is wrapping component ~A" (layer-of self) self)
  (funcall-with-layer-context (adjoin-layer (layer-of self)
                                            (current-layer-context))
                              #'call-next-method)
  (ucw.component.layers.debug "Leaving layer ~A that was wrapping component ~A" (layer-of self) self))

(defclass contextl-context-capturing-action-mixin ()
  ((render-time-layer-context
    :initform nil
    :accessor render-time-layer-context-of))
  (:documentation "An action mixin that captures the layer context at
instantiation time and restores it when the action is executed."))

(defmethod register-action-in-frame :before (frame (action contextl-context-capturing-action-mixin))
  (setf (render-time-layer-context-of action) (current-layer-context)))

(defmethod call-action :around ((action contextl-context-capturing-action-mixin)
                                application session frame)
  (let ((previous-contextl-context (render-time-layer-context-of action)))
    (assert previous-contextl-context () "This should not happen: we are execuring a contextl-context-capturing-action-mixin without a valid render-time-layer-context slot.")
    (ucw.component.layers.debug "Reinstating layer context in CALL-ACTION of ~A" action)
    (funcall-with-layer-context previous-contextl-context #'call-next-method)
    (ucw.component.layers.debug "Leaving the reinstated layer context of CALL-ACTION of ~A" action)))

(defclass contextl-aware-ajax-action (contextl-context-capturing-action-mixin
                                      ajax-action)
  ()
  (:metaclass mopp:funcallable-standard-class))

